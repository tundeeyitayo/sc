FROM ubuntu:22.04

WORKDIR /var/www/html

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Africa/Lagos

RUN apt-get update \
    && apt-get install -y cron && which cron \
    && rm -rf /etc/cron.*/* \
    && apt-get update \
    && apt-get install -y php8.1-cli php8.1-dev \
       php8.1-mysql php8.1-intl php8.1-ldap \
       php8.1-redis

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY crontab /etc/crontab
RUN chmod +x /usr/local/bin/entrypoint.sh
RUN chmod +x /etc/crontab
RUN chown root /etc/crontab

ENTRYPOINT ["entrypoint.sh"]
CMD ["cron","-f", "-l", "2"]
