<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notification;

class DeliveryAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'buyer_id',
        'address',
        'city',
        'state_id',
        'inputed_by'
    ];

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(NigerianState::class, 'state_id', 'id');
    }

    public function buyer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class, 'buyer_id', 'id');
    }

}
