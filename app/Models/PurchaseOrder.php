<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

class PurchaseOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        "buyer_id",
        "purchase_order_number",
        "line_item_details",
        "status",
        "buyer_comments",
        "delivery_date",
        "payment_terms",
        "inputer_id",
        "name_restricted",
        "delivery_address_id",
        "shipping_terms",
        "expected_warranty_period",
        "purchase_order_url",
        "additional_docs",
        "net_price_discrepancy_tolerance",
        "supplier_requirement",
        "po_finance_line_id",
        'restricted_to_own_suppliers',
        'request_type',
        'purchase_order_terms_url',
        'restricted_to_group',
        'restricted_to_selected',
        'selected_suppliers',
        'selected_group_name',
        'bid_bond_required',
        "min_acceptable_quantity",
        "wht",
        "wht_deductible"
    ];

    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    }

    public function bank_guarantee_requests(): HasMany
    {
        return $this->hasMany(BankGuaranteeRequest::class);
    }

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'buyer_id', 'id');
    }

    public function pending_authorization (): HasOne
    {
        return $this->hasOne(CompanyActivityLog::class, 'activity_key', 'id')->where('status', 'unauthorized');
    }

    public function bid_limitation(): string
    {
        return match (true) {
            $this->restricted_to_own_suppliers == 1 => 'All ' . $this->buyer->company_name . ' suppliers',
            $this->restricted_to_group == 1 => $this->selected_group_name . ' - supplier group',
            $this->restricted_to_selected == 1 => 'Selected ' . $this->buyer->company_name . ' suppliers',
            default => 'All ' . config('app.name') . ' suppliers',
        };
    }

    public function purchase_order_bids(): HasMany
    {
        return $this->hasMany(PurchaseOrderBid::class);
    }

    public function pending_action(): BelongsTo
    {
        return $this->belongsTo(PendingAction::class, 'id', 'pending_action_key');
    }

    public function pending_action_list(): ?Collection
    {
        return $this->pending_action ? collect(json_decode($this->pending_action->fields)) : null;
    }

    public function activity_logs (): HasMany
    {
        return $this->hasMany(CompanyActivityLog::class, 'activity_key', 'id');
    }

    public function last_rejection_comment(): ?string
    {
        $log = collect($this->activity_logs)->filter(function($item) {return ($item->updated_at == $this->activity_logs->max("updated_at")) && filled($item->rejected_at);})->first()  ?? null;
        return  ($log && filled($log->rejection_comments)) ? $log->rejection_comments . ' (' . $log->rejecter->name . ' @ ' . $log->updated_at . ')' : null;
    }

    public function linked_auction (): HasOne
    {
        return $this->hasOne(Auction::class, 'auction_transaction_key', 'id')->where('auction_type', 5);
    }

    public function pending_linked_auction (): ?Collection
    {
        return filled(collect(json_decode(CompanyActivityLog::where('activity_key', $this->id)->where('activity_type_id', 5)->get()?->last()?->payload)?->pending_linked_auction))
            ? collect(json_decode(CompanyActivityLog::where('activity_key', $this->id)->where('activity_type_id', 5)->get()?->last()?->payload)?->pending_linked_auction)
            : collect(json_decode(CompanyActivityLog::where('activity_key', $this->id)->where('activity_type_id', 4)->first()?->payload)?->request)->only(['auction_start', 'auction_duration', 'scheduled_start_date', 'auction_auto_restart']) ;
        /*if (blank($this->linked_auction)) {
            $modifiedAuction = CompanyActivityLog::where('activity_key', $this->id)->where('activity_type_id', 5)->get()->last();
            if (filled($modifiedAuction) && $modifiedAuction->payload->request) {
               return collect(json_decode(CompanyActivityLog::where('activity_key', $this->id)->where('activity_type_id', 5)?->get()->last()->payload)->request)?->only(['auction_start', 'auction_duration', 'scheduled_start_date', 'auction_auto_restart']);
            }
        }
        else {
            return  $this->linked_auction;
        }*/
    }

    public function suppliers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Company::class)->withPivot('is_selected')->using(CompanyPurchaseOrder::class);
    }

    public function preferred_suppliers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
         return $this->belongsToMany(Company::class)->wherePivot('is_selected', 1)->using(CompanyPurchaseOrder::class);
    }

    public function pre_modification_values () {
        if ($this->status == 'modification effected') {
            return filled($this->activity_logs->where('activity_type_id', 5))
                ? json_decode($this->activity_logs->where('activity_type_id', 5)->last()->payload)->old_values
                : null;
        }
        return null;
    }

    public function poLimitation(): string
    {
        switch(true) {
            case $this->restricted_to_own_suppliers == 1:
                return "All " . $this->buyer->company_name . " suppliers";
            case $this->restricted_to_group == 1:
                return "Company Supplier Group: " . $this->selected_group_name;
            case $this->restricted_to_selected == 1:
                return "Selected " . $this->buyer->company_name . " suppliers";
            default:
                return "All " . config('app.name') . " suppliers";
        }
    }

    public function delivery_address (): BelongsTo
    {
        return $this->belongsTo(DeliveryAddress::class);
    }


}
