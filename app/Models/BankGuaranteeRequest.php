<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

class BankGuaranteeRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'supplier_id',
        'buyer_id',
        'request_type',
        'guarantee_amount',
        'guarantee_type',
        'purchase_order_id',
        'purchase_order_url',
        'status',
        'supporting_docs',
        'buyer_name',
        'requesting_as'
    ];

    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class);
    }

    public function purchase_order(): BelongsTo
    {
        return $this->belongsTo(PurchaseOrder::class);
    }

    public function currentStatus(): BelongsTo
    {
        return $this->belongsTo(BankGuaranteeRequestStatus::class, 'status', 'id');
    }

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'buyer_id', 'id');
    }

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'supplier_id', 'id');
    }

    public function pending_action(): BelongsTo
    {
        return $this->belongsTo(PendingAction::class, 'id', 'pending_action_key');
    }

    public function hasPendingActions() : bool {
        return $this->pending_action->count() > 0;
    }

    public function pending_action_list(): ?Collection
    {
        if (filled($this->pending_action) && filled($this->pending_action->fields)) {
            $fields = collect(json_decode($this->pending_action->fields));
            return collect(json_decode($this->pending_action->pending_action_type->parameters))->filter(function ($item) use ($fields){
                return $fields->contains($item->id);
            })->values();
        }
        return null;
    }

    public function bank_guarantee_request_bids() : HasMany
    {
        return $this->hasMany(BankGuaranteeRequestBid::class);
    }

    public function pending_authorization (): HasOne
    {
        return $this->hasOne(CompanyActivityLog::class, 'activity_key', 'id')->where('status', 'unauthorized');
    }

    public function pending_modification (): HasOne
    {
        return $this->hasOne(CompanyActivityLog::class, 'activity_key', 'id')->where('status', 'unauthorized')->where('activity_type_id', 2);
    }

    public function latest_modification_payload () {
        return filled($this->activity_logs->where('activity_type_id', 2)) ? json_decode($this->activity_logs->where('activity_type_id', 2)->last()->payload) : null;
    }

    public function live_auction(): HasOne
    {
        return $this->hasOne(Auction::class, 'auction_transaction_key', 'id')->where('auction_type', 1)->where('status', 'open');
    }

    public function activity_logs (): HasMany
    {
        return $this->hasMany(CompanyActivityLog::class, 'activity_key', 'id');
    }

    public function last_rejection_comment(): ?string
    {
        $log = collect($this->activity_logs)->filter(function($item) {return ($item->updated_at == $this->activity_logs->max("updated_at")) && filled($item->rejected_at);})->first()  ?? null;
        return  ($log && filled($log->rejection_comments)) ? $log->rejection_comments . ' (' . $log->rejecter->name . ' @ ' . $log->updated_at . ')' : null;
    }

}
