<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyerSupplier extends Model
{
    use HasFactory;

    protected $table ='buyer_supplier';

    public function supplier (): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class, 'supplier_id', 'id');
    }
}
