<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class CompanyActivityLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'activity_type_id',
        'activity_key',
        'company_id',
        'pending_action_id',
        'inputted_by',
        'authorization_required',
        'payload',
        'status',
    ];

    public function activity_type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ActivityType::class);
    }

    public function inputer (): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'inputted_by', 'id');
    }

    public function authorizer (): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'authorized_by', 'id');
    }

    public function rejecter (): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'rejected_by', 'id');
    }
    public function modification_requester (): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'modification_requested_by', 'id');
    }

    public function generateDetails(): ?array {
        switch ($this->activity_type->activity_type) {
            case "Bank Guarantee Request Cancellation" :
                $list = BankGuaranteeRequest::where('id', $this->activity_key)->get();
                $data = $list->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "bank" => $item->bank?->bank_name ?? 'Pending',
                        "request_type" => $item->request_type,
                        "guarantee_type" => $item->guarantee_type,
                        "requesting_as" => $item->requesting_as,
                        "bid_count" => $item->bank_guarantee_request_bids?->count() ?? 0,
                        "supplier" => $item->supplier?->company_name ?? 'Not Available',
                        "buyer" => $item->buyer?->company_name ?? $item->buyer_name,
                        "purchase_order_id" => $item->purchase_order_id,
                        "expiration_date" => $item->expiration_date,
                        "supporting_docs" => collect(json_decode($item->supporting_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "approved_tenure_days" => $item->approved_tenure_days,
                        "amount" => $item->guarantee_amount,
                        "purchase_order_url" => filled($item->purchase_order) ? $item->purchase_order->purchase_order_url : $item->purchase_order_url,
                        "status" => $item->pending_authorization ? json_decode($item->pending_authorization->payload)->status : "Not Available",
                        "approved_rate" => filled($item->approved_rate) ? $item->approved_rate : 'pending',
                        'total_charges_desc' => filled($item->charges) ? collect(json_decode($item->charges))->toArray() : 'pending',
                        "total_charges" => filled($item->charges) ? collect(json_decode($item->charges))->sum('amount') : 'pending',
                        "created" => $item->created_at,
                        "pending_actions" => $item->pending_action_list(),
                        "pending_action_comment" => $item->pending_action?->comments,
                        "has_pending_modification" => filled($item->pending_modification),
                        "modification_payload" => $item->latest_modification_payload(),
                    ];
                })->first();
                break;
            case "Bank Guarantee Request Modification" :
                $list = BankGuaranteeRequest::where('id', $this->activity_key)->get();
                $data = $list->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "bank" => $item->bank?->bank_name ?? 'Pending',
                        "guarantee_type" => $item->guarantee_type,
                        "request_type" => $item->request_type,
                        "requesting_as" => $item->requesting_as,
                        "bid_count" => $item->bank_guarantee_request_bids?->count() ?? 0,
                        "supplier" => $item->supplier?->company_name ?? 'Not Available',
                        "buyer" => $item->buyer?->company_name ?? $item->buyer_name,
                        "purchase_order_id" => $item->purchase_order_id,
                        "expiration_date" => $item->expiration_date,
                        "supporting_docs" => collect(json_decode($item->supporting_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "approved_tenure_days" => $item->approved_tenure_days,
                        "amount" => $item->guarantee_amount,
                        "purchase_order_url" => filled($item->purchase_order) ? $item->purchase_order->purchase_order_url : $item->purchase_order_url,
                        "status" => $item->pending_authorization ? json_decode($item->pending_authorization->payload)->status : "Not Available",
                        "approved_rate" => filled($item->approved_rate) ? $item->approved_rate : 'pending',
                        'total_charges_desc' => filled($item->charges) ? collect(json_decode($item->charges))->toArray() : 'pending',
                        "total_charges" => filled($item->charges) ? collect(json_decode($item->charges))->sum('amount') : 'pending',
                        "created" => $item->created_at,
                        "pending_actions" => $item->pending_action_list(),
                        "pending_action_comment" => $item->pending_action?->comments,
                        "has_pending_modification" => filled($item->pending_modification),
                        "modification_payload" => $item->latest_modification_payload(),
                    ];
                })->first();
                break;
            case "Bank Guarantee Request Creation - Open Bid" :
                $list = BankGuaranteeRequest::where('id', $this->activity_key)->get();
                $data = $list->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "guarantee_type" => $item->guarantee_type,
                        "request_type" => $item->request_type,
                        "requesting_as" => $item->requesting_as,
                        "bid_count" => $item->bank_guarantee_request_bids?->count() ?? 0,
                        "supplier" => $item->supplier?->company_name ?? 'Not Available',
                        "buyer" => $item->buyer?->company_name ?? $item->buyer_name,
                        "purchase_order_id" => $item->purchase_order_id,
                        "supporting_docs" => collect(json_decode($item->supporting_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "amount" => $item->guarantee_amount,
                        "purchase_order_url" => filled($item->purchase_order) ? $item->purchase_order->purchase_order_url : $item->purchase_order_url,
                        "status" => $item->pending_authorization ? json_decode($item->pending_authorization->payload)->status : "Not Available",
                        "created" => $item->created_at,
                        "auction_details" => json_decode($item->pending_authorization->payload)->request,
                    ];
                })->first();
                break;
            case "Purchase Order Creation - Open Bid":
                $poList = PurchaseOrder::where('id', $this->activity_key)->get();
                $data = $poList->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "purchase_order_number" => $item->purchase_order_number,
                        "line_item_details" => json_decode($item->line_item_details),
                        "status" => $item->status,
                        "buyer_comments" => $item->buyer_comments,
                        "delivery_date" => $item->delivery_date,
                        "payment_terms" => $item->payment_terms ?? 'Not included',
                        "name_restricted" => $item->name_restricted,
                        "delivery_address" => filled($item->delivery_address_id) ? DeliveryAddress::find($item->delivery_address_id) : 'To be provided',
                        "shipping_terms" => $item->shipping_terms,
                        "expected_warranty_period" => filled($item->expected_warranty_period) ? $item->expected_warranty_period . ' months' : 'Not Applicable',
                        "purchase_order_url" => $item->purchase_order_url ?? 'Not included',
                        "additional_docs" => json_decode($item->additional_docs),
                        "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                        "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single supplier' : 'Multiple suppliers',
                        "po_finance_line" => filled($item->po_finance_line_id) ? PurchaseOrderFinanceLine::find($item->po_finance_line_id)->line_unique_name : 'No financing line applied',
                        "allowed_suppliers" => $item->poLimitation() ,
                        'request_type' => 'Open for Bidding (Auction)',
                        'purchase_order_terms_url' => $item->purchase_order_terms_url ?? 'Not included',
                        'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                            ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                "supplier_name" => $i->supplier->company_name,
                                "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                "company_state" => $i->supplier->company_state
                            ]) : [],
                        'selected_group_name' => $item->selected_group_name,
                        'bid_bond_required' => $item->bid_bond_required,
                        "min_acceptable_quantity" => $item->min_acceptable_quantity,
                        "wht" => $item->wht,
                        "wht_deductible" => $item->wht_deductible,
                        "created" => $item->created_at,
                        "auction_duration" => json_decode($item->pending_authorization->payload)->request->auction_duration,
                        "auction_start" => json_decode($item->pending_authorization->payload)->request->auction_start,
                        "scheduled_start_date" => json_decode($item->pending_authorization->payload)->request->scheduled_start_date,
                        "auction_auto_restart" => json_decode($item->pending_authorization->payload)->request->auction_auto_restart,
                    ];
                })->first();
                break;
            case "Purchase Order Modification":
                $poList = PurchaseOrder::where('id', $this->activity_key)->get();
                $data = $poList->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "purchase_order_number" => $item->purchase_order_number,
                        "line_item_details" => json_decode($item->line_item_details),
                        "status" => $item->status,
                        "buyer_comments" => $item->buyer_comments,
                        "delivery_date" => $item->delivery_date,
                        "payment_terms" => $item->payment_terms ?? 'Not included',
                        "name_restricted" => $item->name_restricted,
                        "delivery_address" => filled($item->delivery_address_id) ? DeliveryAddress::find($item->delivery_address_id) : 'To be provided',
                        "shipping_terms" => $item->shipping_terms,
                        "expected_warranty_period" => filled($item->expected_warranty_period) ? $item->expected_warranty_period . ' months' : 'Not Applicable',
                        "purchase_order_url" => $item->purchase_order_url ?? 'Not included',
                        "additional_docs" => json_decode($item->additional_docs),
                        "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                        "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single supplier' : 'Multiple suppliers',
                        "po_finance_line" => filled($item->po_finance_line_id) ? PurchaseOrderFinanceLine::find($item->po_finance_line_id)->line_unique_name : 'No financing line applied',
                        "allowed_suppliers" => $item->poLimitation() ,
                        'request_type' => 'Open for Bidding (Auction)',
                        'purchase_order_terms_url' => $item->purchase_order_terms_url ?? 'Not included',
                        'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                            ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                "supplier_name" => $i->supplier->company_name,
                                "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                "company_state" => $i->supplier->company_state
                            ]) : [],
                        'selected_group_name' => $item->selected_group_name,
                        'bid_bond_required' => $item->bid_bond_required,
                        "min_acceptable_quantity" => $item->min_acceptable_quantity,
                        "wht" => $item->wht,
                        "wht_deductible" => $item->wht_deductible,
                        "created" => $item->created_at,
                        "auction_duration" => json_decode($item->pending_authorization->payload)->pending_linked_auction->auction_duration,
                        "auction_start" => json_decode($item->pending_authorization->payload)->pending_linked_auction->auction_start,
                        "scheduled_start_date" => json_decode($item->pending_authorization->payload)->pending_linked_auction->scheduled_start_date,
                        "auction_auto_restart" => json_decode($item->pending_authorization->payload)->pending_linked_auction->auction_auto_restart,
                        "old_values" => $item->pre_modification_values()
                    ];
                })->first();
                break;
            case "Purchase Order Auction Bid":
                if (Cache::has('active-auctions-supplier-primed')) {
                    $auction = collect(Cache::get('active-auctions-supplier-primed'))->sole(fn($item) => $item['id'] == $this->activity_key);
                }
                else {
                    $auction = collect([Auction::find($this->activity_key)])->transform(function ($item) {
                        $auction_parent = collect(Cache::get(Str::kebab($item->owner->company_name . '-pos-primed')))->firstWhere('id', $item->auction_transaction_key)
                            ?? collect([$item->parent_transaction])->transform(fn($item) => [
                                "id" => $item->id,
                                "purchase_order_number" => $item->purchase_order_number,
                                "request_type" => $item->request_type,
                                "bid_limitation" => $item->bid_limitation(),
                                "line_item_details" => json_decode($item->line_item_details),
                                "status" => $item->status,
                                "buyer_comments" => $item->buyer_comments,
                                "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                                "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                                "delivery_date" => $item->delivery_date,
                                "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                                "initiator" => $item->inputer_id,
                                "authorizer" => $item->authorizer_id,
                                "name_restricted" => $item->name_restricted,
                                "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                                "shipping_terms" => $item->shipping_terms,
                                'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                                "bid_count" => $item->purchase_order_bids->count() ?? 0,
                                "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item) {
                                    return ["data" => $item];
                                }),
                                "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                                "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                                "termination_reason" => $item->termination_reason,
                                "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                                "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                                "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                                'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                                    ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                        "id" => $i->supplier->id,
                                        "supplier_name" => $i->supplier->company_name,
                                        "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                        "company_state" => $i->supplier->company_state
                                    ]) : [],
                                'selected_group_name' => $item->selected_group_name,
                                "created" => $item->created_at,
                                "payment_terms" => $item->payment_terms,
                                "bid_bond_required" => $item->bid_bond_required,
                                "bpo_confirmed_at" => $item->bpo_confirmed_at,
                                "pending_actions" => $item->pending_action_list(),
                                "cancellation_rejection_comment" => $item->last_rejection_comment(),
                                "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                                "buyer_logo" => $item->buyer->logo_url,
                                "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                                "vat" => $item->vat,
                                "min_acceptable_quantity" => $item->min_acceptable_quantity,
                                "wht" => $item->wht,
                                "wht_deductible" => $item->wht_deductible,
                                "pre_modification_values" => $item->pre_modification_values(),
                                "buyer" => $item->buyer
                            ])->first();
                        $bidLimitation = match ($auction_parent['bid_limitation']) {
                            'All ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'own',
                            $auction_parent['selected_group_name'] . ' - supplier group' => 'group',
                            'Selected ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'selected',
                            'All ' . config('app.name') . ' suppliers' => 'all'
                        };
                        return [
                            'id' => $item->id,
                            'bids' => $item->bids,
                            'buyer' => $auction_parent['buyer']['company_name'],
                            'buyer_masked' => 'Tier ' . $auction_parent['buyer']['company_tier'] . ' buyer in ' . $auction_parent['buyer']['company_state'],
                            'purchase_order' => $auction_parent,
                            'auction_type' => $item->type->auction_type,
                            'buyer_tier' => $auction_parent['buyer']['company_tier'],
                            'duration' => $item->duration,
                            'status' => $item->status,
                            'starts' => $item->starts_at,
                            'ends' => $item->ends_at,
                            'deliver_to' => ($auction_parent['delivery_address'] !== 'To be provided') ? NigerianState::find($auction_parent['delivery_address']['state_id'])->state_name : 'TBA',
                            'net_total' => collect($auction_parent['line_item_details'])->sum(fn($item) => $item->price * $item->quantity),
                            'bid_count' => $item->bids->count(),
                            'bid_limitation' => $bidLimitation,
                            'restricted_to' => match ($bidLimitation) {
                                default => null,
                                'selected' => collect($auction_parent['selected_suppliers'])->pluck('id'),
                                'own' => collect(Company::find($auction_parent['buyer']['id'])->supplier_list)->pluck('supplier_id'),
                                'group' => collect(Company::find($auction_parent['buyer']['id'])->supplier_groups()[$auction_parent['selected_group_name']])->pluck('supplier_id')
                            }
                        ];
                    })->first();
                }
                $data = [
                    "bid" => json_decode($this->payload),
                    "auction" => $auction
                ];
                break;
            default: $data = null;
        }
        return $data;
    }

}
