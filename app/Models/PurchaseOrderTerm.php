<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderTerm extends Model
{
    use HasFactory;

    protected $fillable = [
        'buyer_id',
        'file_alias',
        'terms_url',
        'original_name',
        'uploaded_by'
    ];
}
