<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderFinanceLine extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'line_unique_name',
        'requested_amount',
        'availed_amount',
        'request_description',
        'proposed_tenure_days',
        'approved_tenure_days',
        'status',
        'closed_supplier_set',
        'suppliers_list',
    ];

    public function bank(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Bank::class);
    }
}
