<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Auction extends Model
{
    use HasFactory;

    protected $fillable = [
        'auction_transaction_key',
        'auction_type',
        'company_id',
        'auction_description',
        'related_docs',
        'auction_duration',
        'status',
        'starts_at',
        'ends_at',
        'bid_bond_required',
        'auto_restart'
    ];

    //protected $with = ['bids'];

    public function type(): BelongsTo
    {
        return $this->belongsTo(AuctionType::class, 'auction_type', 'id');
    }

    public function bids() {
        $i = $this->type->auction_type;
        switch ($i) {
            case "Bank Guarantee Request":
                return $this->hasMany(BankGuaranteeRequestBid::class);

            case "Purchase Order Tender":
                return $this->hasMany(PurchaseOrderBid::class);
        }
    }

    public function parent_transaction(): ?BelongsTo // for purchase orders
    {
        return $this->belongsTo(PurchaseOrder::class, 'auction_transaction_key', 'id');
    }

    public function parent_transaction_bg(): ?BelongsTo
    {
        return $this->belongsTo(BankGuaranteeRequest::class, 'auction_transaction_key', 'id');
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function current_best_offer() {
        ;
    }
}
