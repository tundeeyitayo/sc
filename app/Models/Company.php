<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

class Company extends Model
{
    use HasFactory;

    protected $with = (['subscription']);

    public function guarantees(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return match ($this->account_type) {
            "supplier" => $this->hasMany(BankGuaranteeRequest::class, 'supplier_id', 'id')->where('requesting_as', 'seller'),
            "buyer" => $this->hasMany(BankGuaranteeRequest::class, 'buyer_id', 'id'),
            "bank" => $this->hasMany(BankGuaranteeRequest::class, 'bank_id', 'id'),
            "buyer_supplier" => $this->hasMany(BankGuaranteeRequest::class, 'buyer_id', 'id')->where('requesting_as', 'buyer')
                ->orWhereRaw('supplier_id = ' . $this->id . ' && requesting_as = "seller"'),
        };
    }

    public function purchase_orders(): HasMany|BelongsToMany|Collection
    {
        return match ($this->account_type) {
            "supplier" => $this->belongsToMany(PurchaseOrder::class)->withPivot('is_selected')->using(CompanyPurchaseOrder::class),
            "buyer", "buyer_supplier" => $this->hasMany(PurchaseOrder::class, 'buyer_id', 'id')
        };

    }

    public function purchase_order_bids (): HasManyThrough|BelongsToMany
    {
        return match ($this->account_type) {
            "supplier" => $this->hasMany(PurchaseOrderBid::class, 'supplier_id', 'id'),
            "buyer" => $this->hasManyThrough(PurchaseOrderBid::class, PurchaseOrder::class, 'buyer_id', null, 'id', 'id'),
        };
    }

    public function purchase_order_bids_buyer_supplier (): \Illuminate\Database\Eloquent\Collection
    {
        $asBuyer = $this->hasManyThrough(PurchaseOrderBid::class, PurchaseOrder::class, 'buyer_id', null, 'id', 'id')
            ->whereNot('supplier_id',$this->id)->get();
        $asSeller = $this->hasMany(PurchaseOrderBid::class, 'supplier_id', 'id')->get();
        return $asBuyer->concat($asSeller->toArray());
    }

    public function active_purchase_orders(): Collection
    {
        return  collect($this->purchase_orders)->filter(fn($item) => $item->status == 'active' || $item->status == 'bidding opened');
    }

    public function uploaded_files(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UploadedFile::class);
    }

    public function activity_logs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CompanyActivityLog::class);
    }

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class);
    }

    public function authorizer_list(): Collection
    {
        return collect($this->users)->whereIn('role', ['authorizer','super_user']);
    }

    public function pending_authorizations(): \Illuminate\Database\Eloquent\Collection
    {
        return $this->activity_logs()->where('status', 'unauthorized')->where('authorization_required', 1)->get();
    }

    public function supplier_list (): ?\Illuminate\Database\Eloquent\Relations\HasMany
    {
        return collect(['buyer', 'buyer_supplier'])->contains($this->account_type)
            ? $this->hasMany(BuyerSupplier::class, 'buyer_id', 'id')
            : null ;
    }

    public function supplier_groups(): Collection
    {
        return collect($this->supplier_list)->groupBy('group_name');
    }

    public function purchase_order_terms(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UploadedFile::class)->where('section', 'purchase_order_terms');
    }

    public function active_po_finance_lines(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PurchaseOrderFinanceLine::class)->where('expires_at', '>', now())->whereNotNull('approved_at');
    }

    public function delivery_addresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(DeliveryAddress::class, 'buyer_id', 'id');
    }

    public function subscription (): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Subscription::class);
    }

    public function masked_name (): ?string
    {
        return ($this->account_type == 'buyer') || ($this->account_type == 'buyer_supplier') ? 'Tier ' . $this->company_tier . ' buyer in ' . $this->company_state : null;
    }

    public function is_buyer () : bool {
        return ($this->account_type == 'buyer') || ($this->account_type == 'buyer_supplier');
    }

}
