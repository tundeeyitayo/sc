<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        "sender",
        "room_id",
        "status",
        "message",
        "file",
        "reply"
    ];

    public function sent_by (): BelongsTo
    {
        return $this->belongsTo(User::class, 'sender', 'id');
    }

    public function room (): BelongsTo
    {
        return $this->belongsTo(Room::class);
    }
}
