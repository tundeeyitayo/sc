<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankGuaranteeRequestStatus extends Model
{
    use HasFactory;
}
