<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CompanyPurchaseOrder extends Pivot
{
    protected $appends = ['company'];
    protected function company(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, array $attributes) => Company::where('id', $attributes['company_id'])->get()->map->only([
                'id',
                'company_name',
                'company_address',
                'company_city',
                'company_state',
            ])->first(),
        );
    }
}
