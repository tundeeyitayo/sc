<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendingAction extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'pending_action_type_id',
        'pending_action_key',
        'fields',
        'comments'
    ];

    public function pending_action_type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PendingActionType::class);
    }
}
