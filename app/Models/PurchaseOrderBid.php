<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderBid extends Model
{
    use HasFactory;

    protected $fillable = [
        'auction_id',
        'purchase_order_id',
        'supplier_id',
        'line_item_details',
        'additional_comments',
        'additional_docs',
    ];

    public function supplier(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class, 'supplier_id', 'id');
    }

    public function auction(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Auction::class);
    }

    public function buyer() {
        return $this->auction->owner;
    }

    public function purchase_order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PurchaseOrder::class);
    }

}
