<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'uploaded_by',
        'file_alias',
        'file_stored_as',
        'section',
        'original_name'
    ];
}
