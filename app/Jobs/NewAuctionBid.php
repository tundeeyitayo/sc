<?php

namespace App\Jobs;

use App\Mail\AuthorizationPending;
use App\Mail\BidPlaced;
use App\Mail\NewBidReceived;
use App\Models\CompanyActivityLog;
use App\Models\PurchaseOrderBid;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Laravel\Jetstream\Mail\TeamInvitation;

class NewAuctionBid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public CompanyActivityLog $log, public ?PurchaseOrderBid $bid, public User $user, public bool $auth_required)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $log = $this->log;
        $user = $this->user;

        if (!$this->auth_required) {
            $bid = $this->bid;

            $ownerRecipients = $bid->buyer()->users;
            $bidderRecipients = $user->company->users;

            foreach ($ownerRecipients as $recipient) {
                Mail::to($recipient)->send(new NewBidReceived($bid, $recipient));
            }

            foreach ($bidderRecipients as $recipient) {
                Mail::to($recipient)->send(new BidPlaced($bid, $recipient));
            }
        }
        else {
            $bidderRecipients = $user->company->authorizer_list();

            foreach ($bidderRecipients as $recipient) {
                Mail::to($recipient)->send(new AuthorizationPending($log, $recipient));
            }
        }


    }
}
