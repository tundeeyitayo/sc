<?php

namespace App\Jobs;

use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\PurchaseOrderFinanceLine;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class SyncCompanyPoCaches implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $buyerCompanies = Company::where('account_type', 'buyer')->orWhere('account_type', 'buyer_supplier')->get();
        foreach ($buyerCompanies as $company) {
            if (blank(Cache::get(Str::kebab($company->company_name . '-pos')))
                || (collect(Cache::get(Str::kebab($company->company_name . '-pos')))->count() !== $company->purchase_orders->count()) )
            {
                $companyPos = $company->purchase_orders;
                Cache::forget(Str::kebab($company->company_name . '-pos'));
                Cache::put(Str::kebab($company->company_name . '-pos'), $companyPos);
                Cache::put(Str::kebab($company->company_name . '-pos-primed'), collect($companyPos)->sortByDesc('updated_at')
                    ->values()->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "purchase_order_number" => $item->purchase_order_number,
                        "request_type" => $item->request_type,
                        "bid_limitation" => $item->bid_limitation(),
                        "line_item_details" => json_decode($item->line_item_details),
                        "status" => $item->status,
                        "buyer_comments" => $item->buyer_comments,
                        "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                        "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                        "delivery_date" => $item->delivery_date,
                        "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                        "initiator" => $item->inputer_id,
                        "authorizer" => $item->authorizer_id,
                        "name_restricted" => $item->name_restricted,
                        "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                        "shipping_terms" => $item->shipping_terms,
                        'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                        "bid_count" => $item->purchase_order_bids->count() ?? 0,
                        "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                        "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                        "termination_reason" => $item->termination_reason,
                        "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                        "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                        "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                        'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                            ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                "id" => $i->supplier->id,
                                "supplier_name" => $i->supplier->company_name,
                                "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                "company_state" => $i->supplier->company_state
                            ]) : [],
                        'selected_group_name' => $item->selected_group_name,
                        "created" => $item->created_at,
                        "payment_terms" => $item->payment_terms,
                        "bid_bond_required" => $item->bid_bond_required,
                        "bpo_confirmed_at" => $item->bpo_confirmed_at,
                        "pending_actions" => $item->pending_action_list(),
                        "cancellation_rejection_comment" => $item->last_rejection_comment(),
                        "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                        "buyer_logo" => $item->buyer->logo_url,
                        "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                        "vat" => $item->vat,
                        "min_acceptable_quantity" => $item->min_acceptable_quantity,
                        "wht" => $item->wht,
                        "wht_deductible" => $item->wht_deductible,
                        "pre_modification_values" => $item->pre_modification_values(),
                        "buyer" => $item->buyer->only(['id', 'company_name', 'company_state', 'company_tier'])
                    ];
                }));
            }
        }
    }
}
