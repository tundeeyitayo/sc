<?php

namespace App\Jobs;

use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class ManageCompanyUsersCache implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Cache::forget('all_supplier_company_users');
        Cache::put('all_supplier_company_users', Company::with('users')->where('account_type', 'supplier')->orWhere('account_type', 'buyer_supplier')->get()->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten());
    }
}
