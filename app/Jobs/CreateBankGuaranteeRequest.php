<?php

namespace App\Jobs;

use App\Mail\AuthorizationPending;
use App\Mail\BankGuaranteeAuctionCreated;
use App\Mail\NewAuctionAvailable;
use App\Models\Auction;
use App\Models\BankGuaranteeRequest;
use App\Models\CompanyActivityLog;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class CreateBankGuaranteeRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public BankGuaranteeRequest $bg,
                                public ?Collection $bankList = null,
                                public bool $auth_required = false,
                                public ?Collection $auth_list = null,
                                public ?CompanyActivityLog $log = null,
                                public ?Auction $auction = null
    )
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (!$this->auth_required){
            foreach ($this->bankList as $recipient) {
                if ($this->auction->status == 'open') {
                    Mail::to($recipient)->send(new NewAuctionAvailable($this->auction, $recipient));
                }
            }
            foreach ($this->auth_list as $staff) {
                Mail::to($staff)->send(new BankGuaranteeAuctionCreated($this->auction, $staff));
            }
        }
        else {
            foreach ($this->auth_list as $recipient) {
                Mail::to($recipient)->send(new AuthorizationPending($this->log, $recipient));
            }
        }
    }
}
