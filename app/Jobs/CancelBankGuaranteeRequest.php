<?php

namespace App\Jobs;

use App\Mail\AuthorizationPending;
use App\Mail\BankGuaranteeRequestCancelled;
use App\Models\BankGuaranteeRequest;
use App\Models\CompanyActivityLog;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class CancelBankGuaranteeRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public BankGuaranteeRequest $bg,
                                public ?Collection $bankList = null,
                                public bool $auth_required = false,
                                public ?Collection $auth_list = null,
                                public ?CompanyActivityLog $log = null
    ){}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (!$this->auth_required){
            $userList = User::find($this->bankList);
            $requesterType = $this->bg->requesting_as == 'seller' ? 'supplier' : 'buyer';
            $cancelMessage = $this->bg->request_type == 'Preferred Bank' ? ' you will no longer be able to proceed with processing ' : ' bidding has been stopped for ';

            foreach ($userList as $recipient) {
                Mail::to($recipient)->send(new BankGuaranteeRequestCancelled($this->bg->$requesterType->company_name, $this->bg->id, $recipient, $cancelMessage));
            }
        }
        else {
            foreach ($this->auth_list as $recipient) {
                Mail::to($recipient)->send(new AuthorizationPending($this->log, $recipient));
            }
        }
    }
}
