<?php

namespace App\Jobs;

use App\Models\BankGuaranteeRequest;
use App\Models\UploadedFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class DeleteUnusedBankGuaranteeUploads implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $uploaded = UploadedFile::all()->pluck('file_stored_as');
        foreach (Storage::allFiles('public/bg_docs') as $file) {
            if (collect($uploaded)->map(function ($item) {
                return 'public/bg_docs/' . $item;
            })->doesntContain($file)){
                Storage::delete($file);
            }
        }

    }
}
