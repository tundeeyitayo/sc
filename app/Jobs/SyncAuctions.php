<?php

namespace App\Jobs;

use App\Events\AuctionClosed;
use App\Events\AuctionRestarted;
use App\Mail\BankGuaranteeAuctionCreated;
use App\Mail\BankGuaranteeRequestExpired;
use App\Mail\NewAuctionAvailable;
use App\Mail\PurchaseOrderAuctionCreated;
use App\Mail\PurchaseOrderRequestExpired;
use App\Models\Auction;
use App\Models\Bank;
use App\Models\BankGuaranteeRequest;
use App\Models\BankGuaranteeRequestBid;
use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\NigerianState;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderFinanceLine;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SyncAuctions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $dueForClosureAuctions = Auction::where('status', 'open')->where('ends_at', '<=', now())->get();
        $dueForOpenAuctions = Auction::where('status', 'scheduled')->where('starts_at', '<=', now())->get();
        $bgAuctionChangedCount = 0;
        $poAuctionChangedCount = 0;

        foreach ($dueForClosureAuctions as $item) {
            $company = Company::find($item->company_id);

            if (($item->auto_restart == 0 ) || (($item->auto_restart == 1) && ($item->auto_restart_count == 2))) {
                $item->status = 'expired';
                $item->save();
                switch ($item->type->auction_type) {
                    case 'Bank Guarantee Request':
                        $item->parent_transaction->status = 2;
                        $item->push();
                        $bgAuctionChangedCount += 1;
                        $this->rebuildBgCaches($item->parent_transaction->id, $company);
                        break;
                    case 'Purchase Order Tender':
                        $item->parent_transaction->status = 'expired';
                        $item->push();
                        $poAuctionChangedCount += 1;
                        $this->rebuildPoCaches($item->parent_transaction->id, $company);
                        break;
                }
                AuctionClosed::dispatch($item, $company);
            }
            elseif (($item->auto_restart == 1) && ($item->auto_restart_count < 2)) {
                $item->auto_restart_count += 1;
                $item->ends_at = Carbon::parse($item->ends_at)->addDays($item->auction_duration);
                $item->status = 'open';
                $item->save();
                switch ($item->type->auction_type) {
                    case 'Bank Guarantee Request':
                        $item->parent_transaction->status = 8;
                        $item->push();
                        $bgAuctionChangedCount += 1;
                        $this->rebuildBgCaches($item->parent_transaction->id, $company);
                        break;
                    case 'Purchase Order Tender':
                        $item->parent_transaction->status = "bidding opened";
                        $item->push();
                        $poAuctionChangedCount += 1;
                        $this->rebuildPoCaches($item->parent_transaction->id, $company);
                        break;
                }
                AuctionRestarted::dispatch($item, $company);
            }
        }

        foreach ($dueForOpenAuctions as $item) {
            $company = Company::find($item->company_id);

            switch ($item->type->auction_type) {
                case 'Bank Guarantee Request':
                    if ($company->use_authorizer == 0 || (($company->use_authorizer == 1) && blank($item->parent_transaction->pending_authorization))) {
                        $item->status = 'open';
                        $item->save();

                        $bg = $item->parent_transaction;
                        $bg->status = 8;
                        $bg->save();

                        $bankSuperUsers = Bank::with('super_users')->get()->pluck('super_users')->reject(fn($item) => empty($item))->flatten();
                        $staffList = $company->authorizer_list()->merge(
                            [User::find($company->activity_logs->where('activity_type_id', 3)->where('activity_key', $item->auction_transaction_key)->first()->inputted_by)]
                        );

                        foreach ($bankSuperUsers as $recipient) {
                            Mail::to($recipient)->send(new NewAuctionAvailable($item, $recipient));
                        }
                        foreach ($staffList as $staff) {
                            Mail::to($staff)->send(new BankGuaranteeAuctionCreated($item, $staff));
                        }
                    }
                    else {
                        $item->status = 'expired';
                        $item->save();

                        $bg = $item->parent_transaction;
                        $bg->status = 2;
                        $bg->save();

                        $staffList = $company->authorizer_list()->merge(
                            [User::find($company->activity_logs->where('activity_type_id', 3)->where('activity_key', $item->auction_transaction_key)->first()->inputted_by)]
                        );
                        foreach ($staffList as $staff) {
                            Mail::to($staff)->send(new BankGuaranteeRequestExpired($item, $staff));
                        }
                    }
                    $this->rebuildBgCaches($bg, $company);
                    break;
                case 'Purchase Order Tender':
                    if ($company->use_authorizer == 0 || (($company->use_authorizer == 1) && blank($item->parent_transaction->pending_authorization))) {
                        $item->status = 'open';
                        $item->save();

                        $purchase_order = $item->parent_transaction;
                        $purchase_order->status = 'bidding opened';
                        $purchase_order->save();
                        $poAuctionChangedCount += 1;

                        $supplierCompanyUsers = Cache::get('all_supplier_company_users', Company::with('users')->where('account_type', 'supplier')->orWhere('account_type', 'buyer_supplier')->get()->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten());
                        $supplierCompanyUsers = collect($supplierCompanyUsers)->reject(fn($item) => $item->company->id == $company->id);
                        $staffList = $company->authorizer_list()->merge(
                            [User::find($company->activity_logs->where('activity_type_id', 4)->where('activity_key', $item->auction_transaction_key)->first()->inputted_by)]
                        );

                        foreach ($supplierCompanyUsers as $recipient) {
                            Mail::to($recipient)->send(new NewAuctionAvailable($item, $recipient));
                        }
                        foreach ($staffList as $staff) {
                            Mail::to($staff)->send(new PurchaseOrderAuctionCreated($item, $staff));
                        }
                    }
                    else {
                        $item->status = 'expired';
                        $item->save();

                        $purchase_order = $item->parent_transaction;
                        $purchase_order->status = 'expired';
                        $purchase_order->save();
                        $poAuctionChangedCount += 1;

                        $staffList = $company->authorizer_list()->merge(
                            [User::find($company->activity_logs->where('activity_type_id', 4)->where('activity_key', $item->auction_transaction_key)->first()->inputted_by)]
                        );
                        foreach ($staffList as $staff) {
                            Mail::to($staff)->send(new PurchaseOrderRequestExpired($item, $staff));
                        }
                    }
                    $this->rebuildPoCaches($purchase_order->id, $company);
                    break;
            }
        }

        if ($poAuctionChangedCount > 0) {
            $this->updateSupplierAuctions();
        }
        if ($bgAuctionChangedCount > 0) {
            $this->updateBankAuctions();
        }




        /*Cache::forget('active-auctions-bank');
        Cache::put('active-auctions-bank', Auction::where('status', 'open')->get()
            ->filter(fn($item) => collect(json_decode($item->type->responder_models))->contains('bank'))->sortByDesc('created_at')
            ->values());*/

    }

    private function rebuildPoCaches ($po, $company): void
    {
        $purchaseOrder= PurchaseOrder::find($po);
        $companyPos = Cache::pull(Str::kebab($company->company_name . '-pos'));
        $primed = Cache::pull(Str::kebab($company->company_name . '-pos-primed'));

        $companyPos = collect($companyPos)->reject( function ($item) use ($purchaseOrder) {
            return $item->id == $purchaseOrder->id;
        })->push($purchaseOrder->withoutRelations());

        $primed = collect($primed)->reject( function ($item) use ($purchaseOrder) {
            return $item['id'] == $purchaseOrder->id;
        });

        $singlePrime = [
            "id" => $purchaseOrder->id,
            "purchase_order_number" => $purchaseOrder->purchase_order_number,
            "request_type" => $purchaseOrder->request_type,
            "bid_limitation" => $purchaseOrder->bid_limitation(),
            "line_item_details" => json_decode($purchaseOrder->line_item_details),
            "status" => $purchaseOrder->status,
            "buyer_comments" => $purchaseOrder->buyer_comments,
            "bank_payment_obligation_url" => $purchaseOrder->bank_payment_obligation_url,
            "bank_payment_obligation_id" => $purchaseOrder->bank_payment_obligation_id,
            "delivery_date" => $purchaseOrder->delivery_date,
            "purchase_order_terms_url" => $purchaseOrder->purchase_order_terms_url ?? 'Not Included',
            "initiator" => $purchaseOrder->inputer_id,
            "authorizer" => $purchaseOrder->authorizer_id,
            "name_restricted" => $purchaseOrder->name_restricted,
            "delivery_address" => DeliveryAddress::find($purchaseOrder->delivery_address_id) ?? 'To be provided',
            "shipping_terms" => $purchaseOrder->shipping_terms,
            'expected_warranty_period' => filled($purchaseOrder->expected_warranty_period) ? $purchaseOrder->expected_warranty_period : 'Not Applicable',
            "bid_count" => $purchaseOrder->purchase_order_bids->count() ?? 0,
            "additional_docs" => collect(json_decode($purchaseOrder->additional_docs))->map(function ($item){
                return ["data" => $item];
            }),
            "purchase_order_url" => $purchaseOrder->purchase_order_url ?? 'Not Included',
            "total_amount" => collect(json_decode($purchaseOrder->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
            "termination_reason" => $purchaseOrder->termination_reason,
            "net_price_discrepancy_tolerance" => $purchaseOrder->net_price_discrepancy_tolerance,
            "supplier_requirement" => $purchaseOrder->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
            "finance_line" => PurchaseOrderFinanceLine::find($purchaseOrder->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
            'selected_suppliers' => filled(BuyerSupplier::find(json_decode($purchaseOrder->selected_suppliers)))
                ? BuyerSupplier::whereIn('supplier_id', json_decode($purchaseOrder->selected_suppliers))->get()->map(fn($i) => [
                    "id" => $i->supplier->id,
                    "supplier_name" => $i->supplier->company_name,
                    "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                    "company_state" => $i->supplier->company_state
                ]) : [],
            'selected_group_name' => $purchaseOrder->selected_group_name,
            "created" => $purchaseOrder->created_at,
            "payment_terms" => $purchaseOrder->payment_terms,
            "bid_bond_required" => $purchaseOrder->bid_bond_required,
            "bpo_confirmed_at" => $purchaseOrder->bpo_confirmed_at,
            "pending_actions" => $purchaseOrder->pending_action_list(),
            "cancellation_rejection_comment" => $purchaseOrder->last_rejection_comment(),
            "linked_auction" => $purchaseOrder->linked_auction ?? $purchaseOrder->pending_linked_auction(),
            "buyer_logo" => $purchaseOrder->buyer->logo_url,
            "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $purchaseOrder->id)->where('is_selected', 1)->get(),
            "vat" => $purchaseOrder->vat,
            "min_acceptable_quantity" => $purchaseOrder->min_acceptable_quantity,
            "wht" => $purchaseOrder->wht,
            "wht_deductible" => $purchaseOrder->wht_deductible,
            "pre_modification_values" => $purchaseOrder->pre_modification_values(),
            "buyer" => $purchaseOrder->buyer->only(['id', 'company_name', 'company_state', 'company_tier'])
        ];
        $primed = collect([$singlePrime])->merge(collect($primed))->values();

        Cache::forever(Str::kebab($company->company_name . '-pos'), $companyPos);
        Cache::forever(Str::kebab($company->company_name . '-pos-primed'), $primed);
    }

    private function rebuildBgCaches ($bg, $company): void
    {
        $bgRequest = BankGuaranteeRequest::find($bg);
        $companyGuarantees = Cache::pull(Str::kebab($company->company_name . '-guarantees'));
        $primed = Cache::pull(Str::kebab($company->company_name . '-guarantees-primed'));

        $companyGuarantees = collect($companyGuarantees)->reject( function ($item) use ($bgRequest) {
            return $item->id == $bgRequest->id;
        })->push($bgRequest->withoutRelations());

        $primed = collect($primed)->reject( function ($item) use ($bgRequest) {
            return $item['id'] == $bgRequest->id;
        });

        $singlePrime = [
            "id" => $bgRequest->id,
            "bank" => $bgRequest->bank->bank_name ?? 'Pending',
            "request_type" => $bgRequest->request_type,
            "requesting_as" => $bgRequest->requesting_as,
            "bid_count" => $bgRequest->bank_guarantee_request_bids->count() ?? 0,
            "supplier" => $bgRequest->supplier->company_name ?? 'Not Available',
            "buyer" => $bgRequest->buyer->company_name ?? $bgRequest->buyer_name,
            "purchase_order_id" => $bgRequest->purchase_order_id,
            "expiration_date" => $bgRequest->expiration_date,
            "supporting_docs" => collect(json_decode($bgRequest->supporting_docs))->map(function ($item){
                return ["data" => $item];
            }),
            "approved_tenure_days" => $bgRequest->approved_tenure_days,
            "amount" => $bgRequest->guarantee_amount,
            "purchase_order_url" => filled($bgRequest->purchase_order) ? $bgRequest->purchase_order->purchase_order_url : $bgRequest->purchase_order_url,
            "status" => $bgRequest->currentStatus->label,
            "approved_rate" => filled($bgRequest->approved_rate) ? $bgRequest->approved_rate : 'pending',
            'total_charges_desc' => filled($bgRequest->charges) ? collect(json_decode($bgRequest->charges))->toArray() : 'pending',
            "total_charges" => filled($bgRequest->charges) ? collect(json_decode($bgRequest->charges))->sum('amount') : 'pending',
            "created" => $bgRequest->created_at,
            "pending_actions" => $bgRequest->pending_action_list(),
            "pending_action_comment" => $bgRequest->pending_action?->comments,
            "cancellation_rejection_comment" => $bgRequest->last_rejection_comment(),
            "has_pending_modification" => filled($bgRequest->pending_modification),
            "modification_payload" => $bgRequest->latest_modification_payload(),
            "guarantee_type" => $bgRequest->guarantee_type
        ];
        $primed = collect([$singlePrime])->merge(collect($primed))->values();

        Cache::forever(Str::kebab($company->company_name . '-guarantees'), $companyGuarantees);
        Cache::forever(Str::kebab($company->company_name . '-guarantees-primed'), $primed);}

    private function updateSupplierAuctions(): void
    {
        $activeSupplierAuctions = Auction::where('status', 'open')->get()
            ->filter(fn($item) => collect(json_decode($item->type->responder_models))->contains('supplier'))->sortByDesc('updated_at')
            ->values();

        $activeSupplierAuctionsPrimed = $activeSupplierAuctions->transform(function ($item) {
            $auction_parent = collect(Cache::get(Str::kebab($item->owner->company_name . '-pos-primed')))->firstWhere('id', $item->auction_transaction_key)
                ?? collect([$item->parent_transaction])->transform(fn($item) => [
                    "id" => $item->id,
                    "purchase_order_number" => $item->purchase_order_number,
                    "request_type" => $item->request_type,
                    "bid_limitation" => $item->bid_limitation(),
                    "line_item_details" => json_decode($item->line_item_details),
                    "status" => $item->status,
                    "buyer_comments" => $item->buyer_comments,
                    "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                    "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                    "delivery_date" => $item->delivery_date,
                    "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                    "initiator" => $item->inputer_id,
                    "authorizer" => $item->authorizer_id,
                    "name_restricted" => $item->name_restricted,
                    "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                    "shipping_terms" => $item->shipping_terms,
                    'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                    "bid_count" => $item->purchase_order_bids->count() ?? 0,
                    "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item) {
                        return ["data" => $item];
                    }),
                    "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                    "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                    "termination_reason" => $item->termination_reason,
                    "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                    "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                    "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                    'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                        ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                            "id" => $i->supplier->id,
                            "supplier_name" => $i->supplier->company_name,
                            "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                            "company_state" => $i->supplier->company_state
                        ]) : [],
                    'selected_group_name' => $item->selected_group_name,
                    "created" => $item->created_at,
                    "payment_terms" => $item->payment_terms,
                    "bid_bond_required" => $item->bid_bond_required,
                    "bpo_confirmed_at" => $item->bpo_confirmed_at,
                    "pending_actions" => $item->pending_action_list(),
                    "cancellation_rejection_comment" => $item->last_rejection_comment(),
                    "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                    "buyer_logo" => $item->buyer->logo_url,
                    "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                    "vat" => $item->vat,
                    "min_acceptable_quantity" => $item->min_acceptable_quantity,
                    "wht" => $item->wht,
                    "wht_deductible" => $item->wht_deductible,
                    "pre_modification_values" => $item->pre_modification_values(),
                    "buyer" => $item->buyer
                ])->first();
            $bidLimitation = match ($auction_parent['bid_limitation']) {
                'All ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'own',
                $auction_parent['selected_group_name'] . ' - supplier group' => 'group',
                'Selected ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'selected',
                'All ' . config('app.name') . ' suppliers' => 'all'
            };
            return [
                'id' => $item->id,
                'bids' => $item->bids,
                'buyer' => $auction_parent['buyer']['company_name'],
                'buyer_masked' => 'Tier ' . $auction_parent['buyer']['company_tier'] . ' buyer in ' . $auction_parent['buyer']['company_state'],
                'purchase_order' => $auction_parent,
                'auction_type' => $item->type->auction_type,
                'buyer_tier' => $auction_parent['buyer']['company_tier'],
                'duration' => $item->duration,
                'status' => $item->status,
                'starts' => $item->starts_at,
                'ends' => $item->ends_at,
                'deliver_to' => ($auction_parent['delivery_address'] !== 'To be provided') ? NigerianState::find($auction_parent['delivery_address']['state_id'])->state_name : 'TBA',
                'net_total' => collect($auction_parent['line_item_details'])->sum(fn($item) => $item->price * $item->quantity),
                'bid_count' => $item->bids->count(),
                'bid_limitation' => $bidLimitation,
                'restricted_to' => match ($bidLimitation) {
                    default => null,
                    'selected' => collect($auction_parent['selected_suppliers'])->pluck('id'),
                    'own' => collect(Company::find($auction_parent['buyer']['id'])->supplier_list)->pluck('supplier_id'),
                    'group' => collect(Company::find($auction_parent['buyer']['id'])->supplier_groups()[$auction_parent['selected_group_name']])->pluck('supplier_id')
                }
            ];
        });

        Cache::forget('active-auctions-supplier');
        Cache::put('active-auctions-supplier', $activeSupplierAuctions);

        Cache::forget('active-auctions-supplier-primed');
        Cache::put('active-auctions-supplier-primed', $activeSupplierAuctionsPrimed);
    }

    private function updateBankAuctions(): void
    {}
}
