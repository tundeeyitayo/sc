<?php

namespace App\Jobs;

use App\Models\CompanyActivityLog;
use App\Models\PurchaseOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class PurchaseOrderModified implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public PurchaseOrder $purchaseOrder, public Collection $authorizers, public CompanyActivityLog $log)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        foreach ($this->authorizers as $recipient) {
            Mail::to($recipient)->send(new \App\Mail\PurchaseOrderModified($this->purchaseOrder, $this->log, $recipient));
        }

    }
}
