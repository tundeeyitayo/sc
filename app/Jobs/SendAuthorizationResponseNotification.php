<?php

namespace App\Jobs;

use App\Mail\PendingAuthorizationResponse;
use App\Models\CompanyActivityLog;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendAuthorizationResponseNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public CompanyActivityLog $log)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $user = User::find($this->log->inputted_by);
        $resp = '';
        if (filled($this->log->authorized_by)) {
            $resp = 'approved' ;
        }
        elseif (filled($this->log->rejected_by)) {
            $resp = 'rejected' ;
        }
        if ($resp) {
            Mail::to($user)->send(new PendingAuthorizationResponse($this->log, $resp, $user->name));
        }

    }
}
