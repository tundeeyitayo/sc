<?php

namespace App\Jobs;

use App\Mail\AuthorizationPending;
use App\Mail\NewAuctionAvailable;
use App\Mail\PurchaseOrderAuctionCreated;
use App\Models\Auction;
use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyActivityLog;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\NigerianState;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderFinanceLine;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class CreatePurchaseOrderRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(public PurchaseOrder $purchaseOrder,
                                public ?Collection $recipientList = null,
                                public bool $auth_required = false,
                                public ?Collection $auth_list = null,
                                public ?CompanyActivityLog $log = null,
                                public ?Auction $auction = null)
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (($this->purchaseOrder->request_type == "Open for Bidding (Auction)") && !$this->auth_required){
            $filteredList = collect($this->recipientList)->reject(fn($item) => $item->company_id == $this->purchaseOrder->buyer_id)->all();
            foreach ($filteredList as $recipient) {
                if ($this->auction->status == 'open') {
                    Mail::to($recipient)->send(new NewAuctionAvailable($this->auction, $recipient));
                }
            }
            foreach ($this->auth_list as $staff) {
                Mail::to($staff)->send(new PurchaseOrderAuctionCreated($this->auction, $staff));
            }
            $this->updateSupplierAuctions();
        }
        elseif(($this->purchaseOrder->request_type == "Preferred Supplier") && !$this->auth_required) {

        }
        elseif ($this->auth_required) {
            foreach ($this->auth_list as $recipient) {
                Mail::to($recipient)->send(new AuthorizationPending($this->log, $recipient));
            }
        }
    }

    private function updateSupplierAuctions(): void
    {
        $activeSupplierAuctions = Auction::where('status', 'open')->get()
            ->filter(fn($item) => collect(json_decode($item->type->responder_models))->contains('supplier'))->sortByDesc('updated_at')
            ->values();

        $activeSupplierAuctionsPrimed = $activeSupplierAuctions->transform(function ($item) {
            $auction_parent = collect(Cache::get(Str::kebab($item->owner->company_name . '-pos-primed')))->firstWhere('id', $item->auction_transaction_key)
                ?? collect([$item->parent_transaction])->transform(fn($item) => [
                    "id" => $item->id,
                    "purchase_order_number" => $item->purchase_order_number,
                    "request_type" => $item->request_type,
                    "bid_limitation" => $item->bid_limitation(),
                    "line_item_details" => json_decode($item->line_item_details),
                    "status" => $item->status,
                    "buyer_comments" => $item->buyer_comments,
                    "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                    "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                    "delivery_date" => $item->delivery_date,
                    "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                    "initiator" => $item->inputer_id,
                    "authorizer" => $item->authorizer_id,
                    "name_restricted" => $item->name_restricted,
                    "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                    "shipping_terms" => $item->shipping_terms,
                    'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                    "bid_count" => $item->purchase_order_bids->count() ?? 0,
                    "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item) {
                        return ["data" => $item];
                    }),
                    "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                    "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                    "termination_reason" => $item->termination_reason,
                    "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                    "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                    "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                    'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                        ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                            "id" => $i->supplier->id,
                            "supplier_name" => $i->supplier->company_name,
                            "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                            "company_state" => $i->supplier->company_state
                        ]) : [],
                    'selected_group_name' => $item->selected_group_name,
                    "created" => $item->created_at,
                    "payment_terms" => $item->payment_terms,
                    "bid_bond_required" => $item->bid_bond_required,
                    "bpo_confirmed_at" => $item->bpo_confirmed_at,
                    "pending_actions" => $item->pending_action_list(),
                    "cancellation_rejection_comment" => $item->last_rejection_comment(),
                    "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                    "buyer_logo" => $item->buyer->logo_url,
                    "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                    "vat" => $item->vat,
                    "min_acceptable_quantity" => $item->min_acceptable_quantity,
                    "wht" => $item->wht,
                    "wht_deductible" => $item->wht_deductible,
                    "pre_modification_values" => $item->pre_modification_values(),
                    "buyer" => $item->buyer
                ])->first();
            $bidLimitation = match ($auction_parent['bid_limitation']) {
                'All ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'own',
                $auction_parent['selected_group_name'] . ' - supplier group' => 'group',
                'Selected ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'selected',
                'All ' . config('app.name') . ' suppliers' => 'all'
            };
            return [
                'id' => $item->id,
                'bids' => $item->bids,
                'buyer' => $auction_parent['buyer']['company_name'],
                'buyer_masked' => 'Tier ' . $auction_parent['buyer']['company_tier'] . ' buyer in ' . $auction_parent['buyer']['company_state'],
                'purchase_order' => $auction_parent,
                'auction_type' => $item->type->auction_type,
                'buyer_tier' => $auction_parent['buyer']['company_tier'],
                'duration' => $item->duration,
                'status' => $item->status,
                'starts' => $item->starts_at,
                'ends' => $item->ends_at,
                'deliver_to' => ($auction_parent['delivery_address'] !== 'To be provided') ? NigerianState::find($auction_parent['delivery_address']['state_id'])->state_name : 'TBA',
                'net_total' => collect($auction_parent['line_item_details'])->sum(fn($item) => $item->price * $item->quantity),
                'bid_count' => $item->bids->count(),
                'bid_limitation' => $bidLimitation,
                'restricted_to' => match ($bidLimitation) {
                    default => null,
                    'selected' => collect($auction_parent['selected_suppliers'])->pluck('id'),
                    'own' => collect(Company::find($auction_parent['buyer']['id'])->supplier_list)->pluck('supplier_id'),
                    'group' => collect(Company::find($auction_parent['buyer']['id'])->supplier_groups()[$auction_parent['selected_group_name']])->pluck('supplier_id')
                }
            ];
        });

        Cache::forget('active-auctions-supplier');
        Cache::put('active-auctions-supplier', $activeSupplierAuctions);

        Cache::forget('active-auctions-supplier-primed');
        Cache::put('active-auctions-supplier-primed', $activeSupplierAuctionsPrimed);
    }
}
