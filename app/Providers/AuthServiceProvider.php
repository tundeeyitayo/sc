<?php

namespace App\Providers;

use App\Models\Auction;
use App\Models\BankActivityLog;
use App\Models\CompanyActivityLog;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //user
        Gate::define('request-bg', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "buyer") || ($user->company->account_type == "supplier")) && ($user->role !== "super_user") && $user->is_active());
        Gate::define('request-distributor-finance', fn(User $user) => (($user->company->account_type == "buyer_supplier") || $user->company->account_type == "supplier") && ($user->is_active()) && ($user->role !== "super_user"));
        Gate::define('request-inventory-finance', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "supplier")) && $user->is_active() && ($user->role !== "super_user"));
        Gate::define('create-invoice', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "supplier")) && $user->is_active() && ($user->role !== "super_user"));
        Gate::define('create-purchase-order', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "buyer")) && $user->is_active()  && ($user->role !== "super_user"));
        Gate::define('view-supplier-auctions', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "supplier")) && $user->is_active());
        Gate::define('view-buyer-auctions', fn(User $user) => ((($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "buyer")) && $user->is_active() ));
        Gate::define('view-selected-buyer-auction', fn(User $user, Auction $auction) => ((($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "buyer")) && $user->is_active() && ($auction->company_id == $user->company->id) ));
        Gate::define('view-bank-auctions', fn(User $user) => ($user->is_bank_user()));
        Gate::define('add-supplier', fn(User $user) => (($user->company->account_type == "buyer_supplier") || ($user->company->account_type == "buyer")) && $user->is_active() && ($user->role !== "super_user"));
        Gate::define('authorize-pending-transaction', fn(User $user, CompanyActivityLog $log) => ($user->company->use_authorizer == 1) && ($user->role == "authorizer" || $user->role == "super_user") && ($log->company_id == $user->company->id) && ($log->inputted_by !== $user->id) && $user->is_active());
        Gate::define('authorize-transactions', fn(User $user) => ($user->company->use_authorizer == 1) && ($user->role == "authorizer" || $user->role == "super_user") && $user->is_active());

        //bank
        Gate::define('process-bank-bg', fn(User $user) => $user->is_bank_user() && ($user->role == "inputer") && $user->is_active());
        Gate::define('process-bank-distributor-finance', fn(User $user) => $user->is_bank_user() && ($user->role == "inputer") && $user->is_active());
        Gate::define('process-bank-inventory-finance', fn(User $user) => $user->is_bank_user() && ($user->role == "inputer") && $user->is_active());
        Gate::define('process-bank-invoice', fn(User $user) => $user->is_bank_user() && ($user->role == "inputer") && $user->is_active());
        Gate::define('authorize-bank-pending-transaction', fn(User $user, BankActivityLog $log) => $user->is_bank_user() && (($user->role == "authorizer") || ($user->role == "super_user")) && ($log->bank_id == $user->bank_id) && ($log->inputted_by !== $user->id) && $user->is_active());
    }
}
