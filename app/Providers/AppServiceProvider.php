<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Inertia\Inertia;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->environment('local')) {
            $this->app->register(\Laravel\Telescope\TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        Inertia::share([
            'menu' => function () {
                $menu = [];

                if (Gate::allows('authorize-transactions')) {
                    $menu["Pending Authorizations"] = [
                        [
                            'title' => 'Authorize Pending Transactions',
                            'uri' => 'pending_authorizations',
                            'actionRequired' => function () {
                                return Auth::check() ? Auth::user()->company->pending_authorizations()->count() > 0 : false;
                            },
                            'actionCount' => function () {
                                return Auth::check() ? Auth::user()->company->pending_authorizations()->count() : 0;
                            }

                        ],
                    ];
                }

                if (Gate::allows('request-bg')) {
                    $menu["Bank Guarantee"] = [
                        [
                            'title' => 'Bank Guarantee Requests',
                            'uri' => 'guarantee.index',
                            'actionRequired' => function () {
                                return Auth::check() && collect(Str::kebab(Auth::user()->company->company_name . '-guarantees'))->pluck('status')->contains(3);
                            },
                            'actionCount' => function () {
                                return Auth::check() ? collect(Str::kebab(Auth::user()->company->company_name . '-guarantees'))->where('status', '=', 3)->count() : 0;
                            }

                        ],
                        [
                            'title' => 'Create Bank Guarantee Request (Open For Bidding)',
                            'uri' => 'guarantee.create',
                        ],
                        [
                            'title' => 'Request Guarantee From Preferred Bank',
                            'uri' => 'guarantee.create_closed',
                        ]
                    ];
                }

                if (Gate::allows('create-invoice')) {
                    $menu["Invoice"] = [
                        [
                            'title' => 'Manage Invoices',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Create Invoice',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Start Invoice Financing (Pre-Shipment) Request Auction',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Request Invoice Financing (Pre-Shipment) From Preferred Bank',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Start Invoice Discounting (Post-Shipment) Request Auction',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Request Invoice Discounting (Post-Shipment) From Preferred Bank',
                            'uri' => 'guarantee.index',
                        ]
                    ];
                }

                if (Gate::allows('request-distributor-finance')) {
                    $menu["Distributor Finance"] = [
                        [
                            'title' => 'Distributor Financing Requests',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Start Distributor Financing Request Auction',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Request Distributor Financing From Preferred Bank',
                            'uri' => 'guarantee.index',
                        ]
                    ];
                }

                if (Gate::allows('request-inventory-finance')) {
                    $menu["Inventory Finance"] = [
                        [
                            'title' => 'Inventory Financing Requests',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Start Inventory Financing Request Auction',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Request Inventory Financing From Preferred Bank',
                            'uri' => 'guarantee.index',
                        ]
                    ];
                }

                if (Gate::allows('view-supplier-auctions')) {
                    $menu["Purchase Order"] = [
                        [
                            'title' => 'Manage Purchase Orders',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Find Open For Bidding Purchase Orders',
                            'uri' => 'auctions.index',
                            'actionRequired' => function () {
                                return collect(Cache::get('active-auctions-supplier-primed'))->count() > 0;
                            },
                            'actionCount' => function () {
                                return Auth::check() ? collect(Cache::get('active-auctions-supplier-primed'))->count() : 0;
                            }
                        ]
                    ];
                }

                if (Gate::allows('create-purchase-order')) {
                    $menu["Purchase Order"] = collect([
                        [
                            'title' => 'Create Purchase Order (Open For Bidding)',
                            'uri' => 'purchase_orders.create',
                        ],
                        [
                            'title' => 'Create Purchase Order (Preferred Supplier)',
                            'uri' => 'purchase_orders.create_closed',
                        ],
                        [
                            'title' => Gate::allows('view-supplier-auctions') ? 'Manage Own Purchase Orders' : 'Manage Purchase Orders',
                            'uri' => 'purchase_orders.show',
                            'actionRequired' => function () {
                                return Auth::check() && collect(Cache::get(Str::kebab(Auth::user()->company->company_name . '-pos')))->pluck('status')->contains('action required');
                            },
                            'actionCount' => function () {
                                return Auth::check() ? collect(Cache::get(Str::kebab(Auth::user()->company->company_name . '-pos')))->where('status', 'action required')->count() : 0;
                            }
                        ]
                    ])->concat(
                        Gate::allows('view-supplier-auctions')
                            ? [
                                [
                                    'title' => 'Manage Incoming Purchase Orders',
                                    'uri' => 'guarantee.index',
                                ],
                                [
                                    'title' => 'Find Open For Bidding Purchase Orders',
                                    'uri' => 'auctions.index',
                                    'actionRequired' => function () {
                                        return collect(Cache::get('active-auctions-supplier-primed'))->count() > 0;
                                    },
                                    'actionCount' => function () {
                                        return Auth::check() ? collect(Cache::get('active-auctions-supplier-primed'))->count() : 0;
                                    }
                                ],

                              ]
                            : [])->toArray();
                }

                if (Gate::allows('add-supplier')) {
                    $menu["Suppliers"] = [
                        [
                            'title' => 'Add Suppliers',
                            'uri' => 'guarantee.index',
                        ],
                        [
                            'title' => 'Manage Suppliers',
                            'uri' => 'guarantee.index',
                        ],
                    ];
                }

                return $menu;
            },
            'routeName' => function () {
                return match (true) {
                    request()->routeIs('dashboard') => 'Dashboard',
                    request()->routeIs('pending_authorizations') => 'Pending Authorizations',
                    request()->routeIs('guarantee*') => 'Bank Guarantee',
                    request()->routeIs('invoices*') => 'Invoice',
                    request()->routeIs('distributor_finance*') => 'Distributor Finance',
                    request()->routeIs('inventory_finance*') => 'Inventory Finance',
                    request()->routeIs('purchase_orders*') => 'Purchase Order',
                    request()->routeIs('settings') => 'Settings',
                    request()->routeIs('subscriptions') => 'Subscriptions',
                    request()->routeIs('suppliers') => 'Suppliers',
                    default => '',
                };
            },
            'activeSubMenu' => function () {
                return request()->route()->getName();
            },
            'userType' => function () {
                if (Auth::check()) {
                    return Auth::user()->company->account_type;
                } else {
                    return null;
                }
            },
            'appName' => config("app.name"),
            'logoAvatar' => fn() => Auth::check()
                ? 'https://ui-avatars.com/api/?name='.urlencode(Auth::user()->company->company_name).'&color=7F9CF5&background=EBF4FF'
                : null
        ]);

    }
}
