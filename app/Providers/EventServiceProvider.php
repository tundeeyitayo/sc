<?php

namespace App\Providers;

use App\Events\AuctionClosed;
use App\Events\AuctionCreated;
use App\Events\AuctionRestarted;
use App\Events\DeliveryAddressCreated;
use App\Events\PurchaseOrderCreated;
use App\Listeners\AuctionClosureActions;
use App\Listeners\AuctionCreatedActions;
use App\Listeners\AuctionRestartedActions;
use App\Listeners\DeliveryAddressCreationActions;
use App\Listeners\PurchaseOrderCreatedActions;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        AuctionClosed::class => [
            AuctionClosureActions::class
        ],
        AuctionRestarted::class => [
            AuctionRestartedActions::class
        ],
        AuctionCreated::class => [
            AuctionCreatedActions::class
        ],
        PurchaseOrderCreated::class => [
            PurchaseOrderCreatedActions::class
        ],
        DeliveryAddressCreated::class => [
            DeliveryAddressCreationActions::class
        ]
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
