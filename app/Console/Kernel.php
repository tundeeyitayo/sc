<?php

namespace App\Console;

use App\Jobs\DeleteUnusedBankGuaranteeUploads;
use App\Jobs\ManageCompanyUsersCache;
use App\Jobs\SyncAuctions;
use App\Jobs\SyncCompanyPoCaches;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
        $schedule->job(new DeleteUnusedBankGuaranteeUploads())->dailyAt("02:16");
        $schedule->job(new SyncCompanyPoCaches())->dailyAt("04:00");
        $schedule->job(new SyncAuctions())->everyFiveMinutes();
        $schedule->job(new ManageCompanyUsersCache())->dailyAt("03:02");

    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
