<?php

namespace App\Http\Requests;

use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Precognition;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class NewBankGuaranteeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('request-bg');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'guarantee_amount' => ['bail','required','numeric','min:0.01'],
            'purchase_order_id' => ['bail','nullable','numeric','exists:purchase_orders,id'],
            'purchase_order_url' => Rule::when(
                function (Fluent $input){
                    return !is_file($input->get('purchase_order_url')) || (is_file($input->get('purchase_order_url')) && Str::of($input->get('purchase_order_url')->getClientOriginalName())->length() > 80 );
                },
                [function (string $attribute, mixed $value, Closure $fail) {
                    if (!is_file($value)) {
                        return ['required'];
                    }
                    else {
                        $fail("Purchase order document name is too long.");
                    }
                }] ,
                ['bail', 'mimes:jpg,pdf,jpeg','max:2048']),
            'supporting_docs' => ['array','max:3'],
            'supporting_docs.*.data' => Rule::forEach(function ( $value, string $attribute) {
                if (is_file($value) && (Str::of($value->getClientOriginalName())->length() < 80)) {
                    return ['bail', 'mimes:jpg,pdf,jpeg', 'max:2048'];
                }
                else{
                    return [
                        function (string $attribute, mixed $value, Closure $fail) {
                            if (!is_file($value)) {
                                return ['required'];
                            }
                            else {
                                $fail("Supporting document :position file name is too long.");
                            }

                        }];
                }
            })
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'purchase_order_id' => 'purchase order',
            'purchase_order_url' => 'purchase order document',
            'supporting_docs' => 'supporting documents',
            'supporting_docs.*.data' => 'supporting document :position'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'guarantee_amount.required' => 'Guarantee amount is required',
            'supporting_docs.*.data.required' => 'Supporting document :position is required',
            'supporting_docs.*.data.mimes' => 'Supporting document :position must be in either jpg, jpeg or pdf format',
            'supporting_docs.*.data.max' => 'Supporting document :position file size cannot exceed 2MB',
            'purchase_order_url.max' => 'Purchase order document file size cannot exceed 2MB',
        ];
    }

    /**
     * Handle a validation, bypasses original trait.
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateResolved(): void
    {
        /*//
            }*/
        $this->prepareForValidation();

        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        }

        $instance = $this->getValidatorInstance();

        if ($this->isPrecognitive()) {
            if ($this->safe()->collect()->keys()->contains('purchase_order_url')){
                foreach ($this->safe()->only(['purchase_order_url']) as $key => $value) {
                    if ($value instanceof UploadedFile) {
                        $extension = ($value)->guessExtension();
                        $alias = ($value)->getClientOriginalName();
                        $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                        ($value)->storeAs('public/bg_docs', $storeAs);
                        \App\Models\UploadedFile::create([
                            'company_id' => Auth::user()->company->id,
                            'uploaded_by' => Auth::id(),
                            'section' => 'bank_guarantee_requests',
                            'original_name' => $alias,
                            'file_alias' => \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                                ? Str::before($alias, ('.' . $value->getClientOriginalExtension())) . '_'. substr(time(), -5) . '.' .$extension
                                : $alias,
                            'file_stored_as' => $storeAs
                        ]);
                    }
                }
            }

            if ($this->safe()->collect()->keys()->contains('supporting_docs')){
                foreach ($this->safe()->only(['supporting_docs'])['supporting_docs'] as $key => $value) {
                    if ($value['data'] instanceof UploadedFile) {
                        $extension = ($value['data'])->guessExtension();
                        $alias = ($value['data'])->getClientOriginalName();
                        $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                        ($value['data'])->storeAs('public/bg_docs', $storeAs);
                        \App\Models\UploadedFile::create([
                            'company_id' => Auth::user()->company->id,
                            'uploaded_by' => Auth::id(),
                            'section' => 'bank_guarantee_requests',
                            'original_name' => $alias,
                            'file_alias' => \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                                ? Str::before($alias, ('.' . ($value['data'])->getClientOriginalExtension())) . '_'. substr(time(),-5) . '.' .$extension
                                : $alias,
                            'file_stored_as' => $storeAs
                        ]);
                    }
                }
            }

            $instance->after(Precognition::afterValidationHook($this));
        }

        if ($instance->fails()) {
            $this->failedValidation($instance);
        }

        $this->passedValidation();
    }
}
