<?php

namespace App\Http\Requests;

use Closure;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Precognition;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class ModifyPurchaseOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('create-purchase-order');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            "bid_limitation" => ['nullable', 'in:all,own,group,selected'],
            "active_line" => ['nullable', 'in:none,existing'],
            "finance_line" => ['required_if:active_line,existing', Rule::in(Auth::user()->company->active_po_finance_lines->pluck('line_unique_name')->all())],
            "supplier_group" => ['required_if:bid_limitation,group', 'nullable', 'string'],
            "selected_suppliers" => ['array', 'min:3'],
            "line_item_details" => ['array', 'min:1'],
            "line_item_details.*.type" => ['required', 'in:product,service'],
            "line_item_details.*.description" => ['required', 'string', 'max:400'],
            "line_item_details.*.price" => ['required', 'numeric', 'min:0.01'],
            "line_item_details.*.quantity" => ['required', 'numeric', 'min:0.01'],
            "line_item_details.*.unit" => ['required', 'string'],
            "x_payment_days" => ['required_if:payment_terms,x-delivery', 'required_if:payment_terms,x-confirmation', 'numeric', 'min:1'],
            "shipping_terms_text" => ['required_if:shipping_terms,other', 'nullable', 'numeric', 'min:1'],
            'purchase_order_url' => Rule::when(
                function (Fluent $input){
                    return !is_file($input->get('purchase_order_url')) || (is_file($input->get('purchase_order_url')) && Str::of($input->get('purchase_order_url')->getClientOriginalName())->length() > 80 );
                },
                [function (string $attribute, mixed $value, Closure $fail) {
                    if (!is_file($value)) {
                        return ['required'];
                    }
                    else {
                        $fail("Purchase order document name is too long.");
                    }
                }] ,
                ['bail', 'mimes:jpg,pdf,jpeg','max:2048']),
            "bank_payment_obligation_url" => Rule::when(
                function (){
                    return Auth::user()->company->company_tier > 2;
                },
                ['required', 'mimes:jpg,pdf,jpeg','max:2048'] ,
                ['nullable']),
            "terms" => ['required', 'in:existing,new,none'],
            "purchase_order_terms_url" => Rule::when(
                function (Fluent $input){
                    return !is_file($input->get('purchase_order_terms_url')) || (is_file($input->get('purchase_order_terms_url')) && Str::of($input->get('purchase_order_terms_url')->getClientOriginalName())->length() > 80 );
                },
                [function (string $attribute, mixed $value, Closure $fail) {
                    if (!is_file($value)) {
                        return ['required'];
                    }
                    else {
                        $fail("Purchase order terms document name is too long.");
                    }
                }] ,
                ['required_if:terms,new','mimes:jpg,pdf,jpeg','max:2048']),
            'additional_docs' => ['array','max:3'],
            'additional_docs.*.data' => Rule::forEach(function ( $value, string $attribute) {
                if (is_file($value) && (Str::of($value->getClientOriginalName())->length() < 80)) {
                    return ['bail', 'mimes:jpg,pdf,jpeg', 'max:2048'];
                }
                else{
                    return [
                        function (string $attribute, mixed $value, Closure $fail) {
                            if (!is_file($value)) {
                                return ['required'];
                            }
                            else {
                                $fail("Supporting document :position file name is too long.");
                            }

                        }];
                }
            }),
            "min_acceptable_quantity" => ['required_if:supplier_requirement,multiple', 'numeric'],
            "wht" => ['required_if:wht_deductible,1', 'numeric', 'max:10', 'min:0'],
            "buyer_comments" => ['nullable', 'string', 'max:1000'],
            "scheduled_start_date" => ['nullable', 'required_if:auction_start,schedule', 'date:format:Y-m-d H:i']
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'bid_limitation' => 'allowed bidders',
            'line_item_details.*.type' => 'line item :position type',
            'line_item_details.*.description' => 'line item :position description',
            'line_item_details.*.price' => 'line item :position unit price',
            'line_item_details.*.quantity' => 'line item :position quantity',
            'line_item_details.*.unit' => 'line item :position unit',
            'bank_payment_obligation_url' => 'bank payment obligation or equivalent document',
            'terms' => 'purchase order terms/conditions',
            'purchase_order_terms_url' => 'purchase order terms/conditions document',
            'purchase_order_url' => 'scanned purchase order document',
            'additional_docs.*.data' => 'additional document :position',
            'po_number' => 'purchase order number',
            'wht' => 'withholding tax',
            'x_payment_days' => 'payment delay duration'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'bid_limitation.in' => 'Unknown allowed bidder selection',
            'supplier_group.required_if' => 'Supplier group is required when restricting purchase order to a group',
            'purchase_order_terms_url.required_if' => 'Please upload a new purchase order terms/conditions document',
            'purchase_order_terms_url.unique' => 'A purchase order terms/conditions document with the same name already exists',
            'wht.required_if' => 'Withholding Tax value is required'
        ];
    }


    /**
     * Handle a validation, bypasses original trait.
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateResolved(): void
    {
        /*//
            }*/
        $this->prepareForValidation();

        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        }

        $instance = $this->getValidatorInstance();

        if ($this->isPrecognitive()) {
            if ($this->safe()->collect()->keys()->contains('purchase_order_terms_url')){
                foreach ($this->safe()->only(['purchase_order_terms_url']) as $key => $value) {
                    if ($value instanceof UploadedFile) {
                        $extension = ($value)->guessExtension();
                        $alias = ($value)->getClientOriginalName();
                        $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                        ($value)->storeAs('public/po_terms', $storeAs);
                        \App\Models\UploadedFile::create([
                            'company_id' => Auth::user()->company->id,
                            'uploaded_by' => Auth::id(),
                            'section' => 'purchase_order_terms',
                            'original_name' => $alias,
                            'file_alias' => \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                                ? Str::before($alias, ('.' . $value->getClientOriginalExtension())) . '_'. substr(time(), -5) . '.' .$extension
                                : $alias,
                            'file_stored_as' => $storeAs
                        ]);
                    }
                }
            }

            if ($this->safe()->collect()->keys()->contains('purchase_order_url')){
                foreach ($this->safe()->only(['purchase_order_url']) as $key => $value) {
                    if ($value instanceof UploadedFile) {
                        $extension = ($value)->guessExtension();
                        $alias = ($value)->getClientOriginalName();
                        $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                        ($value)->storeAs('public/po_docs', $storeAs);
                        \App\Models\UploadedFile::create([
                            'company_id' => Auth::user()->company->id,
                            'uploaded_by' => Auth::id(),
                            'section' => 'purchase_order_docs',
                            'original_name' => $alias,
                            'file_alias' => \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                                ? Str::before($alias, ('.' . $value->getClientOriginalExtension())) . '_'. substr(time(), -5) . '.' .$extension
                                : $alias,
                            'file_stored_as' => $storeAs
                        ]); }
                }
            }

            if ($this->safe()->collect()->keys()->contains('additional_docs')){
                foreach ($this->safe()->only(['additional_docs'])['additional_docs'] as $key => $value) {
                    if ($value['data'] instanceof UploadedFile) {
                        $extension = ($value['data'])->guessExtension();
                        $alias = ($value['data'])->getClientOriginalName();
                        $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                        ($value['data'])->storeAs('public/po_docs', $storeAs);
                        \App\Models\UploadedFile::create([
                            'company_id' => Auth::user()->company->id,
                            'uploaded_by' => Auth::id(),
                            'section' => 'purchase_order_docs',
                            'original_name' => $alias,
                            'file_alias' => \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                                ? Str::before($alias, ('.' . ($value['data'])->getClientOriginalExtension())) . '_'. substr(time(),-5) . '.' .$extension
                                : $alias,
                            'file_stored_as' => $storeAs
                        ]);
                    }
                }
            }

            $instance->after(Precognition::afterValidationHook($this));
        }

        if ($instance->fails()) {
            $this->failedValidation($instance);
        }

        $this->passedValidation();
    }
}
