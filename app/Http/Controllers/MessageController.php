<?php

namespace App\Http\Controllers;

use App\Events\ChatMessageSent;
use App\Models\ActiveSession;
use App\Models\Company;
use App\Models\Message;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderBid;
use App\Models\Room;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Inertia;

class MessageController extends Controller
{
    public function index (Request $request, $sender = null, $chatTo = null): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Inertia\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        if (!Auth::user()->is_bank_user()) {
            if (filled($sender) && filled($chatTo)) {
                $sender = Crypt::decryptString($sender);
                $chatTo = Crypt::decryptString($chatTo);
                if (($sender == Auth::user()->company_id) && Auth::user()->company->is_buyer()){
                    $room = Room::whereJsonContains('subscribers', (string) $sender)->whereJsonContains('subscribers', (string) $chatTo)->first();
                    $roomId = filled($room) ? $room->id : null;

                    if (empty($roomId)) {
                        $room = Room::create([
                            "room_name" => Company::find($sender)->company_name . config('app.name') . '-CHATROOM' . Company::find($chatTo)->company_name,
                            "subscribers" => json_encode([(string) $sender, (string) $chatTo])
                        ]);
                        $roomId = $room->id;
                    }
                }
                else {
                    $request = new Request;
                    $request->session()->flash('flash.banner', 'You are not permitted to access requested resource.');
                    $request->session()->flash('flash.bannerStyle', 'danger');
                    return redirect('/dashboard');
                }
            }
            else {
                $room = null;
                $roomId = null;
            }

            $userCompany = Auth::user()->company;

            $rooms = collect(Room::whereJsonContains('subscribers', (string) Auth::user()->company_id)->get())->transform(function ($item) use ($userCompany) {
                $otherCoy = Company::find((int)collect(json_decode($item->subscribers))->reject(fn($i) => $i ==  (string) $userCompany->id)->first());
                $lastMessage = collect($item->messages)?->last() ?? [];
                return
                    [
                        "roomId" => (string) $item->id,
                        "roomName" => Str::replace(',', '', Str::replace(config('app.name') . '-CHATROOM', '', Str::replace($userCompany->company_name, '', $item->room_name))),
                        "avatar" => filled($otherCoy->logo_url) ? ('/storage/company_logos/' . $otherCoy->logo_url) : ('/storage/company_avatars/' . $otherCoy->default_logo),
                        "unreadCount" => $item->messages->where('status', 'saved')->where('sender', '<>', Auth::id())->count(),
                        "index" => filled($lastMessage) ? $lastMessage->updated_at : 0,
                        "lastMessage" => filled($lastMessage) ? [
                          "_id" => (string) $lastMessage->id,
                          "content" => $lastMessage->message,
                          "senderId" => (string) $lastMessage->sender,
                          "username" => Str::before($lastMessage->sent_by->name, ' ') . '.' . ucfirst(Str::substr(Str::after($lastMessage->sent_by->name, ' '), 0, 1)) . ' of ' . ucfirst($lastMessage->sent_by->company->company_name),
                          "timestamp" => Carbon::parse($lastMessage->created_at)->format('h:i'),
                          "saved" => $lastMessage->status == 'saved',
                          "distributed" => $lastMessage->status == 'distributed',
                          "seen" => $lastMessage->status == 'seen',
                          "new" => $lastMessage->status == 'new'
                        ] : [],
                        "users" => collect($userCompany->users)->merge(collect($otherCoy->users))->transform(fn($user) => [
                            "_id" => (string) $user->id,
                            "username" => Str::before($user->name, ' ') . '.' . ucfirst(Str::substr(Str::after($user->name, ' '), 0, 1)) . ' of ' . ucfirst($user->company->company_name),
                            "avatar" => '/storage/user_avatars/' . $user->avatar,
                            "status" => [
                                "state" => ActiveSession::pluck('user_id')->contains($user->id) ? 'online' : 'offline'
                            ]
                        ]),
                        "typingUsers" => [],
                        "userRole" => (int) collect(json_decode($item->subscribers))->first() == Auth::user()->company_id ? 'buyer' : 'counterparty'
                    ];
            });

            if (filled($room)) {
                if (!Cache::has('convo-' . $room->id)) {
                    Cache::put('convo-' . $room->id, collect($room->messages)
                        ->transform(function ($item) {
                            $file = filled($item->file) ? json_decode($item->file) : null;
                            $reply = filled($item->reply) ? json_decode($item->reply) : null;
                            $reply_file = filled($item->reply) && filled($reply->files) ? $reply->files: null;

                            return [
                                "_id" => (string) $item->id,
                                "indexId" => $item->id,
                                "content" => $item->message,
                                "senderId" => (string) $item->sender,
                                "username" => Str::before($item->sent_by->name, ' ') . '.' . ucfirst(Str::substr(Str::after($item->sent_by->name, ' '), 0, 1)) . ' of ' . ucfirst($item->sent_by->company->company_name),
                                "avatar" => '/storage/user_avatars/' . $item->sent_by->avatar,
                                "date" => Carbon::parse($item->created_at)->format("d M"),
                                "timestamp" => Carbon::parse($item->created_at)->format("h:i"),
                                "system" => $item->status == "system",
                                "saved" => $item->status == "saved",
                                "distributed" => $item->status == "distributed",
                                "seen" => $item->status == "seen",
                                "failure" => $item->status == "failed",
                                "deleted" => false,
                                "disableActions" => false,
                                "disableReactions" => true,
                                "files" => filled($file) ?
                                    [[
                                        "name" => $file->name,
                                        "size" => $file->size,
                                        "type" => $file->type,
                                        "audio" => false,
                                        "duration" => null,
                                        "url" => filled($file->url) ? ('/storage/messaging/' . $file->url) : null,
                                    ]]
                                    : null,
                                "reactions" => [],
                                "replyMessage" => filled($reply) ? [
                                    "content" => $reply->content,
                                    "senderId" => (string) $reply->senderId,
                                    "files" => filled($reply->files) ?
                                        [[
                                            "name" => $reply_file->name,
                                            "size" => $reply_file->size,
                                            "type" => $reply_file->type,
                                            "audio" => false,
                                            "duration" => null,
                                            "url" => filled($reply_file->url) ? ('/storage/messaging/' . $reply_file->url) : null,
                                        ]]
                                        : null
                                ] : null
                            ];
                        })
                        ->values() );
                }
            }

            return Inertia::render('Messaging/Show', [
                "currentUserId" => (string) Auth::id(),
                "rooms" => $rooms,
                "roomId" => filled($roomId) ? (string) $roomId : null,
                "messages" => []
            ]);
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to access requested resource.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function fetch_messages (Request $request): array|Collection
    {
        //$room = Room::find($request->room);
        $roomMessageCache = Cache::rememberForever('convo-' . $request->room, fn() => collect(Room::find($request->room)->messages)
            ->transform(function ($item) {
                $file = filled($item->file) ? json_decode($item->file) : null;
                $reply = filled($item->reply) ? json_decode($item->reply) : null;
                $reply_file = filled($item->reply) && filled($reply->files) ? $reply->files: null;

                return [
                    "_id" => (string) $item->id,
                    "indexId" => $item->id,
                    "content" => $item->message,
                    "senderId" => (string) $item->sender,
                    "username" => Str::before($item->sent_by->name, ' ') . '.' . ucfirst(Str::substr(Str::after($item->sent_by->name, ' '), 0, 1)) . ' of ' . ucfirst($item->sent_by->company->company_name),
                    "avatar" => '/storage/user_avatars/' . $item->sent_by->avatar,
                    "date" => Carbon::parse($item->created_at)->format("d M"),
                    "timestamp" => Carbon::parse($item->created_at)->format("h:i"),
                    "system" => $item->status == "system",
                    "saved" => $item->status == "saved",
                    "distributed" => $item->status == "distributed",
                    "seen" => $item->status == "seen",
                    "failure" => $item->status == "failed",
                    "deleted" => false,
                    "disableActions" => false,
                    "disableReactions" => true,
                    "files" => filled($file) ?
                        [[
                            "name" => $file->name,
                            "size" => $file->size,
                            "type" => $file->type,
                            "audio" => false,
                            "duration" => null,
                            "url" => filled($file->url) ? ('/storage/messaging/' . $file->url) : null,
                        ]]
                        : null,
                    "reactions" => [],
                    "replyMessage" => filled($reply) ? [
                        "content" => $reply->content,
                        "senderId" => (string) $reply->senderId,
                        "files" => filled($reply->files) ?
                            [[
                                "name" => $reply_file->name,
                                "size" => $reply_file->size,
                                "type" => $reply_file->type,
                                "audio" => false,
                                "duration" => null,
                                "url" => filled($reply_file->url) ? ('/storage/messaging/' . $reply_file->url) : null,
                            ]]
                            : null
                    ] : null
                ];
            })
            ->values() );
        $pageSize = 30;

        if (collect($roomMessageCache)->count() > 0) {
            $dbOffset = collect($roomMessageCache)->count() - ($request->currentMessagePage * $pageSize);
            if ($dbOffset < 0) $dbOffset = 0;

            $seenMessageStart = DB::table('messages')->where('room_id', $request->room)->offset($dbOffset)->first()->id ?? null;
            if (filled($seenMessageStart)) {
                DB::table("messages")->where('room_id', $request->room)->where("id", ">=", $seenMessageStart)->whereNot("sender", Auth::id())->limit($pageSize)->update(["status" => "seen"]);
                $messageCache = collect(Cache::pull('convo-' . $request->room))->all();

                $cacheUpdateCount = (collect($roomMessageCache)->count() - ($request->currentMessagePage * $pageSize)) >= 0
                    ? $pageSize
                    : $pageSize + (collect($roomMessageCache)->count() - ($request->currentMessagePage * $pageSize));
                for ($i = $dbOffset; $i < ($cacheUpdateCount + $dbOffset); $i++) {
                    if ($messageCache[$i]['senderId'] != (string) Auth::id()) {
                        $messageCache[$i]['saved'] = false;
                        $messageCache[$i]['seen'] = true;
                    }

                }
                Cache::put('convo-' . $request->room, $messageCache);
            }

            $modifiedMessages = collect(Cache::get('convo-' . $request->room));
            $retrieved_messages = $modifiedMessages
                ->reverse()
                ->take($pageSize * $request->currentMessagePage)
                ->reverse()
                ->values() ;
            $total_messages = $modifiedMessages->count();
            $unread_count = $modifiedMessages->filter(fn($item) => ($item['saved'] == 'true') && ($item['senderId'] != (string) Auth::id()))->count();
            return [
                "messages" => $retrieved_messages,
                "message_count" => $total_messages,
                "unread_count" => $unread_count
            ];
        }
        else{
            return [];
        }

    }

    public function send_message (Request $request) {
        if (filled($request->input('files'))) {
            $file = collect($request->input('files'))->first();
            if ((filled($file))) {
                $content = $request->file('files')[0]['blob'];
                $extension = $file['extension'];
                $alias = Str::kebab(Auth::user()->company->company_name) . '_' . time() . '.' .$extension;
                if (collect(['pdf', 'jpeg', 'jpg'])->contains($extension) && $file['size'] <= 2048000) {
                    Storage::putFileAs('public/messaging/', new File ($content), $alias);
                $file = [
                    "name" => $alias,
                    "size" => $file['size'],
                    "type" => $extension,
                    "file_url" => $alias,
                    ];
                }
                else {
                    abort(403,'You can only send jpg, jpeg or pdf files not exceeding 2MB.');
                }
            }
            else $file = null;
        }
        else $file = null;

        if (filled($request->replyMessage) && isset($request->replyMessage['files']) && filled($request->replyMessage['files'])) {
            $reply_file = collect($request->replyMessage['files'])->first();
        }
        elseif (filled($request->replyMessage) && isset($request->replyMessage['replyMessage']) && isset($request->replyMessage['replyMessage']['files'])) {
            $reply_file = collect($request->replyMessage['replyMessage']['files'])->first();
        }
        else $reply_file = null;

        $message = Message::create([
            "sender" => Auth::id(),
            "room_id" => $request->roomId,
            "status" => 'saved',
            "message" => $request->message_content,
            "file" => filled($file) ? json_encode(
                [
                "name" => $file['name'],
                "size" => $file['size'],
                "type" => $file['type'],
                "audio" => false,
                "duration" => null,
                "url" => $file['file_url'],
            ]) : null,
            "reply" => filled($request->replyMessage) ? json_encode([
                "content" => $request->replyMessage['content'],
                "senderId" => Auth::id(),
                "files" => filled($reply_file) ? [
                        "name" => $reply_file['name'],
                        "size" => $reply_file['size'],
                        "type" => $reply_file['type'],
                        "audio" => false,
                        "duration" => null,
                        "url" => Str::remove("/storage/messaging/", $reply_file['url']),
                    ] : null
            ]) : null
        ]);

        $message_primed = collect([$message])->transform(function ($item) use ($request) {
            $file = filled($item->file) ? json_decode($item->file) : null;
            $reply = filled($item->reply) ? json_decode($item->reply) : null;
            $reply_file = filled($item->reply) && filled($reply->files) ? $reply->files: null;
            return [
                "_id" => (string) $item->id,
                "roomId" => $request->roomId,
                "recipientCompanyId" => collect(json_decode($item->room->subscribers))->map(fn($i) => (int) $i)->reject(fn($m) => $m == Auth::user()->company_id)->first(),
                "indexId" => $item->id,
                "content" => $item->message,
                "senderId" => (string) $item->sender,
                "username" => Str::before($item->sent_by->name, ' ') . '.' . ucfirst(Str::substr(Str::after($item->sent_by->name, ' '), 0, 1)) . ' of ' . ucfirst($item->sent_by->company->company_name),
                "avatar" => '/storage/user_avatars/' . $item->sent_by->avatar,
                "date" => Carbon::parse($item->created_at)->format("d M"),
                "timestamp" => Carbon::parse($item->created_at)->format("h:i"),
                "system" => $item->status == "system",
                "saved" => $item->status == "saved",
                "distributed" => $item->status == "distributed",
                "seen" => $item->status == "seen",
                "failure" => $item->status == "failed",
                "deleted" => false,
                "disableActions" => false,
                "disableReactions" => true,
                "files" => filled($file) ?
                    [[
                        "name" => $file->name,
                        "size" => $file->size,
                        "type" => $file->type,
                        "audio" => false,
                        "duration" => null,
                        "url" => filled($file->url) ? ('/storage/messaging/' . $file->url) : null,
                    ]]
                    : null,
                "reactions" => [],
                "replyMessage" => filled($reply) ? [
                    "content" => $reply->content,
                    "senderId" => (string) $reply->senderId,
                    "files" => filled($reply->files) ?
                        [[
                            "name" => $reply_file->name,
                            "size" => $reply_file->size,
                            "type" => $reply_file->type,
                            "audio" => false,
                            "duration" => null,
                            "url" => filled($reply_file->url) ? ('/storage/messaging/' . $reply_file->url) : null,
                        ]]
                        : null
                ] : null
            ];
        })->first();

        $messageCache = Cache::pull('convo-' . $request->roomId);
        $messageCache = collect($messageCache)->push(collect([$message_primed])->transform(fn($item) => [
            "_id" => $item['_id'],
            "indexId" => $item['indexId'],
            "content" => $item['content'],
            "senderId" => $item['senderId'],
            "username" => $item['username'],
            "avatar" => $item['avatar'],
            "date" => $item['date'],
            "timestamp" => $item['timestamp'],
            "system" => $item['system'],
            "saved" => $item['saved'],
            "distributed" => $item['distributed'],
            "seen" => $item['seen'],
            "failure" => $item['failure'],
            "deleted" => false,
            "disableActions" => false,
            "disableReactions" => true,
            "files" => $item['files'],
            "reactions" => [],
            "replyMessage" => $item['replyMessage']
        ])->first());
        Cache::put('convo-' . $request->roomId, $messageCache);

        ChatMessageSent::dispatch($message, $message_primed);
    }

    public function fetch_related_purchase_orders (Request $request) {
        $company = Company::find(Auth::user()->company_id);
        $counterparty = collect(json_decode(Room::find($request->roomId)->subscribers))->map(fn($item) => (int) $item)
            ->reject(fn($item) => $item == Auth::user()->company_id)->first();
        return match ($request->userRole) {
            "buyer" => $company->hasManyThrough(PurchaseOrderBid::class, PurchaseOrder::class, 'buyer_id', null, 'id', 'id')
                    ->where('supplier_id', $counterparty)->get()->groupBy('purchase_order_id')
                    ->transform(fn($item) => collect($item)->last())
                    ->transform(function ($i) {
                        $po = $i->purchase_order;
                            return [
                                'po_number' => $po->purchase_order_number,
                                "delivery_date" => $po->delivery_date,
                                "po_line_items" => json_decode($po->line_item_details),
                                "bid_date" => $i->created_at,
                                "supplier_id" => $i->supplier_id,
                                "auction_id" => $i->auction_id
                            ];
                        })->values()->sortByDesc('po_created'),
            'counterparty' => $company->hasMany(PurchaseOrderBid::class, 'supplier_id', 'id')->get()->groupBy('purchase_order_id')
                    ->transform(fn($item) => collect($item)->last())
                    ->transform(function ($i) {
                        $po = $i->purchase_order;
                        return [
                            'po_number' => $po->purchase_order_number,
                            "delivery_date" => $po->delivery_date,
                            "po_line_items" => json_decode($po->line_item_details),
                            "bid_date" => $i->created_at,
                            "supplier_id" => $i->supplier_id,
                            "auction_id" => $i->auction_id
                        ];
                    })->values()->sortByDesc('po_created')
        };
    }
}
