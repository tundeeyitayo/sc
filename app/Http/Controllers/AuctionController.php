<?php

namespace App\Http\Controllers;

use App\Jobs\NewAuctionBid;
use App\Mail\PurchaseOrderBidRejected;
use App\Models\Auction;
use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyActivityLog;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\NigerianState;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderBid;
use App\Models\PurchaseOrderFinanceLine;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class AuctionController extends Controller
{
    //cache: active-auctions-supplier, active-auctions-bank
    public function index (Request $request) {
        if (Gate::allows('view-supplier-auctions')) {
            $activeAuctions = Cache::rememberForever('active-auctions-supplier', fn() => Auction::where('status', 'open')->get()->filter(fn($item) => collect(json_decode($item->type->responder_models))->contains('supplier'))->sortByDesc('updated_at')
                ->values());
            $activeAuctionsPrimed = Cache::rememberForever('active-auctions-supplier-primed', function () use ($activeAuctions) {
                return $activeAuctions->transform(function ($item) {
                    $auction_parent = collect(Cache::get(Str::kebab($item->owner->company_name . '-pos-primed')))->firstWhere('id', $item->auction_transaction_key)
                        ?? collect([$item->parent_transaction])->transform(fn($item) => [
                            "id" => $item->id,
                            "purchase_order_number" => $item->purchase_order_number,
                            "request_type" => $item->request_type,
                            "bid_limitation" => $item->bid_limitation(),
                            "line_item_details" => json_decode($item->line_item_details),
                            "status" => $item->status,
                            "buyer_comments" => $item->buyer_comments,
                            "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                            "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                            "delivery_date" => $item->delivery_date,
                            "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                            "initiator" => $item->inputer_id,
                            "authorizer" => $item->authorizer_id,
                            "name_restricted" => $item->name_restricted,
                            "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                            "shipping_terms" => $item->shipping_terms,
                            'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                            "bid_count" => $item->purchase_order_bids->count() ?? 0,
                            "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item) {
                                return ["data" => $item];
                            }),
                            "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                            "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                            "termination_reason" => $item->termination_reason,
                            "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                            "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                            "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                            'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                                ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                    "id" => $i->supplier->id,
                                    "supplier_name" => $i->supplier->company_name,
                                    "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                    "company_state" => $i->supplier->company_state
                                ]) : [],
                            'selected_group_name' => $item->selected_group_name,
                            "created" => $item->created_at,
                            "payment_terms" => $item->payment_terms,
                            "bid_bond_required" => $item->bid_bond_required,
                            "bpo_confirmed_at" => $item->bpo_confirmed_at,
                            "pending_actions" => $item->pending_action_list(),
                            "cancellation_rejection_comment" => $item->last_rejection_comment(),
                            "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                            "buyer_logo" => $item->buyer->logo_url,
                            "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                            "vat" => $item->vat,
                            "min_acceptable_quantity" => $item->min_acceptable_quantity,
                            "wht" => $item->wht,
                            "wht_deductible" => $item->wht_deductible,
                            "pre_modification_values" => $item->pre_modification_values(),
                            "buyer" => $item->buyer
                        ])->first();
                    $bidLimitation = match ($auction_parent['bid_limitation']) {
                        'All ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'own',
                        $auction_parent['selected_group_name'] . ' - supplier group' => 'group',
                        'Selected ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'selected',
                        'All ' . config('app.name') . ' suppliers' => 'all'
                    };
                    return [
                        'id' => $item->id,
                        'bids' => $item->bids,
                        'buyer' => $auction_parent['buyer']['company_name'],
                        'buyer_masked' => 'Tier ' . $auction_parent['buyer']['company_tier'] . ' buyer in ' . $auction_parent['buyer']['company_state'],
                        'purchase_order' => $auction_parent,
                        'auction_type' => $item->type->auction_type,
                        'buyer_tier' => $auction_parent['buyer']['company_tier'],
                        'duration' => $item->duration,
                        'status' => $item->status,
                        'starts' => $item->starts_at,
                        'ends' => $item->ends_at,
                        'deliver_to' => ($auction_parent['delivery_address'] !== 'To be provided') ? NigerianState::find($auction_parent['delivery_address']['state_id'])->state_name : 'TBA',
                        'net_total' => collect($auction_parent['line_item_details'])->sum(fn($item) => $item->price * $item->quantity),
                        'bid_count' => $item->bids->count(),
                        'bid_limitation' => $bidLimitation,
                        'restricted_to' => match ($bidLimitation) {
                            default => null,
                            'selected' => collect($auction_parent['selected_suppliers'])->pluck('id'),
                            'own' => collect(Company::find($auction_parent['buyer']['id'])->supplier_list)->pluck('supplier_id'),
                            'group' => collect(Company::find($auction_parent['buyer']['id'])->supplier_groups()[$auction_parent['selected_group_name']])->pluck('supplier_id')
                        }
                    ];
                });
            });
            $table_fields = ['auction_type', 'buyer', 'net_total', 'deliver_to', 'ends', 'bid_count'];
            $sortable_fields = ['auction_type', 'deliver_to', 'net_total', 'ends', 'bid_count'];
            $date_fields = ['starts', 'ends'];
            $currency_fields = ["net_total"];
            $attributes = [
                "id" => "ID",
                "buyer" => "Buyer",
                "auction_type" => "Auction Type",
                "deliver_to" => "Deliver To",
                "net_total" => "Net Total Amount",
                "bid_count" => "Bids",
                "ends" => "Ends",
            ];

            $header_fields = [];
            foreach (collect(collect($activeAuctionsPrimed)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = array_merge([
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ], collect($date_fields)->contains($field) ? ['formatType' => 'relative' ] : [],
                    collect($currency_fields)->contains($field) ? ['formatType' => 'currency' ] : []);

            }
            $header_fields[] = "__slot:actions:details";

            return Inertia::render('Auctions/Show', [
                "data" => $activeAuctionsPrimed,
                "headerFields" => $header_fields,
                "nigerianStates" => NigerianState::all()->pluck('state_name', 'id'),
                "searchableBuyers" => collect($activeAuctionsPrimed)->filter(fn($item) => $item['purchase_order']['name_restricted'] == 0)->pluck('buyer')->unique()
            ]);
        }
        elseif (Gate::allows('view-bank-auctions')) {
            $activeAuctions = Cache::rememberForever('active-auctions-bank', function () {
                return Auction::all()->filter(fn($item) => collect(json_decode($item->type->responder_models))->contains('bank'));
            });
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function placeBid (Request $request): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        if (Gate::allows('view-supplier-auctions')) {
            $auction = Auction::find($request->id);
            if ((Auth::user()->company->use_authorizer == 1) &&
                Auth::user()->company->activity_logs->where('activity_key', $auction->id)->where('activity_type_id', 6)->where('status', 'unauthorized')->count() > 0){
                $request->session()->flash('flash.banner', 'Your company has a pending unauthorized bid for this auction');
                $request->session()->flash('flash.bannerStyle', 'danger');
                return back();
            }
            $po = $auction->parent_transaction;
            $supplier_requirement = $po->supplier_requirement;
            $min_acceptable_quantity = $po->min_acceptable_quantity;
            $po_line_items = json_decode($po->line_item_details);
            $price_variance = $po->net_price_discrepancy_tolerance;

            $validator = Validator::make($request->all(), [
                "id" => "numeric|exists:auctions,id",
                "line_items" => ['array', 'min:1'],
                "line_items.*.quantity" => Rule::foreach(function ( $value, $attribute) use ($po_line_items, $supplier_requirement, $min_acceptable_quantity) {
                    $line = ($po_line_items)[(int) Str::after((Str::beforeLast($attribute,'.')), '.')];
                    if ($supplier_requirement == 'multiple') {
                        if (($line->quantity - $line->fulfilled_quantity) < ceil(($min_acceptable_quantity/100) * $line->quantity)) {
                            return ['required', 'numeric', 'min:'. ($line->quantity - $line->fulfilled_quantity), 'max:'. ($line->quantity - $line->fulfilled_quantity)];
                        }
                        else {
                            return ['required', 'numeric', 'min:'. ceil(($min_acceptable_quantity/100) * $line->quantity), 'max:' . ($line->quantity - $line->fulfilled_quantity)];
                        }
                    }
                    else {
                        return ['numeric'];
                    }
                }),
                "line_items.*.price" => Rule::forEach(function ($value, $attribute) use ($po_line_items, $price_variance) {
                    $line = ($po_line_items)[(int) Str::after((Str::beforeLast($attribute,'.')), '.')];
                    $max_price = (1 + ($price_variance/100)) * $line->price;
                    return ['required', 'numeric', 'lte:'. $max_price];
                }),
                "comment" => ['nullable', 'string', 'max:400']
            ], [
                  "line_items.*.quantity.numeric" => 'Line item :position quantity is required.',
                  "Supporting document :position must be a file of type jpg, jpeg or pdf"
            ], [
                "line_items.*.price" => "line item :position unit price",
                "line_items.*.quantity" => "line item :position quantity",
                "supporting_docs.*.data" => "supporting document :position"
            ]);
            $validator->sometimes('supporting_docs.*.data', ['file', 'mimes:jpg,jpeg,pdf', 'max:2048'], fn($item) => is_file($item->name));
            $validator->sometimes('supporting_docs.*.data', ['string', 'ends_with:jpg,jpeg,pdf'], fn($item) => is_string($item));
            $validator->validate();
            $supportingDocs = [];
            $i = 0;
            $removedFiles = $request->removed_files;

            foreach ($request->supporting_docs as $key => $value) {
                if ($value['data'] instanceof UploadedFile) {
                    $i++;
                    $extension = ($value['data'])->guessExtension();
                    $alias = ($value['data'])->getClientOriginalName();
                    $storeAs = Str::kebab(Auth::user()->company->company_name) . '_' . $i . time() . '.' .$extension;
                    ($value['data'])->storeAs('public/auction_docs', $storeAs);
                    $fileAlias = \App\Models\UploadedFile::all()->pluck('file_alias')->contains($alias)
                        ? Str::before($alias, ('.' . ($value['data'])->getClientOriginalExtension())) . '_'. substr(time(), -5) . '.' .$extension
                        : $alias;
                    \App\Models\UploadedFile::create([
                        'company_id' => Auth::user()->company->id,
                        'uploaded_by' => Auth::id(),
                        'section' => 'auctions',
                        'original_name' => $alias,
                        'file_alias' => $fileAlias,
                        'file_stored_as' => $storeAs
                    ]);
                    $supportingDocs = collect($supportingDocs)->push($fileAlias);
                }
                elseif (is_string($value['data'])) {
                    $supportingDocs = collect($supportingDocs)->push($value['data']);
                }
            }

            if (Auth::user()->company->use_authorizer == 1) {
                $log = CompanyActivityLog::create([
                    'activity_type_id' => 6,
                    'activity_key' => $auction->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 1,
                    'payload' => json_encode([
                        "id" => $auction->id,
                        "request" => collect([$request->all()])->map(fn($item) => [
                            "id" => $item['id'],
                            "line_items" => $item['line_items'],
                            "supporting_docs" => $supportingDocs->map(fn($i) => [
                                'data' => $i
                            ]),
                            "comment" => $item['comment']
                        ])->first(),
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);
                NewAuctionBid::dispatch($log, null, Auth::user(), true);

                $request->session()->flash('flash.banner', 'Your bid for PO# ' . $po->purchase_order_number . ' has been pushed to your company authorizer(s)');
            }
            else {
                if (filled($removedFiles)) {
                    $this->deleteRemovedFiles(collect($removedFiles)->diff($supportingDocs)->toArray());
                }
                $bid = PurchaseOrderBid::create([
                    'auction_id' => $auction->id,
                    'purchase_order_id' => $po->id,
                    'supplier_id' => Auth::user()->company->id,
                    'line_item_details' => json_encode($request->line_items),
                    'additional_comments' => $request->comment,
                    'additional_docs' => filled($supportingDocs) ? json_encode($supportingDocs) : null,
                ]);
                $log = CompanyActivityLog::create([
                    'activity_type_id' => 6,
                    'activity_key' => $auction->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 0,
                    'payload' => json_encode([
                        "id" => $auction->id,
                        "bid" => $bid->id,
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);
                $this->updateSupplierAuctionsCache($auction->id);
                $this->updateParentPoCache($po->id);
                NewAuctionBid::dispatch($log, $bid, Auth::user(), false);

                $request->session()->flash('flash.banner', 'Your bid for PO# ' . $po->purchase_order_number . ' has been submitted successfully');
            }
            $request->session()->flash('flash.bannerStyle', 'success');
            return back();
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function authorizeBid (Request $request): \Illuminate\Http\RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $auction = Auction::find(json_decode($log->payload)->id);
            $removedFiles = json_decode($log->payload)->removed_files;
            $po = $auction->parent_transaction;
            $pulledRequest = json_decode($log->payload)->request;

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            $supportingDocs = collect($pulledRequest->supporting_docs)->map(fn($item) => $item->data);
            if (filled($removedFiles)) {
                $this->deleteRemovedFiles(collect($removedFiles)->diff($supportingDocs)->toArray());
            }
            $bid = PurchaseOrderBid::create([
                'auction_id' => $auction->id,
                'purchase_order_id' => $po->id,
                'supplier_id' => Auth::user()->company->id,
                'line_item_details' => json_encode($pulledRequest->line_items),
                'additional_comments' => $pulledRequest->comment,
                'additional_docs' => filled($supportingDocs) ? json_encode($supportingDocs) : null,
            ]);
            $this->updateSupplierAuctionsCache($auction->id);
            $this->updateParentPoCache($po->id);
            NewAuctionBid::dispatch($log, $bid, Auth::user(), false);

            $request->session()->flash('flash.banner', 'Your bid for PO# ' . $po->purchase_order_number . ' has been submitted successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function rejectBid(Request $request): \Illuminate\Http\RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $auction = Auction::find(json_decode($log->payload)->id);
            $po = $auction->parent_transaction;
            $log->rejected_by = Auth::id();
            $log->rejected_at = now();
            $log->status = 'rejected';
            $log->save();

            $pulledRequest = json_decode($log->payload)->request;
            $supportingDocs = collect($pulledRequest->supporting_docs)->map(fn($item) => $item->data);
            $existingSupportingDocs = PurchaseOrderBid::where('auction_id', $auction->id)
                ->where('supplier_id', Auth::user()->company_id)
                ->pluck('additional_docs')->pluck('additional_docs')
                ->reject(fn($i) => $i == "[]" )
                ->reduce(fn($a, $item) => $a.$item);
            $existingSupportingDocs = json_decode(Str::replace('][', ',', $existingSupportingDocs));
            if (filled($supportingDocs)) {
                $this->deleteRemovedFiles($supportingDocs->diff(collect($existingSupportingDocs))->toArray());
            }
            Mail::to($log->inputer)->queue(new PurchaseOrderBidRejected($log, $request->rejection_comments, $po));

            $request->session()->flash('flash.banner', 'Bid submission has been rejected successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function showBids (Request $request): \Illuminate\Foundation\Application|\Illuminate\Routing\Redirector|\Inertia\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        $auction = Auction::find($request->id);
        $lookupSupplierId = $request->supplierId;
        if ((filled($lookupSupplierId) && $auction->bids->pluck('supplier_id')->doesntContain($lookupSupplierId)) || blank($auction)) {
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
        if (Gate::allows('view-selected-buyer-auction', $auction)) {
            $purchaseOrder = $auction->parent_transaction;
            $bids = collect($auction->bids)->groupBy('supplier_id')->transform(function ($item) use ($auction) {
                $supplier = collect($item)->first()->supplier;
                $bid = collect($item)->last();
                return [
                    'id' => $bid->id,
                    'supplier_name' => $supplier->company_name,
                    'bid_submitted' => $bid->created_at,
                    'bid_count' => $item->count(),
                    'part_fulfilment' => $auction->parent_transaction->supplier_requirement == 'multiple'
                        && filled(collect(json_decode($bid->line_item_details))->pluck('quantity', 'description')
                            ->diffAssoc(collect(json_decode($auction->parent_transaction->line_item_details))->pluck('quantity', 'description'))) ,
                    'supplier_location' => $supplier->company_state,
                    'supplier_city' => $supplier->company_city,
                    'supplier_crypt' => Crypt::encryptString($supplier->id),
                    'net_total' => collect(json_decode($bid->line_item_details))->sum(fn($l) => $l->price * $l->quantity),
                    'has_supporting_docs' => filled($bid->additional_docs),
                    'details' => $bid,
                    'previous_bids' => collect($item)->reject(fn($item) => $item == $bid)->values()->transform(fn($i) => [
                        'date' => $i->updated_at,
                        'line_items' => json_decode($i->line_item_details)
                    ])
                ];
            })->sortByDesc('bid_submitted')->values();

            $table_fields = ['supplier_name', 'bid_submitted', 'net_total', 'supplier_location', 'bid_count'];
            $sortable_fields = ['supplier_name', 'bid_submitted', 'net_total'];
            $date_time_fields = ['bid_submitted'];
            $currency_fields = ["net_total"];
            $attributes = [
                "supplier_name" => "Supplier",
                "bid_submitted" => "Bid Submitted",
                "bid_count" => "Bid Count",
                "net_total" => "Net Total Amount",
                "supplier_location" => "Supplier Location",
            ];
            $header_fields = [];
            foreach (collect(collect($bids)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = array_merge([
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ], collect($date_time_fields)->contains($field) ? ['formatType' => 'humanDateTime' ] : [],
                    collect($currency_fields)->contains($field) ? ['formatType' => 'currency' ] : []);

            }
            $header_fields[] = "__slot:actions:details";

            return Inertia::render('Auctions/Bids/Show', [
                "data" => $bids,
                "headerFields" => $header_fields,
                "nigerianStates" => NigerianState::all()->pluck('state_name', 'id'),
                "auction" => $auction,
                "purchaseOrder" => $purchaseOrder,
                "poNetTotal" => collect(json_decode($purchaseOrder->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                "buyerCrypt" => Crypt::encryptString(Auth::user()->company_id),
                "referredSupplier" => $request->supplierId
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    private function updateSupplierAuctionsCache($auction, $delete = false): void
    {
        if (!$delete) {
            $activeSupplierAuctionsPrimed = Cache::pull('active-auctions-supplier-primed');
            $auction = Auction::find($auction);
            $activeSupplierAuctionsPrimed = collect($activeSupplierAuctionsPrimed)->reject( function ($item) use ($auction) {return $item['id'] == $auction->id;});
            $auction_parent = collect(Cache::get(Str::kebab($auction->owner->company_name . '-pos-primed')))->firstWhere('id', $auction->auction_transaction_key)
                ?? collect([$auction->parent_transaction])->transform(fn($item) => [
                    "id" => $item->id,
                    "purchase_order_number" => $item->purchase_order_number,
                    "request_type" => $item->request_type,
                    "bid_limitation" => $item->bid_limitation(),
                    "line_item_details" => json_decode($item->line_item_details),
                    "status" => $item->status,
                    "buyer_comments" => $item->buyer_comments,
                    "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                    "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                    "delivery_date" => $item->delivery_date,
                    "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                    "initiator" => $item->inputer_id,
                    "authorizer" => $item->authorizer_id,
                    "name_restricted" => $item->name_restricted,
                    "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                    "shipping_terms" => $item->shipping_terms,
                    'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                    "bid_count" => $item->purchase_order_bids->count() ?? 0,
                    "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item) {
                        return ["data" => $item];
                    }),
                    "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                    "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                    "termination_reason" => $item->termination_reason,
                    "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                    "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                    "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                    'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                        ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                            "id" => $i->supplier->id,
                            "supplier_name" => $i->supplier->company_name,
                            "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                            "company_state" => $i->supplier->company_state
                        ]) : [],
                    'selected_group_name' => $item->selected_group_name,
                    "created" => $item->created_at,
                    "payment_terms" => $item->payment_terms,
                    "bid_bond_required" => $item->bid_bond_required,
                    "bpo_confirmed_at" => $item->bpo_confirmed_at,
                    "pending_actions" => $item->pending_action_list(),
                    "cancellation_rejection_comment" => $item->last_rejection_comment(),
                    "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                    "buyer_logo" => $item->buyer->logo_url,
                    "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                    "vat" => $item->vat,
                    "min_acceptable_quantity" => $item->min_acceptable_quantity,
                    "wht" => $item->wht,
                    "wht_deductible" => $item->wht_deductible,
                    "pre_modification_values" => $item->pre_modification_values(),
                    "buyer" => $item->buyer
                ])->first();
            $bidLimitation = match ($auction_parent['bid_limitation']) {
                'All ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'own',
                $auction_parent['selected_group_name'] . ' - supplier group' => 'group',
                'Selected ' . $auction_parent['buyer']['company_name'] . ' suppliers' => 'selected',
                'All ' . config('app.name') . ' suppliers' => 'all'
            };
            $singlePrime = [
                'id' => $auction->id,
                'bids' => $auction->bids,
                'buyer' => $auction_parent['buyer']['company_name'],
                'buyer_masked' => 'Tier ' . $auction_parent['buyer']['company_tier'] . ' buyer in ' . $auction_parent['buyer']['company_state'],
                'purchase_order' => $auction_parent,
                'auction_type' => $auction->type->auction_type,
                'buyer_tier' => $auction_parent['buyer']['company_tier'],
                'duration' => $auction->duration,
                'status' => $auction->status,
                'starts' => $auction->starts_at,
                'ends' => $auction->ends_at,
                'deliver_to' => ($auction_parent['delivery_address'] !== 'To be provided') ? NigerianState::find($auction_parent['delivery_address']['state_id'])->state_name : 'TBA',
                'net_total' => collect($auction_parent['line_item_details'])->sum(fn($item) => $item->price * $item->quantity),
                'bid_count' => $auction->bids->count(),
                'bid_limitation' => $bidLimitation,
                'restricted_to' => match ($bidLimitation) {
                    default => null,
                    'selected' => collect($auction_parent['selected_suppliers'])->pluck('id'),
                    'own' => collect(Company::find($auction_parent['buyer']['id'])->supplier_list)->pluck('supplier_id'),
                    'group' => collect(Company::find($auction_parent['buyer']['id'])->supplier_groups()[$auction_parent['selected_group_name']])->pluck('supplier_id')
                }
            ];

            $activeSupplierAuctionsPrimed = collect([$singlePrime])->merge(collect($activeSupplierAuctionsPrimed))->values();
            Cache::forever('active-auctions-supplier-primed', $activeSupplierAuctionsPrimed);
        }
    }

    private function updateParentPoCache ($po, $delete = false): void
    {
        if (!$delete) {
            $purchaseOrder= PurchaseOrder::find($po);
            $primed = Cache::pull(Str::kebab($purchaseOrder->buyer->company_name . '-pos-primed'));
            $primed = collect($primed)->reject( function ($item) use ($purchaseOrder) {return $item['id'] == $purchaseOrder->id;});

            $singlePrime = [
                "id" => $purchaseOrder->id,
                "purchase_order_number" => $purchaseOrder->purchase_order_number,
                "request_type" => $purchaseOrder->request_type,
                "bid_limitation" => $purchaseOrder->bid_limitation(),
                "line_item_details" => json_decode($purchaseOrder->line_item_details),
                "status" => $purchaseOrder->status,
                "buyer_comments" => $purchaseOrder->buyer_comments,
                "bank_payment_obligation_url" => $purchaseOrder->bank_payment_obligation_url,
                "bank_payment_obligation_id" => $purchaseOrder->bank_payment_obligation_id,
                "delivery_date" => $purchaseOrder->delivery_date,
                "purchase_order_terms_url" => $purchaseOrder->purchase_order_terms_url ?? 'Not Included',
                "initiator" => $purchaseOrder->inputer_id,
                "authorizer" => $purchaseOrder->authorizer_id,
                "name_restricted" => $purchaseOrder->name_restricted,
                "delivery_address" => DeliveryAddress::find($purchaseOrder->delivery_address_id) ?? 'To be provided',
                "shipping_terms" => $purchaseOrder->shipping_terms,
                'expected_warranty_period' => filled($purchaseOrder->expected_warranty_period) ? $purchaseOrder->expected_warranty_period : 'Not Applicable',
                "bid_count" => $purchaseOrder->purchase_order_bids->count() ?? 0,
                "additional_docs" => collect(json_decode($purchaseOrder->additional_docs))->map(function ($item){
                    return ["data" => $item];
                }),
                "purchase_order_url" => $purchaseOrder->purchase_order_url ?? 'Not Included',
                "total_amount" => collect(json_decode($purchaseOrder->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                "termination_reason" => $purchaseOrder->termination_reason,
                "net_price_discrepancy_tolerance" => $purchaseOrder->net_price_discrepancy_tolerance,
                "supplier_requirement" => $purchaseOrder->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                "finance_line" => PurchaseOrderFinanceLine::find($purchaseOrder->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                'selected_suppliers' => filled(BuyerSupplier::find(json_decode($purchaseOrder->selected_suppliers)))
                    ? BuyerSupplier::whereIn('supplier_id', json_decode($purchaseOrder->selected_suppliers))->get()->map(fn($i) => [
                        "id" => $i->supplier->id,
                        "supplier_name" => $i->supplier->company_name,
                        "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                        "company_state" => $i->supplier->company_state
                    ]) : [],
                'selected_group_name' => $purchaseOrder->selected_group_name,
                "created" => $purchaseOrder->created_at,
                "payment_terms" => $purchaseOrder->payment_terms,
                "bid_bond_required" => $purchaseOrder->bid_bond_required,
                "bpo_confirmed_at" => $purchaseOrder->bpo_confirmed_at,
                "pending_actions" => $purchaseOrder->pending_action_list(),
                "cancellation_rejection_comment" => $purchaseOrder->last_rejection_comment(),
                "linked_auction" => $purchaseOrder->linked_auction ?? $purchaseOrder->pending_linked_auction(),
                "buyer_logo" => $purchaseOrder->buyer->logo_url,
                "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $purchaseOrder->id)->where('is_selected', 1)->get(),
                "vat" => $purchaseOrder->vat,
                "min_acceptable_quantity" => $purchaseOrder->min_acceptable_quantity,
                "wht" => $purchaseOrder->wht,
                "wht_deductible" => $purchaseOrder->wht_deductible,
                "pre_modification_values" => $purchaseOrder->pre_modification_values(),
                "buyer" => $purchaseOrder->buyer->only(['id', 'company_name', 'company_state', 'company_tier'])
            ];
            $primed = collect([$singlePrime])->merge(collect($primed))->values();
        }
        else {
            //$companyPos = collect($companyPos)->reject( function ($item) use ($po) { return $item->id == $po; });
            //$primed = collect($primed)->reject( function ($item) use ($po) { return $item['id'] == $po;});
        }

        Cache::forever(Str::kebab($purchaseOrder->buyer->company_name . '-pos-primed'), $primed);
    }

    private function deleteRemovedFiles(array $removedFiles): void
    {
        $removedFilesPaths = collect($removedFiles)->map(function ($item) {
            return 'public/auction_docs/' . \App\Models\UploadedFile::where('file_alias', $item)->first()?->file_stored_as;
        })->all();
        \App\Models\UploadedFile::whereIn('file_alias', $removedFiles)->delete();
        Storage::delete($removedFilesPaths);
    }
}
