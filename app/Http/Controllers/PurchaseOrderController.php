<?php

namespace App\Http\Controllers;

use App\Events\PurchaseOrderCreated;
use App\Events\DeliveryAddressCreated;
use App\Http\Requests\ModifyPurchaseOrder;
use App\Http\Requests\NewPurchaseOrder;
use App\Jobs\PurchaseOrderModified;
use App\Models\Auction;
use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyActivityLog;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\NigerianState;
use App\Models\PendingAction;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderFinanceLine;
use App\Models\Unit;
use App\Models\UploadedFile;
use App\Models\User;
use App\Notifications\PurchaseOrderModificationRequested;
use App\Notifications\PurchaseOrderRejected;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Fluent;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Inertia\Response;


class PurchaseOrderController extends Controller
{
    //Caches : company-name-pos, company-name-pos-primed, company-name-suppliers, company-name-suppliers-primed, company-name-supplier-groups, company-name-supplier-groups-primed
    public function index(Request $request) {
        if (Gate::allows('create-purchase-order')){
            $companyPurchaseOrders = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-pos'), function () {
                return Auth::user()->company->purchase_orders;
            });
            $purchase_orders = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-pos-primed'), function () use ($companyPurchaseOrders) {
                return collect($companyPurchaseOrders)->sortByDesc('updated_at')->values()->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "purchase_order_number" => $item->purchase_order_number,
                        "request_type" => $item->request_type,
                        "bid_limitation" => $item->bid_limitation(),
                        "line_item_details" => json_decode($item->line_item_details),
                        "status" => $item->status,
                        "buyer_comments" => $item->buyer_comments,
                        "bank_payment_obligation_url" => $item->bank_payment_obligation_url,
                        "bank_payment_obligation_id" => $item->bank_payment_obligation_id,
                        "delivery_date" => $item->delivery_date,
                        "purchase_order_terms_url" => $item->purchase_order_terms_url ?? 'Not Included',
                        "initiator" => $item->inputer_id,
                        "authorizer" => $item->authorizer_id,
                        "name_restricted" => $item->name_restricted,
                        "delivery_address" => DeliveryAddress::find($item->delivery_address_id) ?? 'To be provided',
                        "shipping_terms" => $item->shipping_terms,
                        'expected_warranty_period' => filled($item->expected_warranty_period) ? $item->expected_warranty_period : 'Not Applicable',
                        "bid_count" => $item->purchase_order_bids->count() ?? 0,
                        "additional_docs" => collect(json_decode($item->additional_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "purchase_order_url" => $item->purchase_order_url ?? 'Not Included',
                        "total_amount" => collect(json_decode($item->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                        "termination_reason" => $item->termination_reason,
                        "net_price_discrepancy_tolerance" => $item->net_price_discrepancy_tolerance,
                        "supplier_requirement" => $item->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                        "finance_line" => PurchaseOrderFinanceLine::find($item->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                        'selected_suppliers' => filled(BuyerSupplier::find(json_decode($item->selected_suppliers)))
                            ? BuyerSupplier::whereIn('supplier_id', json_decode($item->selected_suppliers))->get()->map(fn($i) => [
                                "id" => $i->supplier->id,
                                "supplier_name" => $i->supplier->company_name,
                                "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                                "company_state" => $i->supplier->company_state
                            ]) : [],
                        'selected_group_name' => $item->selected_group_name,
                        "created" => $item->created_at,
                        "payment_terms" => $item->payment_terms,
                        "bid_bond_required" => $item->bid_bond_required,
                        "bpo_confirmed_at" => $item->bpo_confirmed_at,
                        "pending_actions" => $item->pending_action_list(),
                        "cancellation_rejection_comment" => $item->last_rejection_comment(),
                        "linked_auction" => $item->linked_auction ?? $item->pending_linked_auction(),
                        "buyer_logo" => $item->buyer->logo_url,
                        "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $item->id)->where('is_selected', 1)->get(),
                        "vat" => $item->vat,
                        "min_acceptable_quantity" => $item->min_acceptable_quantity,
                        "wht" => $item->wht,
                        "wht_deductible" => $item->wht_deductible,
                        "pre_modification_values" => $item->pre_modification_values(),
                        "buyer" => $item->buyer->only(['id', 'company_name', 'company_state', 'company_tier'])
                    ];
                });
            });
            $supplierGroups = Auth::user()->company->supplier_groups()->reject(fn ($item, $key) => empty($key))->transform(function ($item) {
                return collect($item)->map(function($item) {
                    $company = Company::find($item["supplier_id"]);
                    return [
                        "id" => $item["supplier_id"],
                        "supplier_name" => $company->company_name,
                        "supplier_code" => $item["supplier_code"] ?? 'Not Available',
                        "supplier_state" => $company->company_state
                    ];
                });
            });
            $table_fields = ["purchase_order_number","request_type", "total_amount",  "status", "bid_limitation", "created"];
            $sortable_fields = ["request_type", "total_amount", "status", "created", "bid_limitation"];
            $date_fields = ["created"];
            $currency_fields = ["total_amount"];
            $percent_fields = [];
            $attributes = [
                "purchase_order_number" => "Purchase Order Number",
                "request_type" => "Type",
                "total_amount" => "Total Amount",
                "status" => "Status",
                "bid_limitation" => "Open To",
                "created" => "Date Created",
            ];

            $header_fields = [];
            //$header_fields[] = "__slot:checkboxes";
            foreach (collect(collect($purchase_orders)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = array_merge([
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ], collect($date_fields)->contains($field) ? ['formatType' => 'date' ] : [],
                    collect($percent_fields)->contains($field) ? ['formatType' => 'percent' ] : [],
                    collect($currency_fields)->contains($field) ? ['formatType' => 'currency' ] : []);

            }
            $header_fields[] = "__slot:actions:details";
            $userStatuses = collect($purchase_orders)->pluck('status')->unique()->sort();
            $companySuppliers = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-suppliers'), function () {
                return Auth::user()->company->supplier_list;
            });
            $suppliers = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-suppliers-primed'),fn() => $companySuppliers->transform(fn($item) => [
                "id" => $item->supplier->id,
                "company_name" => $item->supplier->company_name,
                "supplier_code" => $item->supplier_code ?? 'Not Available',
                "company_state" => $item->supplier->company_state,
                "supplier_group" => $item->group_name ?? 'No Group',
            ]));
            $userRequestTypes = collect($purchase_orders)->pluck('request_type')->unique()->sort();

            return Inertia::render('PurchaseOrders/Show', [
                "data" => $purchase_orders,
                "headerFields" => $header_fields,
                "userStatuses" => $userStatuses,
                "notFoundMsg" => 'No purchase orders found',
                "nigerianStates" => NigerianState::all()->pluck('state_name', 'id'),
                "supplierGroups" => $supplierGroups,
                "suppliersTable" => $suppliers,
                "serviceUnits" => Unit::where('unit_type', 'service')->get()->transform(fn($item) => [
                    "unit" => $item->unit,
                    "unit_type" => $item->unit_type,
                    "singular" => Str::singular($item->unit)
                ])->sortBy('unit')->values(),
                "productUnits" => Unit::where('unit_type', 'product')->get()->transform(fn($item) => [
                    "unit" => $item->unit,
                    "unit_type" => $item->unit_type,
                    "singular" => Str::singular($item->unit)
                ])->sortBy('unit')->values(),
                "purchaseOrderTerms" => collect(Auth::user()->company->purchase_order_terms)->transform(fn ($item) => [
                    "id" => $item->id,
                    "alias" => $item->file_alias
                ]),
                "purchaseOrderLines" => collect(Auth::user()->company->active_po_finance_lines)->transform(fn($item) => [
                    "id" => $item->id,
                    "bank" => $item->bank->bank_name,
                    "line_name" => $item->line_unique_name,
                    "availed" => $item->availed_amount,
                    "available" => $item->available_amount,
                    "description" => $item->request_description,
                    "expires_at" => $item->expires_at,
                    "available_for" => $item->closed_supplier_set == 0 ? 'All Suppliers' : 'Limited Supplier List'
                ]),
                "deliveryAddresses" => collect(Auth::user()->company->delivery_addresses)->transform(fn($item) => [
                    "id" => $item->id,
                    "address" => $item->address,
                    "city" => $item->city,
                    "state" => $item->state->state_name,
                ]),
                "userRequestTypes" => $userRequestTypes
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function create(Request $request): Application|Redirector|Response|\Illuminate\Contracts\Foundation\Application|RedirectResponse
    {
        if (Gate::allows('create-purchase-order')){
            $supplierGroups = Auth::user()->company->supplier_groups()->reject(fn ($item, $key) => empty($key))->transform(function ($item) {

                return collect($item)->map(function($item) {
                    $company = Company::find($item["supplier_id"]);
                    return [
                        "id" => $item["supplier_id"],
                        "supplier_name" => $company->company_name,
                        "supplier_code" => $item["supplier_code"] ?? 'Not Available',
                        "supplier_state" => $company->company_state
                    ];
                });
            });
            $companySuppliers = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-suppliers'), function () {
                return Auth::user()->company->supplier_list;
            });
            $suppliers = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-suppliers-primed'),fn() => $companySuppliers->transform(fn($item) => [
                "id" => $item->supplier->id,
                "company_name" => $item->supplier->company_name,
                "supplier_code" => $item->supplier_code ?? 'Not Available',
                "company_state" => $item->supplier->company_state,
                "supplier_group" => $item->group_name ?? 'No Group',
            ]));
            $table_fields = ["company_name", "supplier_code", "location", "supplier_group"];
            $sortable_fields = ["company_name", "supplier_code", "location", "supplier_group"];
            $attributes = [
                "id" => "ID",
                "company_name" => "Supplier Company",
                "supplier_code" => "Supplier Code",
                "company_state" => "Location",
                "supplier_group" => "Supplier Group",
            ];
            $header_fields = [];
            $header_fields[] = "__slot:checkboxes";
            foreach (collect(collect($suppliers)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = [
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ];

            }

            return Inertia::render('PurchaseOrders/Create', [
                "supplierGroups" => $supplierGroups,
                "serviceUnits" => Unit::where('unit_type', 'service')->get()->transform(fn($item) => [
                    "unit" => $item->unit,
                    "unit_type" => $item->unit_type,
                    "singular" => Str::singular($item->unit)
                ])->sortBy('unit')->values(),
                "productUnits" => Unit::where('unit_type', 'product')->get()->transform(fn($item) => [
                    "unit" => $item->unit,
                    "unit_type" => $item->unit_type,
                    "singular" => Str::singular($item->unit)
                ])->sortBy('unit')->values(),
                "purchaseOrderTerms" => collect(Auth::user()->company->purchase_order_terms)->transform(fn ($item) => [
                    "id" => $item->id,
                    "alias" => $item->file_alias
                ]),
                "purchaseOrderLines" => collect(Auth::user()->company->active_po_finance_lines)->transform(fn($item) => [
                    "id" => $item->id,
                    "bank" => $item->bank->bank_name,
                    "line_name" => $item->line_unique_name,
                    "availed" => $item->availed_amount,
                    "available" => $item->available_amount,
                    "description" => $item->request_description,
                    "expires_at" => $item->expires_at,
                    "available_for" => $item->closed_supplier_set == 0 ? 'All Suppliers' : 'Limited Supplier List'
                ]),
                "deliveryAddresses" => collect(Auth::user()->company->delivery_addresses)->transform(fn($item) => [
                    "id" => $item->id,
                    "address" => $item->address,
                    "city" => $item->city,
                    "state" => $item->state->state_name,
                ]),
                "nigerianStates" => NigerianState::all()->pluck('state_name', 'id'),
                "suppliersTable" => $suppliers,
                "headerFields" => $header_fields,
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function modify(Request $request): RedirectResponse
    {
        if (Gate::allows('create-purchase-order')) {
            if (filled($request->delivery_date)) {
                $request->merge(['max_auction_start' => Carbon::parse($request->delivery_date)->subDays(10)->toDateString()]);
            }
            else {
                $request->merge(['max_auction_start' => Carbon::parse(PurchaseOrder::find($request->id)->delivery_date)->subDays(10)->toDateString()]);
            }

            $validator = Validator::make($request->all(), [
                "bid_limitation" => ['in:all,own,group,selected'],
                "supplier_requirement" => ['in:single,multiple'],
                "selected_suppliers" => ['required_if:bid_limitation,selected', Rule::when(
                    function (Fluent $input){
                        return $input->get('bid_limitation') == 'selected' ;
                    },
                    ['array', 'min:2'] ,
                    [])],
                "selected_group_name" => ['required_if:bid_limitation,group', 'nullable'],
                "line_item_details" => ['array', 'min:1'],
                "line_item_details.*.price" => ['required_with:line_item_details', 'numeric', 'min:0.01'],
                "line_item_details.*.quantity" => ['required_with:line_item_details', 'numeric', 'min:0.01'],
                "line_item_details.*.description" => ['required_with:line_item_details', 'string', 'max:400'],
                "line_item_details.*.unit" => ['required_with:line_item_details', 'string'],
                "line_item_details.*.type" => ['required_with:line_item_details', 'string', 'in:product,service'],
                "purchase_order_terms_url" => ['nullable', 'required_if:terms,new', 'required_if:terms,existing', 'string', 'ends_with:pdf,jpg,jpeg', Rule::when(
                    function (Fluent $input){
                        return $input->get('terms') == 'new' ;
                    },
                    ['exists:uploaded_files,original_name'] ,
                    []),
                    Rule::when(
                        function (Fluent $input){
                            return $input->get('terms') == 'existing' ;
                        },
                        ['exists:uploaded_files,file_alias'] ,
                        []),
                ],
                "x_payment_days" => ['required_if:payment_term,x-delivery', 'required_if:payment_term,x-confirmation', 'numeric', 'integer', 'min:1'],
                "finance_line" => ['required_if:active_line,existing', 'numeric', 'integer'],
                "delivery_date" => ['date', 'after:tomorrow'],
                "delivery_address" => ['required_if:address,new', 'required_if:address,existing', Rule::forEach(function ( $value, string $attribute) {
                    return match ($attribute) {
                        'address' => ['required', 'string', 'max:400'],
                        'city' => ['required', 'string'],
                        'state' => ['required', 'exists:nigerian_states:id'],
                        default => [],
                    };
                })],
                "shipping_term_text" => ['required_if:shipping_terms,other', 'nullable', 'string'],
                "purchase_order_url" => ['nullable', 'ends_with:pdf,jpg,jpeg', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                "additional_docs.*.data" => ['string', 'max:80', 'ends_with:pdf,jpg,jpeg', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                "net_price_discrepancy_tolerance" => ['numeric', 'min:0', 'max:100'],
                "warranty_period" => ['nullable', 'min:0', 'max:24'],
                "auction_start" => ['in:now,schedule'],
                "scheduled_start_date" => ['nullable', 'required_if:auction_start,schedule', 'date:format:Y-m-d H:i', 'before_or_equal:max_auction_start'],
                "min_acceptable_quantity" => ['nullable', 'required_if:supplier_requirement,multiple', 'numeric'],
                "wht" => ['nullable', 'numeric', 'max:10', 'min:0']
            ], [
                'bid_limitation.in' => 'Please select a valid value for eligible suppliers',
                'x_payment_days.required_if' => 'Number of payment delays days is required',
                'shipping_term_text.required_if' => 'Please enter shipping terms',
                'purchase_order_terms_url.ends_with' => 'Invalid payment terms file uploaded',
                'purchase_order_terms_url.exists' => 'Invalid payment terms file uploaded',
                'purchase_order_url.ends_with' => 'Invalid scanned purchase order file uploaded',
                'purchase_order_url.exists' => 'Invalid scanned purchase order file uploaded',
                'additional_docs.*.data.ends_with' => 'Invalid additional document :position uploaded',
                'additional_docs.*.data.exists' => 'Invalid additional document :position uploaded',
                'additional_docs.*.data.max' => 'Additional document :position file name is too long',
                'scheduled_start_date.required_if' => 'Scheduled auction start date is required',
                'scheduled_start_date.before' => 'Scheduled auction start must be a date before ' . Carbon::parse($request->max_auction_start)->format('M d, Y') . ' ( based on P.O delivery date)',
            ], [
                'bid_limitation' => 'eligible suppliers',
                'terms' => 'purchase order terms/conditions',
                'x_payment_days' => 'payment delay days',
                'finance_line' => 'purchase order financing line',
                'delivery_date' => 'delivery date',
                'additional_docs.*.data' => 'additional document :position',
                'net_price_discrepancy_tolerance' => 'net price variance tolerance',
                'min_acceptable_quantity' => 'minimum acceptable quantity',
                'wht' => 'withholding tax',
                'line_item_details' => 'line item',
                "line_item_details.*.price" => 'line item :position price',
                "line_item_details.*.quantity" => 'line item :position quantity',
                "line_item_details.*.description" => 'line item :position description',
                "line_item_details.*.unit" => 'line item :position unit',
                "line_item_details.*.type" => 'line item :position type',
            ]);
            $validator->sometimes('line_item_details.*.unit', 'exists:units,unit', function (Fluent $input, Fluent $item) {
                return !$item->other_unit_check;
            });
            $validator->sometimes('line_item_details.*.unit', 'string', function (Fluent $input, Fluent $item) {
                return $item->other_unit_check;
            });
            $validator->validate();
            $removedFiles = [];
            if (filled($request->additional_docs) || ($request->additional_docs == [])) {
                $additionalDocs = $this->fetchAlias(collect($request->additional_docs)->transform(fn($item) => $item['data'])->reject(fn($item) => blank($item))->all());
            }
            if (filled($request->removed_files)) {
                $removedFiles = $this->fetchAlias($request->removed_files);
            }
            if (filled($request->payment_terms)) {
                $paymentTerms = match ($request->payment_terms) {
                    'in_po_terms' => 'Stated in P.O terms document',
                    'x-delivery' => $request->x_payment_days . ' days after delivery',
                    'x-confirmation' => $request->x_payment_days . ' days after invoice confirmation',
                    'acceptance' => 'Payment on invoice acceptance',
                    'delivery' => 'Payment on delivery',
                    'confirmation' => 'Payment on invoice confirmation',
                    default => null,
                };
            }
            if (filled($request->delivery_address) && blank($request->delivery_address['id'])) {
                $deliveryAddress = DeliveryAddress::create([
                    'buyer_id' => Auth::user()->company->id,
                    'address' => $request->delivery_address['address'],
                    'city' => $request->delivery_address['city'],
                    'state_id' => $request->delivery_address['state'],
                    'inputed_by' => Auth::id()
                ]);
                event(new DeliveryAddressCreated($deliveryAddress));
                $deliveryAddress = $deliveryAddress->id;
            }
            elseif (filled($request->delivery_address) && filled($request->delivery_address['id'])) {
                $deliveryAddress = $request->delivery_address['id'];
            }
            if (filled($request->shipping_terms)) {
                $shippingTerm = match ($request->shipping_terms) {
                    'supplier' => 'Supplier to Handle Delivery',
                    'buyer' => 'Buyer to Handle Delivery',
                    'other' => $request->shipping_term_text,
                    'in_po_terms' => 'Stated in P.O terms document',
                    default => null,
                };
            }
            if (filled($request->purchase_order_terms_url)) {
                $poTerms = match ($request->terms) {
                    'none' => null,
                    'existing' => $request->purchase_order_terms_url,
                    'new' => $this->fetchAlias($request->purchase_order_terms_url),
                    default => null
                };
            }

            $purchaseOrder = PurchaseOrder::find($request->id);
            $oldValues = collect(collect(Cache::get(Str::kebab(Auth::user()->company->company_name . '-pos-primed')))->where('id', $purchaseOrder->id)->first())
                    ->only(
                        collect($request->all())
                            ->reject(fn($item, $key) => collect(['terms', 'active_line', 'removed_files', 'max_auction_start', 'address'])->contains($key))
                            ->merge(['linked_auction' => []])
                            ->keys()
                    );

            $log = CompanyActivityLog::create([
                'activity_type_id' => 5,
                'activity_key' => $purchaseOrder->id,
                'company_id' => Auth::user()->company->id,
                'inputted_by' => Auth::id(),
                'authorization_required' => 1,
                'payload' => json_encode([
                    "id" => $purchaseOrder->id,
                    "old_values" => $oldValues,
                    "pending_linked_auction" => [
                            "auction_start" => $request->auction_start ?? $purchaseOrder->pending_linked_auction()['auction_start'],
                            "auction_duration" => $request->auction_duration ?? $purchaseOrder->pending_linked_auction()['auction_duration'],
                            "auction_auto_restart" => $request->auction_auto_restart ?? $purchaseOrder->pending_linked_auction()['auction_auto_restart'],
                            "scheduled_start_date" => $request->scheduled_start_date ?? $purchaseOrder->pending_linked_auction()['scheduled_start_date'],
                        ],
                    'status' => 'action required',
                ]),
                'status' => 'unauthorized',
            ]);
            foreach ((collect($request->all())->filter(fn($item, $k) => collect($purchaseOrder)->keys()->contains($k))) as $key => $value) {
                $purchaseOrder->$key = match ($key) {
                    'additional_docs' => json_encode($additionalDocs),
                    'payment_terms' => $paymentTerms,
                    'shipping_terms' => $shippingTerm,
                    'purchase_order_terms_url' => $poTerms,
                    'selected_suppliers' => json_encode(collect($request->selected_suppliers)->pluck('id')),
                    'line_item_details' => json_encode($request->line_item_details),
                    default => $value,
                };
            }
            foreach (collect($request->all())->filter(fn($item, $k) => collect($purchaseOrder)->keys()->doesntContain($k)) as $key => $value) {
                switch ($key) {
                    case 'delivery_address':
                        $purchaseOrder->delivery_address_id = $deliveryAddress;
                        break;
                    case 'finance_line':
                        $purchaseOrder->po_finance_line_id = $request->finance_line;
                        break;
                    case 'bid_limitation':
                        if ($value == 'own'){
                            $purchaseOrder->restricted_to_own_suppliers = 1;
                        }
                        elseif ($value == 'group'){
                            $purchaseOrder->restricted_to_group = 1;
                        }
                        elseif ($value == 'selected'){
                            $purchaseOrder->restricted_to_selected = 1;
                        }
                        break;
                }
            }
            $purchaseOrder->status = 'modification effected';
            $purchaseOrder->save();

            PurchaseOrderModified::dispatch($purchaseOrder, Auth::user()->company->authorizer_list(), $log);
            $pending_action = $purchaseOrder->pending_action;
            $pending_action->delete();
            $this->deleteRemovedFiles($removedFiles, $purchaseOrder);

            $request->session()->flash('flash.banner', 'Purchase Order modification has been pushed to your company authorizer(s)');
            $request->session()->flash('flash.bannerStyle', 'success');
            $this->rebuildCaches($purchaseOrder->id);
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function checkNew (NewPurchaseOrder $request, $id) {
        $validated = $request->validated();
    }

    public function checkModify (ModifyPurchaseOrder $request, $id) {
        $validated = $request->validated();
    }

    public function createRequest (Request $request): RedirectResponse
    {
        if (Gate::allows('create-purchase-order')) {
            $request->merge([
                'max_auction_start' => Carbon::parse($request->edd)->subDays(10)->toDateString(),
                'min_edd' => $request->auction_start === 'schedule'
                    ? Carbon::parse($request->scheduled_start_date)->addDays(10)
                    : Carbon::parse(now())->addDays(10)
            ]);

            $validator = Validator::make($request->all(), [
                "bid_limitation" => ['bail', 'required', 'in:all,own,group,selected'],
                "po_number" => ['bail', 'required', 'string', 'min:8', 'unique:purchase_orders,purchase_order_number'],
                "supplier_requirement" => ['bail', 'required', 'in:single,multiple'],
                "selected_suppliers" => ['bail', 'nullable', 'required_if:bid_limitation,selected', Rule::when(
                    function (Fluent $input){
                        return $input->get('bid_limitation') == 'selected' ;
                    },
                    ['array', 'min:2'] ,
                    [])],
                "supplier_group" => ['bail', 'nullable', 'required_if:bid_limitation,group'],
                "line_items" => ['bail', 'required', 'array', 'min:1'],
                "line_items.*.price" => ['bail', 'required', 'numeric', 'min:0.01'],
                "line_items.*.quantity" => ['bail', 'required', 'numeric', 'min:0.01'],
                "line_items.*.description" => ['bail', 'required', 'string', 'max:400'],
                "line_items.*.unit" => ['bail', 'required', 'string'],
                "line_items.*.type" => ['bail', 'required', 'string', 'in:product,service'],
                "terms" => ['bail', 'required', 'in:existing,new,none'],
                "terms_url" => ['bail', 'nullable', 'required_unless:terms,none', 'string', 'ends_with:pdf,jpg,jpeg', Rule::when(
                    function (Fluent $input){
                        return $input->get('terms') == 'new' ;
                    },
                    ['exists:uploaded_files,original_name'] ,
                    []),
                    Rule::when(
                        function (Fluent $input){
                            return $input->get('terms') == 'existing' ;
                        },
                        ['exists:uploaded_files,file_alias'] ,
                        []),
                ],
                "x_payment_days" => ['bail', 'required_if:payment_term,x-delivery', 'required_if:payment_term,x-confirmation', 'nullable', 'numeric', 'integer', 'min:1'],
                "po_line" => ['bail', 'required_if:active_line,existing', 'nullable', 'numeric', 'integer'],
                "edd" => ['bail', 'required', 'date', 'after_or_equal:min_edd'],
                "delivery_address_existing" => ['bail', 'nullable', 'required_if:address,existing', Rule::exists('delivery_addresses', 'id')->where(function (Builder $query) {
                    return $query->where('buyer_id', Auth::user()->company->id);
                })],
                "delivery_address_new" => ['bail', 'required_if:address,new', Rule::forEach(function ( $value, string $attribute) {
                    return match ($attribute) {
                        'address' => ['bail', 'required', 'string', 'max:400'],
                        'city' => ['bail', 'required', 'string'],
                        'state' => ['bail', 'required', 'exists:nigerian_states:id'],
                        default => [],
                    };
                })],
                "shipping_term_text" => ['bail', 'required_if:shipping_term,other', 'nullable', 'string'],
                "physical_scan" => ['bail', 'nullable', 'ends_with:pdf,jpg,jpeg', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                "supporting_docs.*.data" => ['bail', 'string', 'max:80', 'ends_with:pdf,jpg,jpeg', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                "net_discrepancy" => ['bail', 'required', 'numeric', 'min:0', 'max:100'],
                "warranty_period" => ['bail', 'nullable', 'min:0', 'max:24'],
                "auction_start" => ['bail', 'required', 'in:now,schedule'],
                "scheduled_start_date" => ['bail', 'nullable', 'required_if:auction_start,schedule', 'date:format:Y-m-d H:i', 'before_or_equal:max_auction_start'],
                "min_acceptable_quantity" => ['bail', 'nullable', 'required_if:supplier_requirement,multiple', 'numeric'],
                "wht" => ['bail', 'nullable', 'numeric', 'max:10', 'min:0']
            ], [
                'bid_limitation.in' => 'Please select a valid value for eligible suppliers',
                'x_payment_days.required_if' => 'Number of payment delays days is required',
                'shipping_term_text.required_if' => 'Please enter shipping terms',
                'term_url.ends_with' => 'Invalid payment terms file uploaded',
                'term_url.exists' => 'Invalid payment terms file uploaded',
                'physical_scan.ends_with' => 'Invalid scanned purchase order file uploaded',
                'physical_scan.exists' => 'Invalid scanned purchase order file uploaded',
                'supporting_docs.*.data.ends_with' => 'Invalid additional document :position uploaded',
                'supporting_docs.*.data.exists' => 'Invalid additional document :position uploaded',
                'supporting_docs.*.data.max' => 'Additional document :position file name is too long',
                'scheduled_start_date.required_if' => 'Scheduled auction start date is required',
                'edd.after_or_equal' => 'Expected delivery date must be at least 10 days after your scheduled auction start date',
                'scheduled_start_date.before_or_equal' => 'Auction start date must be at least 10 days before your expected delivery date'
            ], [
                'bid_limitation' => 'eligible bidders',
                'po_number' => 'purchase order number',
                'terms' => 'purchase order terms/conditions',
                'x_payment_days' => 'payment delay days',
                'po_line' => 'purchase order financing line',
                'edd' => 'delivery date',
                'delivery_address_existing' => 'existing delivery address',
                'delivery_address_new' => 'new delivery address',
                'supporting_docs.*.data' => 'additional document :position',
                'net_discrepancy' => 'net price variance tolerance',
                'min_acceptable_quantity' => 'minimum acceptable quantity',
                'wht' => 'withholding tax'
            ]);
            $validator->sometimes('line_items.*.unit', 'exists:units,unit', function (Fluent $input, Fluent $item) {
                return !$item->other_unit_check;
            });
            $validator->sometimes('line_items.*.unit', 'string', function (Fluent $input, Fluent $item) {
                return $item->other_unit_check;
            });
            $validator->validate();
            $supportingDocs = collect($request->supporting_docs)->transform(fn($item) => $item['data'])->reject(fn($item) => blank($item))->all();
            $supportingDocs = $this->fetchAlias($supportingDocs);
            $removedFiles = $this->fetchAlias($request->removed_files);
            if ($request->payment_term == 'in_po_terms') {
                $paymentTerms = 'Stated in P.O terms document';
            }
            elseif ($request->payment_term == 'x-delivery') {
                $paymentTerms = $request->x_payment_days . ' days after delivery';
            }
            elseif ($request->payment_term == 'x-confirmation') {
                $paymentTerms = $request->x_payment_days . ' days after invoice confirmation';
            }
            elseif ($request->payment_term == 'acceptance') {
                $paymentTerms = 'Payment on invoice acceptance';
            }
            elseif ($request->payment_term == 'delivery') {
                $paymentTerms = 'Payment on delivery';
            }
            elseif ($request->payment_term == 'confirmation') {
                $paymentTerms = 'Payment on invoice confirmation';
            }

            if (filled($request->delivery_address_new) && filled($request->delivery_address_new['address'])) {
                $deliveryAddress = DeliveryAddress::create([
                    'buyer_id' => Auth::user()->company->id,
                    'address' => $request->delivery_address_new['address'],
                    'city' => $request->delivery_address_new['city'],
                    'state_id' => $request->delivery_address_new['state'],
                    'inputed_by' => Auth::id()
                ]);
                event(new DeliveryAddressCreated($deliveryAddress));
                $deliveryAddress = $deliveryAddress->id;
            }
            elseif (filled($request->delivery_address_existing)) {
                $deliveryAddress = $request->delivery_address_existing;
            }

            $shippingTerm = match ($request->shipping_term) {
                'supplier' => 'Supplier to Handle Delivery',
                'buyer' => 'Buyer to Handle Delivery',
                'other' => $request->shipping_term_text,
                'in_po_terms' => 'Stated in P.O terms document',
                default => null
            };

            $poTerms = match ($request->terms) {
                'existing' => $request->terms_url,
                'new' => $this->fetchAlias($request->terms_url),
                default => null
            };

            $purchaseOrder = PurchaseOrder::create([
                "buyer_id" => Auth::user()->company->id,
                "purchase_order_number" => $request->po_number,
                "line_item_details" => json_encode($request->line_items),
                "status" => 'new',
                "buyer_comments" => $request->comments,
                "delivery_date" => $request->edd,
                "payment_terms" => $paymentTerms,
                "inputer_id" => Auth::id(),
                "name_restricted" => $request->name_visibility == 1 ? 0 : 1,
                "delivery_address_id" => $deliveryAddress ?? null,
                "shipping_terms" => $shippingTerm,
                "expected_warranty_period" => $request->warranty_period,
                "purchase_order_url" => $this->fetchAlias($request->physical_scan),
                "additional_docs" => json_encode($supportingDocs),
                "net_price_discrepancy_tolerance" => $request->net_discrepancy,
                "supplier_requirement" => $request->supplier_requirement,
                "po_finance_line_id" => $request->po_line,
                'restricted_to_own_suppliers' => $request->bid_limitation == 'own' ? 1 : 0,
                'request_type' => 'Open for Bidding (Auction)',
                'purchase_order_terms_url' => $poTerms,
                'restricted_to_group' => $request->bid_limitation == 'group' ? 1 : 0,
                'restricted_to_selected' => $request->bid_limitation == 'selected' ? 1 : 0,
                'selected_suppliers' => json_encode($request->selected_suppliers),
                'selected_group_name' => $request->supplier_group,
                'bid_bond_required' => $request->bid_bond_required,
                "min_acceptable_quantity" => $request->min_acceptable_quantity,
                "wht" => $request->wht,
                "wht_deductible" => $request->wht_deductible,
            ]);

            if (Auth::user()->company->use_authorizer == 1) {
                $log = CompanyActivityLog::create([
                    'activity_type_id' => 4,
                    'activity_key' => $purchaseOrder->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 1,
                    'payload' => json_encode([
                        "id" => $purchaseOrder->id,
                        "request" => $request->all(),
                        'po_type' => 'Open for Bidding (Auction)',
                        'status' => 'new',
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);

                PurchaseOrderCreated::dispatch($purchaseOrder, null, true, Auth::user()->company->authorizer_list(), $log);
                $purchaseOrder->status = 'authorization pending';
                $purchaseOrder->save();
                $request->session()->flash('flash.banner', 'Purchase Order Creation Request has been pushed to your company authorizer(s)');
                $request->session()->flash('flash.bannerStyle', 'success');
                $this->rebuildCaches($purchaseOrder->id);
            }
            else {
                CompanyActivityLog::create([
                    'activity_type_id' => 4,
                    'activity_key' => $purchaseOrder->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 0,
                    'payload' => json_encode([
                        "id" => $purchaseOrder->id,
                        "request" => $request->all(),
                        'po_type' => 'Open for Bidding (Auction)',
                        'status' => 'new',
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);
                if ($request->auction_start == 'now') {
                    $purchaseOrder->status = 'bidding opened';
                    $purchaseOrder->save();
                }
                elseif ($request->auction_start == 'schedule') {
                    $purchaseOrder->status = 'auction scheduled';
                    $purchaseOrder->save();
                }
                $this->rebuildCaches($purchaseOrder->id);

                $auction = Auction::create([
                    'auction_transaction_key' => $purchaseOrder->id,
                    'company_id' => Auth::user()->company->id,
                    'auction_type' => 5,
                    'auction_description' => null,
                    'related_docs' => $purchaseOrder->supporting_docs,
                    'auction_duration' => $request->auction_duration,
                    'status' => $request->auction_start == 'now' ? 'open' : 'scheduled',
                    'starts_at' => $request->auction_start == 'now' ? now() : Carbon::parse($request->scheduled_start_date),
                    'ends_at' => $request->auction_start == 'now'
                        ? now()->addDays($request->auction_duration)
                        : Carbon::parse($request->scheduled_start_date)->addDays($request->auction_duration),
                    "auto_restart" => $request->auction_auto_restart ? 1 : 0
                ]);

                $notificationList = collect(Auth::user()->company->authorizer_list())->merge([Auth::user()]);
                PurchaseOrderCreated::dispatch($purchaseOrder, $request->bid_limitation, false, $notificationList, null, $auction);

                $this->deleteRemovedFiles($removedFiles, $purchaseOrder);

                $request->session()->flash('flash.banner', 'Purchase Order Creation Request has been started successfully, check your email for details');
                $request->session()->flash('flash.bannerStyle', 'success');
            }
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function authorizeRequestCreation (Request $request): RedirectResponse {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $po = PurchaseOrder::find(json_decode($log->payload)->id);
            $removedFiles = json_decode($log->payload)->removed_files;
            $pulledRequest = json_decode($log->payload)->request;

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            if ($pulledRequest->auction_start == 'now') {
                $po->status = 'bidding opened';
                $po->save();
            }
            elseif ($pulledRequest->auction_start == 'schedule') {
                $po->status = 'auction scheduled';
                $po->save();
            }
            $this->rebuildCaches($po->id);

            $auction = Auction::create([
                'auction_transaction_key' => $po->id,
                'company_id' => Auth::user()->company->id,
                'auction_type' => 5,
                'auction_description' => null,
                'related_docs' => $po->additional_docs,
                'auction_duration' => $pulledRequest->auction_duration,
                'status' => $pulledRequest->auction_start == 'now' ? 'open' : 'scheduled',
                'starts_at' => $pulledRequest->auction_start == 'now' ? now() : Carbon::parse($pulledRequest->scheduled_start_date), //TODO: check if scheduled date has passed
                'ends_at' => $pulledRequest->auction_start == 'now'
                    ? now()->addDays($pulledRequest->auction_duration)
                    : Carbon::parse($pulledRequest->scheduled_start_date)->addDays($pulledRequest->auction_duration),
                "auto_restart" => $pulledRequest->auction_auto_restart ? 1 : 0
            ]);

            $notificationList = Auth::user()->company->authorizer_list()->merge(
                [User::find($log->inputted_by)]
            );
            PurchaseOrderCreated::dispatch($po, $pulledRequest->bid_limitation, false, $notificationList, null, $auction);

            $this->deleteRemovedFiles($removedFiles, $po);
            $request->session()->flash('flash.banner', 'Purchase Order creation has been approved successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authorizeRequestModification (Request $request): RedirectResponse {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $po = PurchaseOrder::find(json_decode($log->payload)->id);
            $validator = Validator::make(
                [
                    'expected_delivery_date' => $po->delivery_date,
                    'auction_start_date' => $po->pending_linked_auction()['auction_start'] == 'schedule'
                        ? Carbon::parse($po->pending_linked_auction()['scheduled_start_date'])->toDateTimeString()
                        : now()->addMinutes(2),
                    'check_date_delivery' => $po->pending_linked_auction()['auction_start'] == 'schedule'
                        ? Carbon::parse($po->pending_linked_auction()['scheduled_start_date'])->addDays(10)->toDateString()
                        : Carbon::parse(now()->addDays(10))->toDateString(),
                ],
                [
                'expected_delivery_date' => 'date|after_or_equal:check_date_delivery',
                'auction_start_date' => 'date|after_or_equal::now'
            ]);
            $validator->validate();

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            if ($po->pending_linked_auction()['auction_start'] == 'now') {
                $po->status = 'bidding opened';
                $po->save();
            }
            elseif ($po->pending_linked_auction()['auction_start'] == 'schedule') {
                $po->status = 'auction scheduled';
                $po->save();
            }

            $auction = Auction::create([
                'auction_transaction_key' => $po->id,
                'company_id' => Auth::user()->company->id,
                'auction_type' => 5,
                'auction_description' => null,
                'related_docs' => $po->additional_docs,
                'auction_duration' => $po->pending_linked_auction()['auction_duration'],
                'status' => $po->pending_linked_auction()['auction_start'] == 'now' ? 'open' : 'scheduled',
                'starts_at' => $po->pending_linked_auction()['auction_start'] == 'now' ? now() : Carbon::parse($po->pending_linked_auction()['scheduled_start_date']),
                'ends_at' => $po->pending_linked_auction()['auction_start'] == 'now'
                    ? now()->addDays($po->pending_linked_auction()['auction_duration'])
                    : Carbon::parse($po->pending_linked_auction()['scheduled_start_date'])->addDays($po->pending_linked_auction()['auction_duration']),
                "auto_restart" => $po->pending_linked_auction()['auction_auto_restart'] ? 1 : 0
            ]);

            $this->rebuildCaches($po->id);

            if ($po->restricted_to_own_suppliers == 1) {
                $supplierList = 'own';
            }
            elseif ($po->restricted_to_group == 1) {
                $supplierList = 'group';
            }
            elseif ($po->restricted_to_selected == 1) {
                $supplierList = 'selected';
            }
            else {
                $supplierList = 'all';
            }
            $notificationList = Auth::user()->company->authorizer_list()->merge(
                [User::find($log->inputted_by)]
            );
            PurchaseOrderCreated::dispatch($po, $supplierList, false, $notificationList, null, $auction);

            $request->session()->flash('flash.banner', 'Purchase Order creation has been approved successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function rejectRequestCreation (Request $request): RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $po = PurchaseOrder::find(json_decode($log->payload)->id);
            $poNumber = $po->purchase_order_number;

            $log->rejected_by = Auth::id();
            $log->rejected_at = now();
            $log->status = 'rejected';
            $log->save();

            $this->rebuildCaches($po->id, true);
            $po->delete();

            Notification::send($log->inputer, new PurchaseOrderRejected($log, $poNumber));
            $request->session()->flash('flash.banner', 'Purchase Order creation has been rejected successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function requestPoModification (Request $request): RedirectResponse
    {
        $purchaseOrder = PurchaseOrder::find($request->id);
        $log = CompanyActivityLog::find($request->log);
        $log->modification_requested_by = Auth::id();
        $log->modification_requested_at = now();
        $log->status = 'modification_requested';
        $log->save();

        $purchaseOrder->status = 'action required';
        $purchaseOrder->save();

        $pendingAction = PendingAction::create([
            'company_id' => Auth::user()->company->id,
            'pending_action_type_id' => 2,
            'pending_action_key' => $request->id,
            'fields' => json_encode($request->modifications),
        ]);

        $this->rebuildCaches($purchaseOrder->id);
        $notificationList = collect(Auth::user()->company->authorizer_list())->merge([User::find($log->inputted_by)]);

        Notification::send($notificationList, new PurchaseOrderModificationRequested($pendingAction, $purchaseOrder, $log));
        $request->session()->flash('flash.banner', 'Purchase Order Modification Request has been pushed to the transaction initiator');
        $request->session()->flash('flash.bannerStyle', 'success');

        return back();
    }

    public function generatePurchaseOrderPDF ($selectedRequest): Response
    {
        return Inertia::render('PurchaseOrders/PrintPurchaseOrder', [
            "selectedRequest" => $selectedRequest,
        ]);
    }

    public function requestModification (Request $request) {
        return $request->poModifyList;
    }

    private function fetchAlias ($files) {
        if (is_array($files) || is_object($files)) {
            $aliases = collect($files)->map(function ($item) {
                return UploadedFile::where('original_name', $item)->where('company_id', Auth::user()->company->id)->latest()->value('file_alias');
            })->all();
        }
        else {
            $aliases = UploadedFile::where('original_name', $files)->where('company_id', Auth::user()->company->id)->latest()->value('file_alias');
        }
        return $aliases;
    }

    private function rebuildCaches ($po, $delete = false): void
    {
        $companyPos = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-pos'));
        $primed = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-pos-primed'));

        if (!$delete) {
            $purchaseOrder= PurchaseOrder::find($po);
            $companyPos = collect($companyPos)->reject( function ($item) use ($purchaseOrder) {
                return $item->id == $purchaseOrder->id;
            })->push($purchaseOrder->withoutRelations());
            $primed = collect($primed)->reject( function ($item) use ($purchaseOrder) {return $item['id'] == $purchaseOrder->id;});

            $singlePrime = [
                "id" => $purchaseOrder->id,
                "purchase_order_number" => $purchaseOrder->purchase_order_number,
                "request_type" => $purchaseOrder->request_type,
                "bid_limitation" => $purchaseOrder->bid_limitation(),
                "line_item_details" => json_decode($purchaseOrder->line_item_details),
                "status" => $purchaseOrder->status,
                "buyer_comments" => $purchaseOrder->buyer_comments,
                "bank_payment_obligation_url" => $purchaseOrder->bank_payment_obligation_url,
                "bank_payment_obligation_id" => $purchaseOrder->bank_payment_obligation_id,
                "delivery_date" => $purchaseOrder->delivery_date,
                "purchase_order_terms_url" => $purchaseOrder->purchase_order_terms_url ?? 'Not Included',
                "initiator" => $purchaseOrder->inputer_id,
                "authorizer" => $purchaseOrder->authorizer_id,
                "name_restricted" => $purchaseOrder->name_restricted,
                "delivery_address" => DeliveryAddress::find($purchaseOrder->delivery_address_id) ?? 'To be provided',
                "shipping_terms" => $purchaseOrder->shipping_terms,
                'expected_warranty_period' => filled($purchaseOrder->expected_warranty_period) ? $purchaseOrder->expected_warranty_period : 'Not Applicable',
                "bid_count" => $purchaseOrder->purchase_order_bids->count() ?? 0,
                "additional_docs" => collect(json_decode($purchaseOrder->additional_docs))->map(function ($item){
                    return ["data" => $item];
                }),
                "purchase_order_url" => $purchaseOrder->purchase_order_url ?? 'Not Included',
                "total_amount" => collect(json_decode($purchaseOrder->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                "termination_reason" => $purchaseOrder->termination_reason,
                "net_price_discrepancy_tolerance" => $purchaseOrder->net_price_discrepancy_tolerance,
                "supplier_requirement" => $purchaseOrder->supplier_requirement == 'single' ? 'Single Supplier' : 'Multiple Suppliers',
                "finance_line" => PurchaseOrderFinanceLine::find($purchaseOrder->po_finance_line_id)?->line_unique_name ?? 'Not Applicable',
                'selected_suppliers' => filled(BuyerSupplier::find(json_decode($purchaseOrder->selected_suppliers)))
                    ? BuyerSupplier::whereIn('supplier_id', json_decode($purchaseOrder->selected_suppliers))->get()->map(fn($i) => [
                        "id" => $i->supplier->id,
                        "supplier_name" => $i->supplier->company_name,
                        "supplier_code" => $i->supplier_code ?? 'Not Applicable',
                        "company_state" => $i->supplier->company_state
                    ]) : [],
                'selected_group_name' => $purchaseOrder->selected_group_name,
                "created" => $purchaseOrder->created_at,
                "payment_terms" => $purchaseOrder->payment_terms,
                "bid_bond_required" => $purchaseOrder->bid_bond_required,
                "bpo_confirmed_at" => $purchaseOrder->bpo_confirmed_at,
                "pending_actions" => $purchaseOrder->pending_action_list(),
                "cancellation_rejection_comment" => $purchaseOrder->last_rejection_comment(),
                "linked_auction" => $purchaseOrder->linked_auction ?? $purchaseOrder->pending_linked_auction(),
                "buyer_logo" => $purchaseOrder->buyer->logo_url,
                "preferred_suppliers" => CompanyPurchaseOrder::where('purchase_order_id', $purchaseOrder->id)->where('is_selected', 1)->get(),
                "vat" => $purchaseOrder->vat,
                "min_acceptable_quantity" => $purchaseOrder->min_acceptable_quantity,
                "wht" => $purchaseOrder->wht,
                "wht_deductible" => $purchaseOrder->wht_deductible,
                "pre_modification_values" => $purchaseOrder->pre_modification_values(),
                "buyer" => $purchaseOrder->buyer->only(['id', 'company_name', 'company_state', 'company_tier'])
            ];
            $primed = collect([$singlePrime])->merge(collect($primed))->values();
        }
        else {
            $companyPos = collect($companyPos)->reject( function ($item) use ($po) { return $item->id == $po; });
            $primed = collect($primed)->reject( function ($item) use ($po) { return $item['id'] == $po;});
        }

        Cache::forever(Str::kebab(Auth::user()->company->company_name . '-pos'), $companyPos);
        Cache::forever(Str::kebab(Auth::user()->company->company_name . '-pos-primed'), $primed);
    }

    private function deleteRemovedFiles(array $removedFiles, $purchaseOrder): void
    {
        foreach ($removedFiles as $removedFile) {
            if (collect($purchaseOrder->purchase_order_url)->contains($removedFile) || collect($purchaseOrder->additional_docs)->contains($removedFile)) {
                $removedFiles = collect($removedFiles)->reject($removedFile)->all();
                $replacementOriginal = UploadedFile::where('file_alias', $removedFile)->first()->original_name;
                $replacement = UploadedFile::where('original_name', $replacementOriginal)->where('company_id', Auth::user()->company->id)->get()
                    ->reject(fn($item) => $item->file_alias == $removedFile)->last()->file_alias;
                $removedFiles = collect($removedFiles)->push($replacement)->all();
            }
        }
        UploadedFile::whereIn('file_alias', $removedFiles)->delete();
        $removedFilesPaths = collect($removedFiles)->map(function ($item) {
            return 'public/po_docs/' . $item;
        })->all();
        Storage::delete($removedFilesPaths);
    }

    public function getSingularUnit($word): string
    {
        return Str::singular($word);
    }
}
