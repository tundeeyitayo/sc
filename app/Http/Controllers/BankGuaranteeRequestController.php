<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewBankGuaranteeRequest;
use App\Http\Requests\UpdateBankGuaranteeRequest;
use App\Jobs\CancelBankGuaranteeRequest;
use App\Jobs\CreateBankGuaranteeRequest;
use App\Jobs\ModifyBankGuaranteeRequest;
use App\Jobs\SendAuthorizationResponseNotification;
use App\Models\Auction;
use App\Models\Bank;
use App\Models\BankGuaranteeRequest;
use App\Models\BankGuaranteeRequestBid;
use App\Models\BankGuaranteeRequestStatus;
use App\Models\BuyerSupplier;
use App\Models\Company;
use App\Models\CompanyActivityLog;
use App\Models\CompanyPurchaseOrder;
use App\Models\DeliveryAddress;
use App\Models\Message;
use App\Models\PendingAction;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderBid;
use App\Models\PurchaseOrderFinanceLine;
use App\Models\Room;
use App\Models\Unit;
use App\Models\UploadedFile;
use App\Models\User;
use App\Notifications\BankGuaranteeCreationRejected;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class BankGuaranteeRequestController extends Controller
{
    //Caches : company-name-guarantees, company-name-guarantees-primed
    public function index(Request $request) {
        if (Gate::allows('request-bg')){
            $companyGuaranteeRequests = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-guarantees'), function () {
                return Auth::user()->company->guarantees;
            });
            $guarantees = Cache::rememberForever(Str::kebab(Auth::user()->company->company_name . '-guarantees-primed'), function () use ($companyGuaranteeRequests) {
                return collect($companyGuaranteeRequests)->sortByDesc('updated_at')->values()->transform(function ($item) {
                    return [
                        "id" => $item->id,
                        "bank" => $item->bank?->bank_name ?? 'Pending',
                        "request_type" => $item->request_type,
                        "requesting_as" => $item->requesting_as,
                        "bid_count" => $item->bank_guarantee_request_bids->count() ?? 0,
                        "supplier" => $item->supplier?->company_name ?? 'Not Available',
                        "buyer" => $item->buyer?->company_name ?? $item->buyer_name,
                        "purchase_order_id" => $item->purchase_order_id,
                        "expiration_date" => $item->expiration_date,
                        "supporting_docs" => collect(json_decode($item->supporting_docs))->map(function ($item){
                            return ["data" => $item];
                        }),
                        "approved_tenure_days" => $item->approved_tenure_days,
                        "amount" => $item->guarantee_amount,
                        "purchase_order_url" => filled($item->purchase_order) ? $item->purchase_order->purchase_order_url : $item->purchase_order_url,
                        "status" => $item->currentStatus->label,
                        "approved_rate" => filled($item->approved_rate) ? $item->approved_rate : 'pending',
                        'total_charges_desc' => filled($item->charges) ? collect(json_decode($item->charges))->toArray() : 'pending',
                        "total_charges" => filled($item->charges) ? collect(json_decode($item->charges))->sum('amount') : 'pending',
                        "created" => $item->created_at,
                        "pending_actions" => $item->pending_action_list(),
                        "pending_action_comment" => $item->pending_action?->comments,
                        "cancellation_rejection_comment" => $item->last_rejection_comment(),
                        "has_pending_modification" => filled($item->pending_modification),
                        "modification_payload" => $item->latest_modification_payload(),
                        "guarantee_type" => $item->guarantee_type
                    ];
                });
            });
            $table_fields = ["bank", "amount", "purchase_order_url", "status", "approved_rate", "total_charges", "created"];
            $sortable_fields = ["bank", "approved_rate", "total_charges", "created", "amount"];
            $date_fields = ["created"];
            $currency_fields = ["total_charges", "amount"];
            $percent_fields = ["approved_rate"];
            $attributes = [
                "id" => "ID",
                "bank" => "Bank",
                "purchase_order" => "Purchase Order #",
                "purchase_order_url" => "Purchase Order",
                "status" => "Status",
                "approved_rate" => "Approved Rate",
                "total_charges" => "Total Charges",
                "created" => "Date Created",
                "amount" => "Guarantee Amount"
            ];

            $header_fields = [];
            //$header_fields[] = "__slot:checkboxes";
            foreach (collect(collect($guarantees)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = array_merge([
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ], collect($date_fields)->contains($field) ? ['formatType' => 'date' ] : [],
                    collect($percent_fields)->contains($field) ? ['formatType' => 'percent' ] : [],
                   collect($currency_fields)->contains($field) ? ['formatType' => 'currency' ] : []);

            }
            $header_fields[] = "__slot:actions:details";
            $userBanks = collect($guarantees)->pluck('bank')->unique()->all();
             sort($userBanks);
            $userStatuses = collect($guarantees)->pluck('status')->unique()->sort();
            $userGuaranteeTypes = collect($guarantees)->pluck('guarantee_type')->unique()->sort();
            $userPurchaseOrders = collect(Auth::user()->company->active_purchase_orders())->transform( function ($item) {
                return [
                    "id" => $item->id,
                    "purchase_order_number" => $item->purchase_order_number,
                    "buyer" => $item->buyer->company_name,
                    "url" => $item->purchase_order_url
                ];
            });


            return Inertia::render('Guarantees/Show', [
                "data" => $guarantees,
                "headerFields" => $header_fields,
                "userBanks" => $userBanks,
                "userStatuses" => $userStatuses,
                "userPurchaseOrders" => $userPurchaseOrders,
                "userGuaranteeTypes" => $userGuaranteeTypes,
                "notFoundMsg" => 'No Bank Guarantee record found'
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function create(Request $request) {
        if (Gate::allows('request-bg')){
            $userPurchaseOrders = collect(Auth::user()->company->active_purchase_orders())->transform( function ($item) {
                return [
                    "id" => $item->id,
                    "purchase_order_number" => $item->purchase_order_number,
                    "buyer" => $item->buyer->company_name,
                    "url" => $item->purchase_order_url
                ];
            });
            return Inertia::render('Guarantees/Create', [
                "userPurchaseOrders" => $userPurchaseOrders,
                "requestingAs" => (Auth::user()->company->account_type == 'supplier' || Auth::user()->company->account_type == 'buyer')
                    ? (Auth::user()->company->account_type == 'supplier' ? 'seller' : 'buyer')
                    : null
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updatePurchaseOrder(Request $request) {
        $purchase_order_id = $request->purchase_order_id;
        $bank_guarantee_id = $request->id;
        $user_guarantees = Auth::user()->company->guarantees->toArray();
        $user_purchase_orders = Auth::user()->company->purchase_orders->toArray();

        $validationList = [
            "purchaseOrder" => $purchase_order_id,
            "bankGuarantee" => $bank_guarantee_id,
            "userPurchaseOrders" => $user_purchase_orders,
            "userGuarantees" => $user_guarantees,
        ];

        $validator = Validator::make($validationList, [
            "purchaseOrder" =>  [
            'required',
            'integer',
            'in_array:userPurchaseOrders.*',
            ],
            "bankGuarantee" =>  [
            'required',
            'integer',
            'in_array:userGuarantees.*',
            ],
        ],
            [
               "purchaseOrder.in_array" => "Invalid Purchase Order" ,
               "bankGuarantee.in_array" => "Invalid Bank Guarantee" ,
            ]);
        $validator->validate();

        $guarantee = BankGuaranteeRequest::find($bank_guarantee_id);
        $guarantee->purchase_order_id = $purchase_order_id;
        $guarantee->save();

        return response('Purchase Order saved', 200);
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function check (UpdateBankGuaranteeRequest $request, $id) {
        $validated = $request->validated();
    }
    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkNew (NewBankGuaranteeRequest $request, $id) {
        $validated = $request->validated();
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createRequest(Request $request): RedirectResponse
    {
        if (Gate::allows('request-bg')) {
            $validator = Validator::make($request->all(), [
                "guarantee_amount" => ['required','numeric','min:0.01'],
                "guarantee_type" => ['required',  Rule::in(['Advance Payment Guarantee','Bid Bond/Guarantee','Performance Bond/Guarantee'])],
                "purchase_order_url" => ['nullable', 'required_without:purchase_order_id', 'ends_with:pdf,jpg,jpeg','string','max:80', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                'purchase_order_id' => ['nullable', 'required_without:purchase_order_url', 'numeric' , Rule::exists('purchase_orders', 'id')->where(function (Builder $query) {
                    return $query->where('status', 'active')->orWhere('status', 'bidding opened');
                }),],
                "buyer_name" => ['nullable', 'required_with:purchase_order_url', 'string', 'max:200'],
                "supporting_docs" => ['array', 'max:3'],
                "supporting_docs.*.data" => ['nullable', 'string', 'max:80', 'ends_with:pdf,jpg,jpeg', Rule::exists('uploaded_files', 'original_name')->where(function (Builder $query) {
                    return $query->where('company_id', Auth::user()->company->id);
                })],
                "requesting_as" => ['required', 'in:buyer,seller'],
                "auction_start" => ['required'],
                "auction_duration" => ['required','integer'],
                "scheduled_start_date" => ["nullable", "after:now"]
            ], [
                "purchase_order_url.required_without" => 'Please upload a valid external purchase order document',
                "purchase_order_url.ends_with" => 'Purchase order document must be in either jp(e)g or pdf format',
                "purchase_order_url.max" => 'Purchase order document name is too long',
                "purchase_order_url.exists" => 'Please upload a valid purchase order document',
                "purchase_order_id.required_without" => 'Please select a valid internal purchase order',
                "purchase_order_id.exists" => 'The selected internal purchase order is either expired or invalid',
                "buyer_name.required_with" => 'Buyer name is required for externally issued purchase orders',
                "supporting_docs.max" => 'You are not permitted to upload more than 3 supporting documents',
                "supporting_docs.*.data.exists" => "Supporting document :position is invalid, please retry upload",
                "supporting_docs.*.data.ends_with" => "'Supporting document :position must be in either jp(e)g or pdf format'"
            ],
                [
                    "supporting_docs" => "supporting documents"
                ]);
            $validator->validate();
            $supportingDocs = collect($request->supporting_docs)->transform(fn($item) => $item['data'])->all();
            $supportingDocs = $this->fetchAlias($supportingDocs);
            $removedFiles = $this->fetchAlias($request->removed_files);
            $bgRequest = BankGuaranteeRequest::create([
                'supplier_id' => $request->requesting_as == 'seller' ? Auth::user()->company->id : null,
                'buyer_id' => ($request->requesting_as == 'seller')
                    ? (filled($request->purchase_order_id) ? PurchaseOrder::find($request->purchase_order_id)->buyer_id : null)
                    : Auth::user()->company->id,
                'request_type' => 'Open for Bidding (Auction)',
                'guarantee_amount' => $request->guarantee_amount,
                'guarantee_type' => $request->guarantee_type,
                'purchase_order_id' => $request->purchase_order_id,
                'purchase_order_url' => $request->purchase_order_url,
                'status' => 11,
                'supporting_docs' => json_encode($supportingDocs),
                'buyer_name' => ($request->requesting_as == 'seller')
                    ? (filled($request->purchase_order_url) ? $request->buyer_name : null)
                    : null,
                'requesting_as' => $request->requesting_as
            ]);

            if (Auth::user()->company->use_authorizer == 1) {
                $log = CompanyActivityLog::create([
                    'activity_type_id' => 3,
                    'activity_key' => $bgRequest->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 1,
                    'payload' => json_encode([
                        "id" => $bgRequest->id,
                        "request" => $request->all(),
                        'buyer' => $bgRequest->buyer ? $bgRequest->buyer->company_name : $bgRequest->buyer_name,
                        'status' => $bgRequest->currentStatus->label,
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);
                CreateBankGuaranteeRequest::dispatch($bgRequest, null, true, Auth::user()->company->authorizer_list(), $log);
                $bgRequest->status = 9;
                $bgRequest->save();
                $request->session()->flash('flash.banner', 'Bank Guarantee Creation Request has been pushed to your company authorizer(s)');
                $request->session()->flash('flash.bannerStyle', 'success');
                $this->rebuildCaches($bgRequest->id);
            }
            else {
                CompanyActivityLog::create([
                    'activity_type_id' => 3,
                    'activity_key' => $bgRequest->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 0,
                    'payload' => json_encode([
                        "id" => $bgRequest->id,
                        'buyer' => $bgRequest->buyer ? $bgRequest->buyer->company_name : $bgRequest->buyer_name,
                        'status' => $bgRequest->currentStatus->label,
                        'removed_files' => $removedFiles,
                    ]),
                    'status' => 'unauthorized',
                ]);
                if ($request->auction_start == 'now') {
                    $bgRequest->status = 8;
                    $bgRequest->save();
                }
                elseif ($request->auction_start == 'schedule') {
                    $bgRequest->status = 12;
                    $bgRequest->save();
                }
                $this->rebuildCaches($bgRequest->id);

                $auction = Auction::create([
                    'auction_transaction_key' => $bgRequest->id,
                    'company_id' => Auth::user()->company->id,
                    'auction_type' => 1,
                    'auction_description' => null,
                    'related_docs' => $bgRequest->supporting_docs,
                    'auction_duration' => $request->auction_duration,
                    'status' => $request->auction_start == 'now' ? 'open' : 'scheduled',
                    'starts_at' => $request->auction_start == 'now' ? now() : Carbon::parse($request->scheduled_start_date),
                    'ends_at' => $request->auction_start == 'now'
                        ? now()->addDays($request->auction_duration)
                        : Carbon::parse($request->scheduled_start_date)->addDays($request->auction_duration),
                    "auto_restart" => $request->auction_auto_restart ? 1 : 0
                ]);
                $bankSuperUsers = Bank::with('super_users')->get()->pluck('super_users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten();
                $notificationList = collect(Auth::user()->company->authorizer_list())->merge([Auth::user()]);
                CreateBankGuaranteeRequest::dispatch($bgRequest, $bankSuperUsers, false, $notificationList, null, $auction);

                $this->deleteRemovedFiles($removedFiles, $bgRequest);

                $request->session()->flash('flash.banner', 'Bank Guarantee Auction Creation Request has been started successfully, check your email for details');
                $request->session()->flash('flash.bannerStyle', 'success');
            }
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function cancelRequest(Request $request): RedirectResponse
    {
        if (Gate::allows('request-bg')){
            $bgRequest = BankGuaranteeRequest::find($request->id);
            if (collect(['pending', 'action required', 'processing', 'bidding opened'])->contains($bgRequest->currentStatus->label)) {
                if (Auth::user()->company->use_authorizer == 1) {
                    $log = CompanyActivityLog::create([
                        'activity_type_id' => 1,
                        'activity_key' => $request->id,
                        'company_id' => Auth::user()->company->id,
                        'inputted_by' => Auth::id(),
                        'authorization_required' => 1,
                        'payload' => json_encode([
                            'id' => $request->id,
                            'buyer' => $bgRequest->buyer ? $bgRequest->buyer->company_name : $bgRequest->buyer_name,
                            'status' => $bgRequest->currentStatus->label
                        ]),
                        'status' => 'unauthorized',
                    ]);
                    CancelBankGuaranteeRequest::dispatch($bgRequest, null, true, Auth::user()->company->authorizer_list(), $log);
                    $bgRequest->status = 9;
                    $bgRequest->save();
                    $request->session()->flash('flash.banner', 'Bank Guarantee Cancellation Request has been pushed to your company authorizer(s)');
                    $request->session()->flash('flash.bannerStyle', 'success');
                    $this->rebuildCaches($bgRequest->id);
                }
                else {
                    $this->processCancellation($bgRequest);

                    CompanyActivityLog::create([
                        'activity_type_id' => 1,
                        'activity_key' => $request->id,
                        'company_id' => Auth::user()->company->id,
                        'inputted_by' => Auth::id(),
                        'authorization_required' => 0,
                        'payload' => json_encode([]),
                        'status' => 'unauthorized',
                    ]);

                    PendingAction::where('company_id', Auth::user()->company->id)->where('pending_action_key', $bgRequest->id)->where('pending_action_type_id', 1)->delete();
                    $request->session()->flash('flash.banner', 'Bank Guarantee Request has been cancelled successfully');
                    $request->session()->flash('flash.bannerStyle', 'success');
                }

            }
            else {
                $request->session()->flash('flash.banner', 'Request with status: ' . ucfirst($bgRequest->currentStatus->label) . ' cannot be cancelled.');
                $request->session()->flash('flash.bannerStyle', 'danger');
            }
            return back();
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function modifyRequest (Request $request): RedirectResponse
    {
        if (Gate::allows('request-bg')) {
            $bgRequest = BankGuaranteeRequest::find($request->id);
            $supporting_docs = collect($request->supporting_docs)->reject(function ($item) use ($bgRequest) {
               return collect(json_decode($bgRequest->supporting_docs))->contains($item['data']) || blank($item['data']);
            })->map(function ($item) {
                return collect($item)->values()->toArray();
            })->flatten()->all();

            $validationFields = $request->pending_action_fields;
            $validationArray = collect([
                'amount' => $request->amount,
                'purchase_order_id' => $request->purchase_order_id,
                'purchase_order_url' => $this->fetchAlias($request->purchase_order_url),
                'supporting_docs' => $this->fetchAlias($supporting_docs),
            ])->reject(function($item, $key) use ($validationFields){
                return collect($validationFields)->doesntContain($key);
            })->all();
            $oldValues = collect($validationArray)->map(fn($item, $key) =>
               $key == 'amount' ?  $bgRequest->guarantee_amount :  ($key == 'supporting_docs' ? json_decode($bgRequest->$key) : $bgRequest->$key)
            )->all();

            $validationRules = collect([
                'amount' => ['required','numeric','min:0.01', Rule::unique('bank_guarantee_requests', 'guarantee_amount')->where(fn (Builder $query) => $query->where('id', $bgRequest->id))],
                'purchase_order_id' => ['required_without:purchase_order_url',Rule::exists('purchase_orders', 'id')->where(function (Builder $query) {
                    return $query->where('status', 'active');
                }),],
                'purchase_order_url' => ['required_without:purchase_order_id', 'string','max:80', 'exists:uploaded_files,file_alias', Rule::unique('bank_guarantee_requests', 'purchase_order_url')->where(fn (Builder $query) => $query->where('id', $bgRequest->id))],
                'supporting_docs' => ['array','min:1','max:3'],
                'supporting_docs.*' => ['required', 'different:purchase_order_url', 'string', 'exists:uploaded_files,file_alias'],
            ])->reject(function ($item, $key) use ($validationFields) {
                return collect($validationFields)->doesntContain($key);
            })->all();

            $validator = Validator::make($validationArray, $validationRules, [
                'amount.required' => 'Guarantee amount is required',
                'amount.unique' => 'Guarantee amount has not been updated',
                'purchase_order_id.exists' => 'Invalid purchase order selected',
                'purchase_order_id.required_without' => 'Purchase order is required',
                'purchase_order_url.required_without' => 'Purchase order is required',
                'purchase_order_url.unique' => 'Please upload a new purchase order document',
                'purchase_order_url.not_in' => 'Uploading same document as a supporting document is not permitted',
                'supporting_docs.*.different' => 'Uploading same document as the purchase order document is not permitted',
                'purchase_order_url.max' => 'Uploaded purchase order document name is too long, please rename and retry upload',
                'supporting_docs.max' => 'Supporting document count has exceeded the maximum of 3',
                'supporting_docs.min' => 'You must upload at least 1 new supporting document',
                'supporting_docs.array' => 'Please check all uploaded supporting documents'
            ], [
                'amount' => 'guarantee amount',
                'purchase_order_id' => 'purchase order',
                'purchase_order_url' => 'purchase order document',
                'supporting_docs' => 'supporting documents',
                'supporting_docs.*.data' => 'supporting document :position'
            ]);
            $validator->validate();
            $removedFiles = $this->fetchAlias($request->removed_files);

            if (Auth::user()->company->use_authorizer == 1) {
                $log = CompanyActivityLog::create([
                    'activity_type_id' => 2,
                    'activity_key' => $request->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 1,
                    'payload' => json_encode([
                        'id' => $request->id,
                        'buyer' => $bgRequest->buyer ? $bgRequest->buyer->company_name : $bgRequest->buyer_name,
                        'status' => $bgRequest->currentStatus->label,
                        'modifications' => $validationArray,
                        'supporting_docs' => collect(json_decode($bgRequest->supporting_docs))->reject(fn($item) => collect($request->removed_files)->contains($item))->merge($this->fetchAlias($supporting_docs))->all(),
                        'removed_files' => $removedFiles,
                        'old_values' => $oldValues
                    ]),
                    'status' => 'unauthorized',
                ]);
                ModifyBankGuaranteeRequest::dispatch($bgRequest, null, true, Auth::user()->company->authorizer_list(), $log);
                $bgRequest->status = 9;
                $bgRequest->save();
                $request->session()->flash('flash.banner', 'Bank Guarantee Modification Request has been pushed to your company authorizer(s)');
                $request->session()->flash('flash.bannerStyle', 'success');
                $this->rebuildCaches($bgRequest->id);
            }
            else {
                $supportingDocs = collect(json_decode($bgRequest->supporting_docs))->reject(fn($item) => collect($request->removed_files)->contains($item))->merge($this->fetchAlias($supporting_docs))->all();

                CompanyActivityLog::create([
                    'activity_type_id' => 2,
                    'activity_key' => $request->id,
                    'company_id' => Auth::user()->company->id,
                    'inputted_by' => Auth::id(),
                    'authorization_required' => 0,
                    'payload' => json_encode([
                        'id' => $request->id,
                        'buyer' => $bgRequest->buyer ? $bgRequest->buyer->company_name : $bgRequest->buyer_name,
                        'status' => $bgRequest->currentStatus->label,
                        'modifications' => $validationArray,
                        'supporting_docs' => collect(json_decode($bgRequest->supporting_docs))->reject(fn($item) => collect($request->removed_files)->contains($item))->merge($this->fetchAlias($supporting_docs))->all(),
                        'removed_files' => $removedFiles,
                        'old_values' => $oldValues
                    ]),
                    'status' => 'unauthorized',
                ]);
                $this->processModification($bgRequest, $validationArray, $supportingDocs, $removedFiles);
            }
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to perform requested action.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }

        return back();
    }

    public function authorizeRequestCancellation (Request $request): RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $bgRequest = BankGuaranteeRequest::find($request->id);
            $this->processCancellation($bgRequest);

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            PendingAction::where('company_id', Auth::user()->company->id)->where('pending_action_key', $bgRequest->id)->where('pending_action_type_id', 1)->delete();
            SendAuthorizationResponseNotification::dispatch($log);
            $request->session()->flash('flash.banner', 'Bank Guarantee Request cancellation has been approved successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function rejectRequestCancellation (Request $request): RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $this->processRejection($request, $log);
            $request->session()->flash('flash.banner', 'Bank Guarantee Request cancellation has been rejected successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to process this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function authorizeRequestModification(Request $request): RedirectResponse {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $bgRequest = BankGuaranteeRequest::find($request->id);
            $updatesArray = json_decode($log->payload)->modifications;
            $supportingDocs = json_decode($log->payload)->supporting_docs;
            $removedFiles = json_decode($log->payload)->removed_files;

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            PendingAction::where('company_id', Auth::user()->company->id)->where('pending_action_key', $bgRequest->id)->where('pending_action_type_id', 1)->delete();
            $this->processModification($bgRequest, $updatesArray, $supportingDocs, $removedFiles );
            SendAuthorizationResponseNotification::dispatch($log);
            $request->session()->flash('flash.banner', 'Bank Guarantee Request modification has been approved successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function authorizeRequestCreation(Request $request): RedirectResponse {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $bgRequest = BankGuaranteeRequest::find(json_decode($log->payload)->id);
            $removedFiles = json_decode($log->payload)->removed_files;
            $pulledRequest = json_decode($log->payload)->request;

            $log->authorized_by = Auth::id();
            $log->authorized_at = now();
            $log->status = 'authorized';
            $log->save();

            if ($pulledRequest->auction_start == 'now') {
                $bgRequest->status = 8;
                $bgRequest->save();
            }
            elseif ($pulledRequest->auction_start == 'schedule') {
                $bgRequest->status = 12;
                $bgRequest->save();
            }
            $this->rebuildCaches($bgRequest->id);

            $auction = Auction::create([
                'auction_transaction_key' => $bgRequest->id,
                'company_id' => Auth::user()->company->id,
                'auction_type' => 1,
                'auction_description' => null,
                'related_docs' => $bgRequest->supporting_docs,
                'auction_duration' => $pulledRequest->auction_duration,
                'status' => $pulledRequest->auction_start == 'now' ? 'open' : 'scheduled',
                'starts_at' => $pulledRequest->auction_start == 'now' ? now() : Carbon::parse($pulledRequest->scheduled_start_date),
                'ends_at' => $pulledRequest->auction_start == 'now'
                    ? now()->addDays($pulledRequest->auction_duration)
                    : Carbon::parse($pulledRequest->scheduled_start_date)->addDays($pulledRequest->auction_duration),
                "auto_restart" => $pulledRequest->auction_auto_restart ? 1 : 0
            ]);
            $bankSuperUsers = Bank::with('super_users')->get()->pluck('super_users')->flatten();
            $notificationList = Auth::user()->company->authorizer_list()->merge(
                [User::find($log->inputted_by)]
            );
            CreateBankGuaranteeRequest::dispatch($bgRequest, $bankSuperUsers, false, $notificationList, null, $auction);

            $this->deleteRemovedFiles($removedFiles, $bgRequest);
            $request->session()->flash('flash.banner', 'Bank Guarantee Request creation has been approved successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to authorize this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function rejectRequestModification (Request $request): RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $this->processRejection($request, $log);
            $request->session()->flash('flash.banner', 'Bank Guarantee Request modification has been rejected successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to process this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    public function rejectRequestCreation (Request $request): RedirectResponse
    {
        $log = CompanyActivityLog::find($request->log);
        if (Gate::allows('authorize-pending-transaction', $log)) {
            $bgRequest = BankGuaranteeRequest::find(json_decode($log->payload)->id);
            $bgRequest->delete();
            $companyGuarantees = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-guarantees'));
            $primed = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-guarantees-primed'));

            $companyGuarantees = collect($companyGuarantees)->reject( function ($item) use ($log) {
                return $item->id == json_decode($log->payload)->id;
            });
            $primed = collect($primed)->reject( function ($item) use ($log) {
                return $item['id'] == json_decode($log->payload)->id;
            });
            Cache::forever(Str::kebab(Auth::user()->company->company_name . '-guarantees'), $companyGuarantees);
            Cache::forever(Str::kebab(Auth::user()->company->company_name . '-guarantees-primed'), $primed);

            Notification::send(User::find($log->inputted_by) , new BankGuaranteeCreationRejected(json_decode($log->payload)->request));

            $log->rejected_by = Auth::id();
            $log->rejected_at = now();
            $log->status = 'rejected';
            $log->save();

            $request->session()->flash('flash.banner', 'Bank Guarantee Request creation has been rejected successfully');
            $request->session()->flash('flash.bannerStyle', 'success');
        }
        else {
            $request->session()->flash('flash.banner', 'You are not permitted to process this request.');
            $request->session()->flash('flash.bannerStyle', 'danger');
        }
        return back();
    }

    private function processCancellation (BankGuaranteeRequest $bgRequest): void
    {
        if ($bgRequest->request_type == 'Open for Bidding (Auction)') {
            $biddingBankUsers = $bgRequest->bank_guarantee_request_bids->pluck('bank_inputer')->merge($bgRequest->bank_guarantee_request_bids->pluck('bank_authorizer'))->unique();
            if (collect($biddingBankUsers)->count() > 0) {
                CancelBankGuaranteeRequest::dispatch($bgRequest, $biddingBankUsers);
            }
            $auction = $bgRequest->live_auction;
            if(filled($auction)) {
                $auction->status = 'canceled';
                $auction->save();
            }


        } else {
            if (filled($bgRequest->bank_id)) {
                CancelBankGuaranteeRequest::dispatch($bgRequest, collect([$bgRequest->bank_inputer, $bgRequest->bank_authorizer]));
            }
        }
        $bgRequest->status = 6;
        $bgRequest->canceled_at = now();
        $bgRequest->save();
        $this->rebuildCaches($bgRequest->id);
    }

    private function processModification (BankGuaranteeRequest $bgRequest, $updatesArray, $supportingDocs = null, $removedFiles = null): void {
        $bgRequest->status = 10;
        foreach ($updatesArray as $key => $value) {
            if ($key == 'amount') {
                $bgRequest->guarantee_amount = $value;
            }
            elseif ($key !== 'supporting_docs') {
                $bgRequest->$key = $value;
            } elseif (filled($supportingDocs)) {
                $bgRequest->$key = json_encode($supportingDocs);
            }
        }
        if (collect($updatesArray)->contains('purchase_order_url')) {
            $bgRequest->purchase_order_id = null;
        }
        elseif (collect($updatesArray)->contains('purchase_order_id')) {
            $bgRequest->purchase_order_url = null;
        }
        $bgRequest->save();
        $this->rebuildCaches($bgRequest->id);

        $bgRequest = $bgRequest->fresh();
        if (filled($bgRequest->bank_id)) {
            ModifyBankGuaranteeRequest::dispatch($bgRequest, collect([$bgRequest->bank_inputer, $bgRequest->bank_authorizer]));
        }

        $this->deleteRemovedFiles($removedFiles, $bgRequest);
    }

    private function rebuildCaches ($bgRequest) {
        $bgRequest = BankGuaranteeRequest::find($bgRequest);
        $companyGuarantees = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-guarantees'));
        $primed = Cache::pull(Str::kebab(Auth::user()->company->company_name . '-guarantees-primed'));

        $companyGuarantees = collect($companyGuarantees)->reject( function ($item) use ($bgRequest) {
            return $item->id == $bgRequest->id;
        })->push($bgRequest->withoutRelations());

        $primed = collect($primed)->reject( function ($item) use ($bgRequest) {
            return $item['id'] == $bgRequest->id;
        });

        $singlePrime = [
            "id" => $bgRequest->id,
            "bank" => $bgRequest->bank->bank_name ?? 'Pending',
            "request_type" => $bgRequest->request_type,
            "requesting_as" => $bgRequest->requesting_as,
            "bid_count" => $bgRequest->bank_guarantee_request_bids->count() ?? 0,
            "supplier" => $bgRequest->supplier->company_name ?? 'Not Available',
            "buyer" => $bgRequest->buyer->company_name ?? $bgRequest->buyer_name,
            "purchase_order_id" => $bgRequest->purchase_order_id,
            "expiration_date" => $bgRequest->expiration_date,
            "supporting_docs" => collect(json_decode($bgRequest->supporting_docs))->map(function ($item){
                return ["data" => $item];
            }),
            "approved_tenure_days" => $bgRequest->approved_tenure_days,
            "amount" => $bgRequest->guarantee_amount,
            "purchase_order_url" => filled($bgRequest->purchase_order) ? $bgRequest->purchase_order->purchase_order_url : $bgRequest->purchase_order_url,
            "status" => $bgRequest->currentStatus->label,
            "approved_rate" => filled($bgRequest->approved_rate) ? $bgRequest->approved_rate : 'pending',
            'total_charges_desc' => filled($bgRequest->charges) ? collect(json_decode($bgRequest->charges))->toArray() : 'pending',
            "total_charges" => filled($bgRequest->charges) ? collect(json_decode($bgRequest->charges))->sum('amount') : 'pending',
            "created" => $bgRequest->created_at,
            "pending_actions" => $bgRequest->pending_action_list(),
            "pending_action_comment" => $bgRequest->pending_action?->comments,
            "cancellation_rejection_comment" => $bgRequest->last_rejection_comment(),
            "has_pending_modification" => filled($bgRequest->pending_modification),
            "modification_payload" => $bgRequest->latest_modification_payload(),
            "guarantee_type" => $bgRequest->guarantee_type
        ];
        $primed = collect([$singlePrime])->merge(collect($primed))->values();

        Cache::forever(Str::kebab(Auth::user()->company->company_name . '-guarantees'), $companyGuarantees);
        Cache::forever(Str::kebab(Auth::user()->company->company_name . '-guarantees-primed'), $primed);
    }

    private function processRejection(Request $request, $log): void
    {
        $bgRequest = BankGuaranteeRequest::find($request->id);
        $bgRequest->status = BankGuaranteeRequestStatus::where('label', json_decode($log->payload)->status)->sole()->id;
        $bgRequest->save();

        $log->rejected_by = Auth::id();
        $log->rejected_at = now();
        $log->status = 'rejected';
        $log->rejection_comments = $request->rejection_comments ?? null;
        $log->save();

        $this->rebuildCaches($bgRequest->id);

        SendAuthorizationResponseNotification::dispatch($log);
    }

    private function fetchAlias ($files) {
        if (is_array($files) || is_object($files)) {
            $aliases = collect($files)->map(function ($item) {
                return UploadedFile::where('original_name', $item)->where('company_id', Auth::user()->company->id)->latest()->value('file_alias');
            })->all();
        }
        else {
            $aliases = UploadedFile::where('original_name', $files)->where('company_id', Auth::user()->company->id)->latest()->value('file_alias');
        }
        return $aliases;
    }

    public function test() {
       /* $recentBgPoUrls = BankGuaranteeRequest::where('created_at', '>', now()->subDays(30))->pluck('purchase_order_url');
        $recentBgSupportingDocs = BankGuaranteeRequest::where('created_at', '>', now()->subDays(30))->pluck('supporting_docs')->reject(function ($item){ return json_decode($item) == [] ;})->map(function ($item){ return json_decode($item); })->flatten();

        //all pending uploads - covers all sections as long as naming convention is followed
        $recentPendingUploads = CompanyActivityLog::where('created_at', '>', now()->subDays(30))->where('status', 'unauthorized')->whereJsonContains('payload->status', 'action required')->orWhereJsonContains('payload->status', 'new')->get()
            ->filter(fn ($item) => collect(json_decode($item->payload))->keys()->contains('modifications') || collect(json_decode($item->payload))->keys()->contains('new_request'))->pluck('payload')
            ->map(function ($item) {
                if (collect(json_decode($item))->has('modifications') && collect(json_decode($item)->modifications)->has('purchase_order_url')) {
                    return json_decode($item)->modifications->purchase_order_url;
                }
                elseif( collect(json_decode($item))->has('modifications') && collect(json_decode($item)->modifications)->has('supporting_docs')) {
                    return json_decode($item)->modifications->supporting_docs;
                }
                elseif (collect(json_decode($item))->has('new_request') && collect(json_decode($item)->new_request)->has('purchase_order_url')) {
                    return json_decode($item)->modifications->purchase_order_url;
                }
                elseif( collect(json_decode($item))->has('new_request') && collect(json_decode($item)->new_request)->has('supporting_docs')) {
                    return json_decode($item)->modifications->supporting_docs;
                }
                else {
                    return '';
                }
            })->flatten();

        $recentBgUploads = collect($recentBgPoUrls)->merge($recentBgSupportingDocs)->merge($recentPendingUploads)->unique();

        $recentlyUploadedBgFiles = UploadedFile::where('created_at', '>', now()->subDays(30))->where('section', 'bank_guarantee_requests')->pluck('file_alias');
        $filesForDeletion = collect($recentlyUploadedBgFiles)->diff($recentBgUploads);
        $storageNames = UploadedFile::whereIn('file_alias', $filesForDeletion)->pluck('file_stored_as');

        UploadedFile::whereIn('file_alias', $filesForDeletion)->delete();
        Storage::delete(collect($storageNames)->map(function ($item) {
            return 'public/bg_docs/' . $item;
        })->all());*/
        //$auction = Auction::find(8);
        //$bid = PurchaseOrderBid::find(10);

        //dd(PurchaseOrder::whereJsonContains('selected_suppliers', 2)->get());
        //Cache::put('all_supplier_company_users', Company::with('users')->where('account_type', 'supplier')->orWhere('account_type', 'buyer_supplier')->get()->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten());
        //dd();

        /*foreach (User::all() as $user){
            $user_store_as = Str::kebab($user->name) . '-' . time() . '.svg';
            if (Storage::put('public/user_avatars/' . $user_store_as, $this->generateAvatar($user->name))) {
                $user->avatar = $user_store_as;
            }
            else{
                $user->avatar = 'user.svg';
            }
            $user->save();
        }*/

        /*foreach (Company::all() as $company){
            $company_store_as = Str::kebab($company->company_name) . '-' . time() . '.svg';
            if (Storage::put('public/company_avatars/' . $company_store_as, $this->generateDefaultLogo($company->company_name))) {
                $company->default_logo = $company_store_as;
            }
            else{
                $company->default_logo = 'building.svg';
            }
            $company->save();
        }*/

        $coy = Company::find(10);
        dd($coy->purchase_order_bids);


        //TODO: include uploaded files from other sections and delete all unused files from storage (move to job when finished)
    }
    public function generateAvatar ($name): string
    {
        $background = collect(['#e53935', '#d81b60', '#8e24aa', '#5e35b1', '#3949ab', '#1e88e5', '#039be5', '#00acc1', '#00897b', '#43a047', '#7cb342', '#c0ca33', '#fdd835', '#ffb300', '#fb8c00', '#f4511e', '#6d4c41', '#757575', '#546e7a'])->random();
        $firstName = Str::before($name, ' ');
        $lastName = Str::afterLast($name, ' ');
        $initials = Str::upper(Str::substr($firstName, 0, 1) . Str::substr($lastName, 0, 1));

        return '<svg width="40px" height="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><rect x="0" y="0" width="40" height="40" rx="50%" style="fill:' . $background . '"/><text x="50%" y="50%" dy=".1em" fill="#fff" text-anchor="middle" dominant-baseline="middle" style="font-family: &quot;Euclid Square&quot;, sans-serif; font-size: 19px; line-height: 1">' . $initials . '</text></svg>';
    }

    public function generateDefaultLogo ($name): string
    {
        $background = collect(['#e53935', '#d81b60', '#8e24aa', '#5e35b1', '#3949ab', '#1e88e5', '#039be5', '#00acc1', '#00897b', '#43a047', '#7cb342', '#c0ca33', '#fdd835', '#ffb300', '#fb8c00', '#f4511e', '#6d4c41', '#757575', '#546e7a'])->random();
        $firstName = Str::before($name, ' ');
        $lastName = Str::after($name, ' ');
        $initials = Str::upper(Str::substr($firstName, 0, 1) . Str::substr($lastName, 0, 1));

        return '<svg width="40px" height="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><rect x="0" y="0" width="40" height="40" rx="50%" style="fill:' . $background . '"/><text x="50%" y="50%" dy=".1em" fill="#fff" text-anchor="middle" dominant-baseline="middle" style="font-family: &quot;Euclid Square&quot;, sans-serif; font-size: 19px; line-height: 1">' . $initials . '</text></svg>';
    }

    private function deleteRemovedFiles(array $removedFiles, $bgRequest): void
    {
        foreach ($removedFiles as $removedFile) {
            if (collect($bgRequest->purchase_order_url)->contains($removedFile) || collect($bgRequest->supporting_docs)->contains($removedFile)) {
                $removedFiles = collect($removedFiles)->reject($removedFile)->all();
                $replacementOriginal = UploadedFile::where('file_alias', $removedFile)->first()->original_name;
                $replacement = UploadedFile::where('original_name', $replacementOriginal)->where('company_id', Auth::user()->company->id)->get()
                    ->reject(fn($item) => $item->file_alias == $removedFile)->last()->file_alias;
                $removedFiles = collect($removedFiles)->push($replacement)->all();
            }
        }
        UploadedFile::whereIn('file_alias', $removedFiles)->delete();
        $removedFilesPaths = collect($removedFiles)->map(function ($item) {
            return 'public/bg_docs/' . $item;
        })->all();
        Storage::delete($removedFilesPaths);
    }
}
