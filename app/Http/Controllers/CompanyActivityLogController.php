<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\BankGuaranteeRequest;
use App\Models\CompanyActivityLog;
use App\Models\NigerianState;
use App\Models\PendingAction;
use App\Models\PurchaseOrder;
use App\Models\User;
use App\Notifications\PurchaseOrderModificationRequested;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Inertia\Inertia;

class CompanyActivityLogController extends Controller
{
    public function index(Request $request) {
        if (Gate::allows('authorize-transactions')){
            $pendingAuthorizations =  Auth::user()->company->pending_authorizations();
            $authorizations = $pendingAuthorizations->transform(function ($item) {
                switch ($item->activity_type->related_table) {
                    case 'bank_guarantee_requests' :
                        return [
                            "id" => $item->id,
                            "activity_type" => $item->activity_type->activity_type,
                            "activity_key" => $item->activity_key,
                            "inputer" => $item->inputer->name,
                            "created" => $item->created_at,
                            "counterparty" => json_decode($item->payload)->buyer,
                            "request_type" => BankGuaranteeRequest::find($item->activity_key)->guarantee_type,
                            "details" => $item->generateDetails(),
                        ];
                    case 'purchase_orders' :
                        if ($item->activity_type->activity_type === 'Purchase Order Creation - Open Bid' ){
                            return [
                                "id" => $item->id,
                                "activity_type" => $item->activity_type->activity_type,
                                "activity_key" => $item->activity_key,
                                "inputer" => $item->inputer->name,
                                "created" => $item->created_at,
                                "counterparty" => $item->parentTransaction()->poLimitation(),
                                "request_type" => PurchaseOrder::find($item->activity_key)->request_type,
                                "details" => $item->generateDetails(),
                                "modifications" => collect($item->generateDetails())
                                    ->reject(fn($item, $key) => collect(['id', 'purchase_order_number', 'status', 'created'])->contains($key))
                                    ->transform(fn($item, $key) =>
                                    ($key == 'line_item_details')
                                        ? collect($item)->transform(fn($i, $k) =>
                                    [
                                        "modify" => false,
                                        "comment" => ''
                                    ]
                                    )
                                        : [
                                        "modify" => false,
                                        "comment" => ''
                                    ]
                                    )
                            ];
                        }
                        elseif ($item->activity_type->activity_type === 'Purchase Order Modification' ) {
                            return [
                                "id" => $item->id,
                                "activity_type" => $item->activity_type->activity_type,
                                "activity_key" => $item->activity_key,
                                "inputer" => $item->inputer->name,
                                "created" => $item->created_at,
                                "counterparty" => $item->parentTransaction()->poLimitation(),
                                "request_type" => PurchaseOrder::find($item->activity_key)->request_type,
                                "details" => $item->generateDetails(),
                                "modifications" => collect($item->generateDetails())
                                    ->reject(fn($item, $key) => collect(['id', 'purchase_order_number', 'status', 'created'])->contains($key))
                                    ->transform(fn($item, $key) =>
                                    ($key == 'line_item_details')
                                        ? collect($item)->transform(fn($i, $k) =>
                                    [
                                        "modify" => false,
                                        "comment" => ''
                                    ]
                                    )
                                        : [
                                        "modify" => false,
                                        "comment" => ''
                                    ]
                                    ),
                            ];
                        }
                        break;
                    case 'auctions':
                        $parent = Auction::find(json_decode($item->payload)->id)->parent_transaction;
                        return [
                            "id" => $item->id,
                            "activity_type" => $item->activity_type->activity_type,
                            "activity_key" => $item->activity_key,
                            "inputer" => $item->inputer->name,
                            "created" => $item->created_at,
                            "counterparty" => $parent->name_restricted == 0 ? $parent->buyer->company_name : $parent->buyer->masked_name(),
                            "request_type" => $parent->poLimitation() == "All " . config('app.name') . " suppliers" ? 'Open Tender' : 'Restricted Tender',
                            "details" => $item->generateDetails(),
                        ];
                }

                });
            $table_fields = [
                "activity_type",
                "counterparty",
                "request_type",
                "inputer",
                "created",
            ];
            $sortable_fields = ["activity_type", "inputer", "created", "counterparty", "request_type"];
            $date_fields = ["created"];
            $attributes = [
                "activity_type" => "Transaction Type",
                "inputer" => "Initiator",
                "created" => "Date Created",
                "counterparty" => "Counterparty",
                "request_type" => "Request Type"
            ];

            $header_fields = [];
            foreach (collect(collect($authorizations)->first())->keys() as $field) {
                if (collect($table_fields)->doesntContain($field)) continue;
                $header_fields[] = array_merge([
                    "name" => $field,
                    "label" => collect($attributes)->get($field),
                    "sortable" => collect($sortable_fields)->contains($field)
                ], collect($date_fields)->contains($field) ? ['formatType' => 'date' ] : []);

            }
            $header_fields[] = "__slot:actions:details";
            $userActivityTypes = collect($authorizations)->pluck('activity_type')->unique()->all();
            sort($userActivityTypes);
            $userInitiators = collect($authorizations)->pluck('inputer')->unique()->all();
             sort($userInitiators);
             $counterParties = collect($authorizations)->pluck('counterparty')->unique()->all();
             sort($counterParties);
            $userRequestTypes = collect($authorizations)->pluck('request_type')->unique()->all();
            sort($userRequestTypes);
            $userFinanceLines = Auth::user()->company->active_po_finance_lines->pluck('line_unique_name', 'id');

            return Inertia::render('PendingAuthorizations/Show', [
                "data" => $authorizations,
                "headerFields" => $header_fields,
                "userActivityTypes" => $userActivityTypes,
                "userInitiators" => $userInitiators,
                "counterParties" => $counterParties,
                "notFoundMsg" => 'No unauthorized transactions found',
                "userRequestTypes" => $userRequestTypes,
                "userFinanceLines" => $userFinanceLines,
                "nigerianStates" => NigerianState::all()->pluck('state_name', 'id'),
            ]);
        }
        else{
            $request->session()->flash('flash.banner', 'You are not authorized to view requested page.');
            $request->session()->flash('flash.bannerStyle', 'danger');
            return redirect('/dashboard');
        }
    }

    public function poCounterParty($limitation, $group = null) {
        return match ($limitation) {
            "all" => "All " . config('app.name') . " suppliers",
            "own" => "All " . Auth::user()->company->company_name . " suppliers",
            "selected" => "Selected " . Auth::user()->company->company_name . " suppliers",
            "group" => "Supplier Group: " . $group
        };
    }

}
