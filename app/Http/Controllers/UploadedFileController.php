<?php

namespace App\Http\Controllers;

use App\Models\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadedFileController extends Controller
{
    public function fetch(string $image) {
        return UploadedFile::where('file_alias', $image)->first()->file_stored_as;
    }
    public function fetch_preview(string $image) {
        return UploadedFile::where('company_id', Auth::user()->company->id)->where('original_name', $image)->latest()->value('file_stored_as');
    }
}
