<?php

namespace App\Mail;

use App\Models\Auction;
use App\Models\BankGuaranteeRequest;
use App\Models\Company;
use App\Models\PurchaseOrder;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class NewAuctionAvailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(public Auction $auction, public User $recipient)
    {
        //
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        $title = match ($this->auction->type->auction_type) {
            'Bank Guarantee Request' => 'New Bank Guarantee Request Auction Available',
            'Purchase Order Tender' => 'New Purchase Order Available For Bidding',
        };
        return new Envelope(
            subject: $title,
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $details = [];
        switch ($this->auction->type->auction_type) {
            case 'Bank Guarantee Request':
                $bg = BankGuaranteeRequest::find($this->auction->auction_transaction_key);
                $details = [
                    "guarantee_amount" => $bg->guarantee_amount,
                    "counterparty" => $bg->buyer ? $bg->buyer->company_name : $bg->buyer_name,
                     'url' => config('app.url') . '/auctions',
                    'markdown' => 'emails.bg_auction_request_created'
                ];
                break;
            case 'Purchase Order Tender':
                $po = PurchaseOrder::find($this->auction->auction_transaction_key);
                $buyer = Company::find($po->buyer_id);
                $details = [
                    "po_amount" => collect(json_decode($po->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
                    "buyer_name" => $po->name_restricted == 1
                        ? 'Tier ' . $buyer->company_tier . ' company in ' . $buyer->company_state . ' (Name withheld)'
                        : $buyer->company_name,
                    'url' => config('app.url') . '/auctions',
                    'markdown' => 'emails.po_auction_request_created'
                ];
                break;
        }
        return new Content(
            markdown: $details['markdown'],
            with: $details
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
