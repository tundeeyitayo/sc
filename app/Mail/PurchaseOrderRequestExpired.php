<?php

namespace App\Mail;

use App\Models\Auction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class PurchaseOrderRequestExpired extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(public Auction $auction, public User $staff)
    {
        //
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Purchase Order Request Expired',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $po = $this->auction->parent_transaction;
        $details = [
            "po_amount" => collect(json_decode($po->line_item_details))->sum(fn($item) => $item->price * $item->quantity),
            "allowed_suppliers" => $this->poLimitations($po),
            'url' => config('app.url') . '/auctions',
        ];
        return new Content(
            markdown: 'emails.company_po_expired',
            with: $details
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
