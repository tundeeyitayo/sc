<?php

namespace App\Mail;

use App\Models\Auction;
use App\Models\BankGuaranteeRequest;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class BankGuaranteeAuctionCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(public Auction $auction, public User $staff)
    {
        //
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Bank Guarantee Request Auction Created',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $bg = BankGuaranteeRequest::find($this->auction->auction_transaction_key);
        $details = [
            "guarantee_amount" => $bg->guarantee_amount,
            "counterparty" => $bg->requesting_as == 'seller' ? ($bg->buyer ? $bg->buyer->company_name : $bg->buyer_name) : 'Not Available',
            'url' => config('app.url') . '/auctions',
        ];
        return new Content(
            markdown: 'emails.bg_company_auction_created',
            with: $details
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
