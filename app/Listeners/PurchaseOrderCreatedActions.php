<?php

namespace App\Listeners;

use App\Jobs\CreatePurchaseOrderRequest;
use App\Models\Company;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Cache;

class PurchaseOrderCreatedActions
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $purchaseOrder = $event->purchaseOrder;
        $recipientList = match ($event->recipientList) {
            'all' => Cache::get(
                'all_supplier_company_users',
                Company::where('account_type', 'supplier')->orWhere('account_type', 'buyer_supplier')->get()->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten()
            ),
            'own' => Company::with('users')->find($purchaseOrder->buyer->supplier_list->pluck('supplier_id'))->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten(),
            'group' => Company::with('users')->find(collect($purchaseOrder->buyer->supplier_groups()[$purchaseOrder->selected_group_name])->pluck('supplier_id'))->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten(),
            'selected' => Company::with('users')->find(json_decode($purchaseOrder->selected_suppliers))->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten(),
            null, 'default' => null
        };
        $auth_required = $event->auth_required;
        $auth_list = $event->auth_list;
        $log = $event->log;
        $auction = $event->auction;

        CreatePurchaseOrderRequest::dispatch($purchaseOrder, $recipientList, $auth_required, $auth_list, $log, $auction);

    }
}
