<?php

namespace App\Listeners;

use App\Events\AuctionClosed;
use App\Models\Auction;
use App\Models\BankGuaranteeRequestBid;
use App\Models\Company;
use App\Models\PurchaseOrderBid;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class AuctionClosureActions
{

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $auction = Auction::find($event->auction->id);
        $company = Company::find($event->company->id);

        switch ($auction->type->auction_type) {
            case "Bank Guarantee Request":
                $bankList = User::find(BankGuaranteeRequestBid::where('auction_id', $auction->id)->get(['bank_inputer', 'bank_authorizer'])->pluck('bank_inputer')
                    ->merge(BankGuaranteeRequestBid::where('auction_id', $auction->id)->get(['bank_inputer', 'bank_authorizer'])->pluck('bank_authorizer'))->unique());
                $staffList = $company->authorizer_list()->merge(
                    [User::find($company->activity_logs->where('activity_type_id', 3)->where('activity_key', $auction->auction_transaction_key)->first()->inputted_by)]
                );
                Notification::send($bankList, new \App\Notifications\AuctionClosed($auction, $company));
                Notification::send($staffList, new \App\Notifications\AuctionClosedStaffNotification($auction));
                break;
            case "Purchase Order Tender":
                $supplierList = Company::find(collect($auction->bids)->pluck('supplier_id'))->pluck('users')->flatten();
                $staffList = $company->authorizer_list()->merge(
                    [User::find($company->activity_logs->where('activity_type_id', 4)->where('activity_key', $auction->auction_transaction_key)->first()->inputted_by)]
                );
                Notification::send($supplierList, new \App\Notifications\AuctionClosed($auction, $company));
                Notification::send($staffList, new \App\Notifications\AuctionClosedStaffNotification($auction));
                break;
        }
    }
}
