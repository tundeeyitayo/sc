<?php

namespace App\Listeners;

use App\Events\AuctionRestarted;
use App\Mail\NewAuctionAvailable;
use App\Models\Auction;
use App\Models\Bank;
use App\Models\BankGuaranteeRequestBid;
use App\Models\Company;
use App\Models\User;
use App\Notifications\AuctionRestartedStaffNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class AuctionRestartedActions
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $auction = Auction::find($event->auction->id);
        $company = Company::find($event->company->id);

        switch ($auction->type->auction_type) {
            case "Bank Guarantee Request":
                $bankList = Bank::with('super_users')->get()->pluck('super_users')->reject(fn($item) => empty($item))->flatten();
                $staffList = $company->authorizer_list()->merge(
                    [User::find($company->activity_logs->where('activity_type_id', 3)->where('activity_key', $auction->auction_transaction_key)->first()->inputted_by)]
                );
                foreach ($bankList as $recipient) {
                        Mail::to($recipient)->send(new NewAuctionAvailable($auction, $recipient));
                }

                Notification::send($staffList, new AuctionRestartedStaffNotification($auction));
                break;
            case "Purchase Order Tender":
                $supplierList = Cache::get('all_supplier_company_users', Company::with('users')->where('account_type', 'supplier')->orWhere('account_type', 'buyer_supplier')->get()->pluck('users')->map(fn($item) => $item->all())->reject(fn($item) => empty($item))->flatten());
                $supplierList = collect($supplierList)->reject(fn($item) => $item->company->id == $company->id);
                $staffList = $company->authorizer_list()->merge(
                    [User::find($company->activity_logs->where('activity_type_id', 4)->where('activity_key', $auction->auction_transaction_key)->first()->inputted_by)]
                );
                foreach ($supplierList as $recipient) {
                        Mail::to($recipient)->send(new NewAuctionAvailable($auction, $recipient));
                }

                Notification::send($staffList, new AuctionRestartedStaffNotification($auction));
                break;
        }
    }
}
