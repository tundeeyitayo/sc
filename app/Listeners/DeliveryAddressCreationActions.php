<?php

namespace App\Listeners;

use App\Models\DeliveryAddress;
use App\Notifications\NewDeliveryAddressAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class DeliveryAddressCreationActions
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $address = $event->deliveryAddress;
        Notification::send($address->buyer->users, new NewDeliveryAddressAdded($address));
    }
}
