<?php

namespace App\Notifications;

use App\Models\Auction;
use App\Models\Bank;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AuctionClosedStaffNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Auction $auction)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->greeting('Hello ' . $notifiable->name)
                    ->line($this->auction->type->auction_type == 'Purchase Order Tender'
                        ? 'This is to notify you that your company\'s Purchase Order auction for PO# ' . $this->auction->parent_transaction->purchase_order_number . ' has been closed.'
                        : 'This is to notify you that your company\'s ' . $this->auction->type->auction_type . ' auction  (₦' . number_format($this->auction->parent_transaction->guarantee_amount, 2) . ') has been closed.')
                    ->line(blank($this->auction->winning_bid) ? 'Unfortunately, your company did not select a winning bid before the auction expired.' : 'The bid submitted by ' . Bank::find($this->auction->bids->where('id', $this->auction->winning_bid)->first()->bank_id)->bank_name . ' was accepted by your company.')
                    ->lineIf(blank($this->auction->winning_bid), 'If you wish to re-open this auction, you may go to the ' . ($this->auction->type->auction_type !== 'Purchase Order Tender' ? $this->auction->type->auction_type : 'Purchase Order') . ' section of your user menu, select this request, then click the re-open auction button');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
