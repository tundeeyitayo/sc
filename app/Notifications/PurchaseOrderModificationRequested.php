<?php

namespace App\Notifications;

use App\Models\CompanyActivityLog;
use App\Models\PendingAction;
use App\Models\PurchaseOrder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Crypt;

class PurchaseOrderModificationRequested extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public PendingAction $action, public PurchaseOrder $purchaseOrder, public CompanyActivityLog $log)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        if ($notifiable->id == $this->log->inputted_by) {
            return (new MailMessage)
                ->greeting('Hello ' . $notifiable->name)
                ->line('Please be advised that your company authorizer - ' . ucwords($this->log->modification_requester->name) . ' has requested for modification of '
                    . 'Purchase Order #' . $this->purchaseOrder->purchase_order_number . ' initiated by you on ' . Carbon::parse($this->log->created_at)->format('M d, Y') . '.')
                ->line('Click the button below to effect requested changes OR login to your ' . config('app.name') . ' account and open the "Manage Purchase Orders" link under the Purchase Order menu to see all your PO requests')
                ->action('Modify Purchase Order', url('/purchase_order/modify/' . Crypt::encryptString($this->log->id)));
        }
        else {
            return (new MailMessage)
                ->greeting('Hello ' . $notifiable->name)
                ->line('Please be advised that your company authorizer - ' . ucwords($this->log->modification_requester->name) . ' has requested for modification of '
                    . 'Purchase Order #' . $this->purchaseOrder->purchase_order_number . ' initiated by ' . (ucwords($this->log->inputer->name)) . '.')
                ->action('Purchase Order Details', url('/purchase_orders/' . Crypt::encryptString($this->log->id)))
                ->line('A separate notification email has been pushed to the transaction initiator.');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
