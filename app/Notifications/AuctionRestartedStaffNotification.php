<?php

namespace App\Notifications;

use App\Models\Auction;
use App\Models\Bank;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

class AuctionRestartedStaffNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Auction $auction)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->greeting('Hello ' . $notifiable->name)
            ->lineIf($this->auction->type->auction_type == 'Bank Guarantee Request','This is to notify you that your company\'s ' . $this->auction->type->auction_type . ' (₦' . number_format($this->auction->parent_transaction->guarantee_amount, 2) . ') auction has been restarted.')
            ->lineIf(blank($this->auction->winning_bid), 'Your company did not select a winning bid before the auction expired.')
            ->lineIf(blank($this->auction->winning_bid), 'The new end date for your auction is ' . Carbon::parse($this->auction->end_date)->addDays($this->auction->auction_duration)->toDayDateTimeString());
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
