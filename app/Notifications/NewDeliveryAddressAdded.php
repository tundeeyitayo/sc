<?php

namespace App\Notifications;

use App\Models\DeliveryAddress;
use App\Models\NigerianState;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewDeliveryAddressAdded extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public DeliveryAddress $address)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->greeting('Hello ' . $notifiable->name)
                    ->line('Please be informed that a new delivery address has been added to your company account on ' . config("app.name") . '.')
                    ->line('The new address will be available for selection in future purchase order requests.')
                    ->line('New Address: ' . $this->address->address . ', ' . $this->address->city . ', ' . NigerianState::find($this->address->state_id)->state_name . '.')
                    ->line('New delivery address was added by user: ' . User::find($this->address->inputed_by)->name . '.')
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
