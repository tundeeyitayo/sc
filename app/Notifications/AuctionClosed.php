<?php

namespace App\Notifications;

use App\Models\Auction;
use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AuctionClosed extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public Auction $auction, public Company $company)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->greeting('Hello ' . $notifiable->name)
                    ->lineIf($this->auction->type->auction_type == 'Bank Guarantee Request','Please be informed that ' . $this->auction->type->auction_type . ' Auction opened by ' . ucwords($this->company->company_name) . ', which your bank is currently bidding on has been closed.')
                    ->lineIf($this->auction->type->auction_type == 'Purchase Order Tender','Please be informed that ' . $this->auction->type->auction_type . ' Auction opened by ' . ucwords($this->company->company_name) . ', which your company is currently bidding on has been closed.')
                    ->lineIf($this->auction->type->auction_type == 'Bank Guarantee Request', ((filled($this->auction->winning_bid)) && ($this->auction->bids->where('id', $this->auction->winning_bid)->sole()->bank_id == $notifiable->bank_id))
                        ? 'We are pleased to inform you that your bank has been selected as the winning bidder.'
                        : 'Unfortunately, your bank bid was not selected by the auction initiator.')
                    ->lineIf($this->auction->type->auction_type == 'Purchase Order Tender', ((filled($this->auction->winning_bid)) && ($this->auction->bids->where('id', $this->auction->winning_bid)->sole()->company_id == $notifiable->company_id))
                        ? 'We are pleased to inform you that your company has been selected as the winning bidder.'
                        : 'Unfortunately, your company\'s bid was not selected by the auction initiator.')
                    ->lineIf($this->auction->type->auction_type == 'Bank Guarantee Request',((filled($this->auction->winning_bid)) && ($this->auction->bids->where('id', $this->auction->winning_bid)->sole()->bank_id == $notifiable->bank_id))
                        ? 'The request has now been moved to the ' . $this->auction->type->auction_type . ' section of your user menu for processing.'
                        : 'You will no longer be able to place bids for this request.')
                    ->lineIf($this->auction->type->auction_type == 'Purchase Order Tender',((filled($this->auction->winning_bid)) && ($this->auction->bids->where('id', $this->auction->winning_bid)->sole()->company_id == $notifiable->company_id))
                        ? 'The request has now been moved to the PURCHASE ORDER section of your user menu for processing.'
                        : 'You will no longer be able to place bids for this request.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
