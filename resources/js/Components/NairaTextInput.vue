<script setup>
import { onMounted, ref } from 'vue';
import _ from "lodash";

defineProps({
    modelValue: String,
});

defineEmits(['update:modelValue']);

const input = ref(null);

onMounted(() => {
    if (input.value.hasAttribute('autofocus')) {
        input.value.focus();
    }
});

defineExpose({ focus: () => input.value.focus() });
</script>

<template>
    <div class="flex items-baseline relative">
        <span class="rounded-l-md border border-gray-300 h-full px-2 absolute left-0 font-bold text-lg flex items-center justify-center">₦</span>
        <input
        ref="input"
        type="text"
        step="0.01"
        class="border-gray-300 focus:border-gray-800 focus:ring-gray-800 rounded-md shadow-sm block pl-10 max-w-full"
        :value="modelValue"
        @input="$emit('update:modelValue', _.isFinite(parseFloat($event.target.value)) ? parseFloat($event.target.value) : '')"
        >
    </div>
</template>
