<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;" />

    <title>{{ 'Purchase Order #' . $po->purchase_order_number}}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    <style>
        .font-sans{font-family:Euclid Square,Figtree,ui-sans-serif,system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,"Apple Color Emoji","Segoe UI Emoji",Segoe UI Symbol,"Noto Color Emoji"}
        .antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}
        .w-full{width:100%}
        .max-w-5xl{max-width:64rem}
        .m-auto{margin:auto}
        .px-12{	padding-left: 3rem; /* 48px */
            padding-right: 3rem; /* 48px */}
        .table-auto{table-layout:auto}
        .border-separate{border-collapse: separate;}
        .border-spacing-y-4{border-spacing: var(--tw-border-spacing-x) 1rem;}
        .text-xs{font-size:.75rem;line-height:1rem}
        .font-bold{font-weight:700}
        .text-gray-500{--tw-text-opacity: 1;color:rgb(107 114 128 / var(--tw-text-opacity))}
        .text-center{text-align:center}
        .text-right{text-align:right}
        .text-left{text-align:left}
        .pb-6{padding-bottom: 1.5rem; /* 24px */}
        .font-semibold{font-weight:600}
        .uppercase{text-transform:uppercase}
        .text-lg{font-size:1.125rem;line-height:1.75rem}
        .w-6{width:1.5rem}
        .h-6{height:1.5rem}
        .grayscale{filter: grayscale(100%)}
        .pl-4{padding-left:1rem}
        .text-blue-900{--tw-text-opacity: 1;color:rgb(30 58 138 / var(--tw-text-opacity));}
        .text-3xl{font-size:1.875rem;line-height:2.25rem}
        .text-sm{font-size:.875rem;line-height:1.25rem}
        .pr-2{padding-right:.5rem}
        .pl-2{padding-left:.5rem}
        .inline-block{display:inline-block}
        .border-b{border-bottom-width:1px}
        .border-b-0{border-bottom-width:0px}
        .border-dotted{border-style: dotted}
    </style>
</head>

<body class="font-sans antialiased">
    <div class="w-full max-w-5xl m-auto px-12">
        <table class="table-auto w-full border-separate border-spacing-y-4">
            <tr>
                <td colspan="2" class="text-xs text-gray-500 text-center pb-6">** This purchase order was issued on {{ config('app.name') }}  ({{ config('app.url') }}). Scan QR code to validate **</td>
            </tr>
            <tr>
                <td class="font-semibold text-lg">
                    <table class="table-auto">
                        <tr>
                            <td>@if(filled($po->buyer->logo_url))<img class="w-6 h-6 grayscale" src="{{ '/storage/company_logos/' . $po->buyer->logo_url }}"/>@endif</td>
                            <td class="pl-4">{{ $po->buyer->company_name }}</td>
                        </tr>
                    </table>
                </td>
                <td class="text-right text-blue-900 font-bold text-3xl">
                    <h1>
                        PURCHASE ORDER
                    </h1>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr><td>{{ $po->buyer->company_address }}</td></tr>
                        <tr><td>{{ $po->buyer->company_city }}</td></tr>
                        <tr><td>{{ $po->buyer->company_state }}</td></tr>
                    </table>
                </td>
                <td class="text-right">
                    <table class="table-auto w-full text-sm">
                        <tr>
                            <td class="text-right pr-2 w-full">Date Issued: </td>
                            <td class="text-right w-full pl-2 inline-block text-sm w-full border-b border-dotted border-gray-500">{{ Illuminate\Support\Carbon::parse($po->created_at)->format('M d, Y') }}</td>
                        </tr>
                        <tr>
                            <td class="text-right pr-2">Purchase Order #: </td>
                            <td class="text-right pl-2 inline-block text-sm border-b border-dotted border-gray-500">{{ $po->purchase_order_number }}</td>
                        </tr>
                        <tr>
                            <td class="text-right pr-2">Delivery Date: </td>
                            <td class="text-right w-full pl-2 inline-block text-sm border-b border-dotted border-gray-500">{{ Illuminate\Support\Carbon::parse($po->delivery_date)->format('M d, Y') }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table-auto">
                        <tr>
                            <td class="text-left">
                                {!! SimpleSoftwareIO\QrCode\Facades\QrCode::size(120)->generate($po->purchase_order_number) !!}
                            </td>
                            <td>
                                <table class="table-auto">
                                    <tr>
                                        <td class="w-1/3 h-full px-12" style="vertical-align: top">
                                            <table class="table-auto w-full">
                                                <tr>
                                                    <td class="bg-blue-800 bg-opacity-[20%] border-b border-blue-600 text-blue-900 font-bold h-full px-6 py-2">
                                                        VENDOR
                                                    </td>
                                                </tr>
                                                @if(filled($supplier))
                                                    <tr><td class="px-6">{{ $supplier->company_name }}</td></tr>
                                                    <tr><td class="px-6">{{ $supplier->company_address }}</td></tr>
                                                    <tr><td class="px-6">{{ $supplier->company_city }}</td></tr>
                                                    <tr><td class="px-6">{{ $supplier->company_state }}</td></tr>
                                                @else
                                                    <tr><td class="px-6">Supplier details pending</td></tr>
                                                @endif
                                            </table>
                                        </td>
                                        <td class="w-1/3 h-full pl-20" style="vertical-align: top">
                                            <table class="table-fixed w-full">
                                                <tr>
                                                    <td class="bg-blue-800 bg-opacity-[20%] border-b border-blue-600 text-blue-900 font-bold h-full px-6 py-2">
                                                        SHIP TO
                                                    </td>
                                                </tr>
                                                @if(filled($po->delivery_address_id))
                                                    @php($address = $po->buyer->delivery_addresses->where('id', $po->delivery_address_id)->first())
                                                    <tr><td class="px-6">{{ $po->buyer->company_name }}</td></tr>
                                                    <tr><td class="px-6">{{ $address->address }}</td></tr>
                                                    <tr><td class="px-6">{{ $address->city }}</td></tr>
                                                    <tr><td class="px-6">{{ App\Models\NigerianState::find($address->state_id)->state_name }}</td></tr>
                                                @else
                                                    <tr><td class="px-6">Delivery address pending</td></tr>
                                                @endif
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table-fixed w-full">
                        <tr>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-blue-400 text-blue-900">Shipping Terms</td>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Payment Terms</td>
                        </tr>
                        <tr>
                            <td class="p-2 text-sm border-b border-l border-gray-500">
                                {{ucfirst($po->shipping_terms)}}
                            </td>
                            <td class="p-2 text-sm border-b border-l border-r border-gray-500">
                                {{ ucfirst($po->payment_terms) }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table-auto w-full">
                        <tr>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-blue-400 text-blue-900">#</td>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Item Description</td>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Quantity</td>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Unit Price</td>
                            <td class="uppercase p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Total</td>
                        </tr>

                        @foreach(json_decode($po->line_item_details) as $key => $item)
                            <tr>
                                <td class="p-2 text-sm border-b border-l border-gray-500">
                                    {{$key + 1}}
                                </td>
                                <td class="p-2 text-sm border-b border-l border-r border-gray-500">
                                    {{ ucfirst($item->description) }}
                                </td>
                                <td class="p-2 text-sm border-b border-l border-r border-gray-500">
                                    {{ ucfirst($item->quantity) }}
                                </td>
                                <td class="p-2 text-sm border-b border-l border-r border-gray-500">
                                    {{ number_format($item->price, 2) }}
                                </td>
                                <td class="p-2 text-sm border-b border-l border-r border-gray-500">
                                    {{ number_format($item->price * $item->quantity, 2) }}
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <table class="table-fixed w-full mt-4">
                        <tr>
                            <td class="uppercase w-full p-2 text-sm font-bold bg-blue-900 bg-opacity-20 border-b border-t border-l border-r border-blue-400 text-blue-900">Comment/Supplier Instruction</td>
                        </tr>
                        <tr>
                            <td class="p-2 text-xs border-b border-l border-r border-gray-500">
                                {{ $po->buyer_comments }}
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="text-right">
                    <table class="table-auto w-full text-sm mt-4">
                        <tr>
                            <td class="text-right pr-2 w-full">Net Total (₦) </td>
                            <td class="w-full p-2 inline-block text-sm w-full border border-gray-500 text-left">{{ number_format(collect(json_decode($po->line_item_details))->sum(fn($item) => $item->price * $item->quantity), 2) }}</td>
                        </tr>
                        <tr>
                            <td class="text-right pr-2">VAT: </td>
                            <td class="w-full p-2 inline-block text-sm w-full border border-gray-500 border-t-0 text-left">{{ $po->vat ?? '-' }}</td>
                        </tr>
                        <tr>
                            <td class="text-right pr-2">Total: </td>
                            <td class="w-full p-2 inline-block text-sm w-full border border-gray-500 border-t-0 text-left">{{ number_format(collect(json_decode($po->line_item_details))->sum(fn($item) => $item->price * $item->quantity) + $po->vat, 2) }}</td>
                        </tr>
                        <tr>
                            <td class="text-xs text-right w-full pt-2" colspan="2">* Deductions may be applicable upon invoice payment.</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="text-xs text-gray-500 text-center pb-6">** This purchase order was issued on {{ config('app.name') }}  ({{ config('app.url') }}). Scan QR code to validate **</td>
            </tr>
        </table>
    </div>

</body>
</html>
