@component('mail::message')

Hello {{$recipient->name}},

This mail serves as confirmation that your company's bid for purchase order # {{ $purchaseOrder->purchase_order_number }} has been successfully submitted.
The buyer will either contact you via email or {{config('app.name')}} integrated messaging system for further discussions OR select your bid as the winning bid.
Be sure to check your emails and your {{config('app.name')}} account regularly for updates.
You will be advised of next steps if your bid is selected as the winning bid.

Find a summary of your offer below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Bid Placed:   |     |  {{ $bid->created_at }}                   |
        @foreach(json_decode($bid->line_item_details) as $key=>$bid)
            | Item {{ $key + 1 }}:  |     |  {{ $bid->quantity . ' ' . $bid->unit . ' @ ' .  '₦ ' . number_format($bid->price, 2) . ' / ' . \Illuminate\Support\Str::singular($bid->unit)}}   |
        @endforeach
    @endcomponent
@endcomponent

You can edit your bid at anytime before the auction closes.

@component('mail::button', ['url' => $url])
    Sign In
@endcomponent

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
    If you're having trouble clicking the button above, please copy and paste this link into the address bar of your web browser:
    [{{$url}}]({{$url}})
@endcomponent

@endcomponent
