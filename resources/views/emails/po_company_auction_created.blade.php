@component('mail::message')
Hello {{$staff->name}},

Please be advised that your company's purchase order auction request on {{config('app.name')}} has been {{ $auction->status == 'open' ? 'opened' : ('scheduled to start at ' . Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString()) }}.
{{ $auction->status == 'open' ? 'This means that your tender has been forwarded to ' . $allowed_suppliers . ' on the platform and you will receive offers from them.' : 'Once started, your request will be forwarded to ' . $allowed_suppliers . ' on the platform and you will receive offers from them.' }}
Suppliers will be able to see the current best offer (without supplier specifics) and strive to better it.

You will be notified via email for every bid received, you may also view bids by visiting the "Manage Purchase Orders" section of your user menu, selecting this request and clicking the "View Bids"
link.
When you find the offer(s) that best suit your requirements, you may then select the preferred supplier(s) and close the auction.

Find transaction preview below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Auction Type:   |     |  {{ $auction->type->auction_type }} |
        | Net Total Amount:  |     |  {{ '₦' . number_format($po_amount, 2) }}                   |
        | Eligible Suppliers:  |     |  {{ $allowed_suppliers }}                |
        | Opens At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString() }}               |
        | Ends At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->ends_at)->toDayDateTimeString() }}                   |
    @endcomponent
@endcomponent

Regards,<br>
{{ config('app.name') }}

@endcomponent
