@component('mail::message')
Hello {{$staff->name}},

Please be advised that your company's purchase order auction request on {{config('app.name')}} which was scheduled to start at {{ Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString() }} has expired.
This means that the request has been suspended and is no longer available for processing because it has remained unauthorized until the scheduled start time.

You may re-open this request by visiting the "Manage Purchase Orders" option under "Purchase Orders" in your user menu, selecting this request and clicking the re-open request button.

Find request details below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Auction Type:   |     |  {{ $auction->type->auction_type }} |
        | Net Total Amount:  |     |  {{ '₦' . number_format($po_amount, 2) }}                   |
        | Eligible Suppliers:  |     |  {{ $allowed_suppliers }}                |
        | Scheduled Start:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString() }}               |
        | Ends At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->ends_at)->toDayDateTimeString() }}                   |
    @endcomponent
@endcomponent

Regards,<br>
{{ config('app.name') }}

@endcomponent
