@component('mail::message')

Hello {{$recipient->name}},

A new bid has just been received from a prospective supplier for the ongoing auction of your Purchase Order # {{ $purchaseOrder->purchase_order_number }}.
@if($purchaseOrder->supplier_requirement == 'multiple') Partial fulfilment by suppliers is enabled for this purchase order as requested by your company. @endif

Find a preview of supplier's offer below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: | --- | ----------------------------------------- |
        | Supplier :    |     |   {{ $bid->supplier->company_name }}      |
        | Bid Placed:   |     |    {{ $bid->created_at }}                 |
        @foreach(json_decode($bid->line_item_details) as $key=>$bid)
        | Item {{ $key + 1 }}:  |     |  {{ $bid->quantity . ' ' . $bid->unit . ' @ ' .  '₦ ' . number_format($bid->price, 2) . ' / ' . \Illuminate\Support\Str::singular($bid->unit)}}   |    |
        @endforeach
    @endcomponent
@endcomponent

Sign in and visit the "Purchase Order" page from your user menu, then select this Purchase Order and click view bids to see all bids, additional supplier comments
and supporting documents submitted by prospective suppliers.

@component('mail::button', ['url' => $url])
    Sign In
@endcomponent

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
    If you're having trouble clicking the button above, please copy and paste this link into the address bar of your web browser:
    [{{$url}}]({{$url}})
@endcomponent

@endcomponent
