@component('mail::message')

Hello {{$recipient->name}},

Please be advised that modification of {{config('app.name')}} Bank Guarantee Request has been effected by {{ $company }} as requested by your bank.

You may sign-in by clicking button below to review modified details and continue with processing the request.

@component('mail::button', ['url' => $url])
    Sign In
@endcomponent

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
    If you're having trouble clicking the button above, please copy and paste this link into the address bar of your web browser:
    [{{$url}}]({{$url}})
@endcomponent

@endcomponent
