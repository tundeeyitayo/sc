@component('mail::message')
    Hello {{$staff->name}},

    Please be advised that your company's bank guarantee auction request scheduled to start at  {{Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString()}} has expired.
    This means that the request has been suspended and is no longer available for processing because it has remained unauthorized until the scheduled start time.

    You may re-open this request by visiting the "Bank Guarantee Requests" option under "Bank Guarantees" in your user menu, selecting this request and clicking the re-open request button.

    Find request details below:

    @component('mail::panel')
        @component('mail::table')
            |               |     |                                           |
            | ------------: |-----| ----------------------------------------- |
            | Auction Type:   |     |  {{ $auction->type->auction_type }} |
            | Guarantee Amount:  |     |  {{ '₦' . number_format($guarantee_amount, 2) }}                   |
            | CounterParty:  |     |  {{ $counterparty }}                |
            | Scheduled For:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString() }}               |
            | Ends At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->ends_at)->toDayDateTimeString() }}                   |
        @endcomponent
    @endcomponent

    Regards,<br>
    {{ config('app.name') }}

@endcomponent
