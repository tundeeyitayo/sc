@component('mail::message')
Hello {{$staff->name}},

Please be advised that your company's bank guarantee auction request on {{config('app.name')}} has been {{ $auction->status == 'open' ? 'opened' : ('scheduled to start at ' . Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString()) }}.
{{ $auction->status == 'open' ? 'This means that your request has been forwarded to all registered banks on the platform and you will receive offers from them.' : 'Once started, your request will be forwarded to all registered banks on the platform and you will receive offers from them.' }}
Banks will be able to see the current best offer (without bank specifics) and strive to better it.

You will be notified via email for every bid received, you may also view bids by visiting the "Bank Guarantee Requests" section of your user menu, selecting this request and clicking the "View Bids"
link.
When you find the offer that best suits your company, you may then select the preferred bank and close the auction.

Find transaction preview below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Auction Type:   |     |  {{ $auction->type->auction_type }} |
        | Guarantee Amount:  |     |  {{ '₦' . number_format($guarantee_amount, 2) }}                   |
        | CounterParty:  |     |  {{ $counterparty }}                |
        | Opens At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->starts_at)->toDayDateTimeString() }}               |
        | Ends At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->ends_at)->toDayDateTimeString() }}                   |
    @endcomponent
@endcomponent

Regards,<br>
{{ config('app.name') }}

@endcomponent
