@component('mail::message')

Hello {{$recipient}},

Please be advised that {{$log->activity_type->activity_type}} initiated by you has been {{ $resp }} by your company authorizer.

Find details below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Transaction:   |     |  {{ $log->activity_type->activity_type }} |
        | Initiated By:  |     |  {{ $log->inputer->name }}                |
        | Initiated At:  |     |  {{ $log->created_at }}                   |
        | {{$resp == 'approved' ? 'Approved By:' : 'Rejected By:'}}  |     |  {{ $resp == 'approved' ? ucwords($log->authorizer->name) : ucwords($log->rejecter->name) }}                 |
        | {{$resp == 'approved' ? 'Approved At:' : 'Rejected At:'}}  |     |  {{ $resp == 'approved' ? $log->authorized_at : $log->rejected_at }}                  |
        @if($resp == 'rejected' && filled($log->rejection_comments)) | Authorizer Comment:   |     |  {{ $log->rejection_comments }} | @endif
    @endcomponent
@endcomponent

The request {{$resp == 'approved' ? ' is no longer available for processing' : ' has been reverted to its previous status and is now available for further processing' }}.

Regards,<br>
{{ config('app.name') }}


@endcomponent
