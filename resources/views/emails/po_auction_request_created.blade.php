@component('mail::message')

Hello {{$recipient->name}},

This is to notify you of a new purchase order available for bidding on {{config('app.name')}}.

Find transaction preview below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Auction Type:   |     |  {{ $auction->type->auction_type }} |
        | Net Total Amount:  |     |  {{ '₦' . number_format($po_amount, 2) }}                |
        | Buyer:  |     |  {{ $buyer_name }}                |
        | Ends At:  |     |  {{ \Illuminate\Support\Carbon::parse($auction->ends_at)->toDayDateTimeString() }}                   |
    @endcomponent
@endcomponent

Please sign-in and enter a bid on behalf of your company before the auction expires.

@component('mail::button', ['url' => $url])
    Sign In
@endcomponent

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
    If you're having trouble clicking the button above, please copy and paste this link into the address bar of your web browser:
    [{{$url}}]({{$url}})
@endcomponent

@endcomponent
