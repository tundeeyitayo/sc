@component('mail::message')

Hello {{$recipient->name}},

Please be advised that purchase order # {{ $purchaseOrder->purchase_order_number }} has been modified by your company initiator {{ $log->inputer->name }} (as requested by your authorizer).
The modification transaction is now awaiting authorization.

Find details below:

@component('mail::panel')
    @component('mail::table')
        |               |     |                                           |
        | ------------: |-----| ----------------------------------------- |
        | Transaction:   |     |  {{ $log->activity_type->activity_type }} |
        | Initiated By:  |     |  {{ $log->inputer->name }}                |
        | Initiated At:  |     |  {{ $log->created_at }}                   |
    @endcomponent
@endcomponent

Sign in and visit the "Pending Authorizations" page from your user menu to complete authorization.

@component('mail::button', ['url' => $url])
    Sign In
@endcomponent

Regards,<br>
{{ config('app.name') }}

@component('mail::subcopy')
    If you're having trouble clicking the button above, please copy and paste this link into the address bar of your web browser:
    [{{$url}}]({{$url}})
@endcomponent

@endcomponent
