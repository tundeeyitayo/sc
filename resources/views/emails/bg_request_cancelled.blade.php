@component('mail::message')

Hello {{$recipient->name}},

Please be advised that {{config('app.name')}} Bank Guarantee Request initiated by {{ $company }} has been cancelled.

As such, {{ $cancelMessage }} the request.


Regards,<br>
{{ config('app.name') }}

@endcomponent
