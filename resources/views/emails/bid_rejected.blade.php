@component('mail::message')

Hello {{$log->inputer->name}},

Please be advised that your bid submission for ongoing auction of purchase order # {{ $po->purchase_order_number }} has been rejected by your company authorizer - {{ $log->rejecter->name }}.
See authorizer's comment below:

@component('mail::panel')
    {{$comment}}
@endcomponent

Regards,<br>
{{ config('app.name') }}

@endcomponent
