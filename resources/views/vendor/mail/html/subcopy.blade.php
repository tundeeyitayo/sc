<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td style="overflow-wrap: anywhere">
{{ Illuminate\Mail\Markdown::parse($slot) }}
</td>
</tr>
</table>
