<?php

use App\Http\Controllers\AuctionController;
use App\Http\Controllers\BankGuaranteeRequestController;
use App\Http\Controllers\CompanyActivityLogController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\UploadedFileController;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\Middleware\HandlePrecognitiveRequests;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
})->middleware('auth');

//bank guarantee routes
Route::get("/guarantee/index", [BankGuaranteeRequestController::class, "index"])->name("guarantee.index")->middleware('auth');
Route::post('/guarantee/check', [BankGuaranteeRequestController::class, "check"])->name('guarantee.check')->middleware([HandlePrecognitiveRequests::class]);
Route::post('/guarantee/check_new', [BankGuaranteeRequestController::class, "checkNew"])->name('guarantee.check_new')->middleware([HandlePrecognitiveRequests::class]);
Route::get('/guarantee/cancel/{id}', [BankGuaranteeRequestController::class, "cancelRequest"])->name('guarantee.cancel')->middleware('auth');
Route::get('/guarantee/bids/{id}', [BankGuaranteeRequestController::class, "fetch_bids"])->name('guarantee.bids')->middleware('auth');
Route::post('/guarantee/modify', [BankGuaranteeRequestController::class, "modifyRequest"])->name('guarantee.modify')->middleware('auth');
Route::post('/guarantee/create_request', [BankGuaranteeRequestController::class, "createRequest"])->name('guarantee.create_request')->middleware('auth');
Route::get('/guarantee/create', [BankGuaranteeRequestController::class, "create"])->name("guarantee.create")->middleware('auth');
Route::get('/guarantee/create_closed', [BankGuaranteeRequestController::class, "createClosed"])->name("guarantee.create_closed")->middleware('auth');

//transaction authorization routes
Route::get('/pending_authorizations', [CompanyActivityLogController::class, "index"])->name('pending_authorizations')->middleware('auth');
Route::get('/guarantee/authorize_cancellation', [BankGuaranteeRequestController::class, "authorizeRequestCancellation"])->name('guarantee.authorize_cancellation')->middleware('auth');
Route::get('/guarantee/authorize_modification', [BankGuaranteeRequestController::class, "authorizeRequestModification"])->name('guarantee.authorize_modification')->middleware('auth');
Route::get('/guarantee/authorize_creation', [BankGuaranteeRequestController::class, "authorizeRequestCreation"])->name('guarantee.authorize_creation')->middleware('auth');
Route::get('/guarantee/reject_cancellation', [BankGuaranteeRequestController::class, "rejectRequestCancellation"])->name('guarantee.reject_cancellation')->middleware('auth');
Route::get('/guarantee/reject_modification', [BankGuaranteeRequestController::class, "rejectRequestModification"])->name('guarantee.reject_modification')->middleware('auth');
Route::get('/guarantee/reject_creation', [BankGuaranteeRequestController::class, "rejectRequestCreation"])->name('guarantee.reject_creation')->middleware('auth');
Route::get('/purchase_order/authorize_creation', [PurchaseOrderController::class, "authorizeRequestCreation"])->name('purchase_order.authorize_creation')->middleware('auth');
Route::get('/purchase_order/authorize_modification', [PurchaseOrderController::class, "authorizeRequestModification"])->name('purchase_order.authorize_modification')->middleware('auth');
Route::get('/purchase_order/reject_creation', [PurchaseOrderController::class, "rejectRequestCreation"])->name('purchase_order.reject_creation')->middleware('auth');
Route::get('/purchase_order/modification_request', [PurchaseOrderController::class, "requestPoModification"])->name('purchase_order.request_modification')->middleware('auth');
Route::get('/auctions/authorize_bid', [AuctionController::class, 'authorizeBid'])->name('auctions.authorize_bid');
Route::get('/auctions/reject_bid', [AuctionController::class, 'rejectBid'])->name('auctions.reject_bid');

//miscellaneous routes
Route::get('fetch_image/{image}', [UploadedFileController::class, "fetch"])->name("fetch_image")->middleware('auth');
Route::get('fetch_preview/{image}', [UploadedFileController::class, "fetch_preview"])->name("fetch_preview")->middleware('auth');

//purchase order routes
Route::get('/purchase_orders/index', [PurchaseOrderController::class, "index"])->name('purchase_orders.show')->middleware('auth');
Route::post('/purchase_order/modify', [PurchaseOrderController::class, "modify"])->name('purchase_order.modify')->middleware('auth');
Route::get('/purchase_orders/create', [PurchaseOrderController::class, "create"])->name('purchase_orders.create')->middleware('auth');
Route::get('/purchase_orders/create_closed', [PurchaseOrderController::class, "createClosed"])->name('purchase_orders.create_closed')->middleware('auth');
Route::post('/purchase_orders/check_new', [PurchaseOrderController::class, "checkNew"])->name('purchase_orders.check_new')->middleware([HandlePrecognitiveRequests::class]);
Route::post('/purchase_orders/check_modify', [PurchaseOrderController::class, "checkModify"])->name('purchase_orders.check_modify')->middleware([HandlePrecognitiveRequests::class]);
Route::post('/purchase_order/create_request', [PurchaseOrderController::class, "createRequest"])->name('purchase_order.create_request')->middleware('auth');
Route::post('/purchase_order/generate_pdf', [PurchaseOrderController::class, 'generatePurchaseOrderPDF'])->name('purchase_order.generate_pdf');
Route::get('/purchase_order/get_singular/{word}', [PurchaseOrderController::class,'getSingularUnit'])->name('fetch_singular_unit');
//bank routes
Route::get('/bank_requests/guarantee/{id?}', [\App\Http\Controllers\BankActivityLogController::class, 'guarantees'])->name('bank_requests.guarantee');

//company routes
Route::get('/company/profile/{id}', [\App\Http\Controllers\CompanyController::class, 'showProfile'])->name('company.profile'); //TODO: create action and view

//auction routes
Route::get('/auctions', [AuctionController::class, 'index'])->name('auctions.index');
Route::post('/auctions/place_bid', [AuctionController::class, 'placeBid'])->name('auctions.place_bid');
Route::get('/bids/{id}/{supplierId?}', [AuctionController::class, 'showBids'])->name('auction.show_bid');

//messaging routes
Route::get('/messaging/{sender?}/{chatTo?}', [MessageController::class, "index"])->name("messaging")->middleware('auth');
Route::post('/fetch_messages', [MessageController::class, "fetch_messages"])->name("messaging.fetch_messages")->middleware('auth');
Route::post('/send_message', [MessageController::class, "send_message"])->name("messaging.send_message")->middleware('auth');
Route::post('/download_messaging_file', [MessageController::class, "download_file"])->name("messaging.download_file")->middleware('auth');
Route::post('/fetch_related_purchase_orders', [MessageController::class, "fetch_related_purchase_orders"])->name("messaging.fetch_pos")->middleware('auth');


//test
Route::get('/test', [BankGuaranteeRequestController::class,"test"])->name('tester');
