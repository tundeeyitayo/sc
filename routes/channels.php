<?php

use App\Models\Room;
use App\Models\User;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
/*
Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/

Broadcast::channel('channel.{id}', function (User $user, int $id) {
    return ($user->company->id == $id)
        || Room::whereJsonContains('subscribers', (string) $id)->pluck("subscribers")
            ->flatMap(fn(string $values) => collect(json_decode($values))->map(fn($item) => (int) $item))
            ->unique()
            ->values()->contains($user->company->id);
});
