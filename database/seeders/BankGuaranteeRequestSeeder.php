<?php

namespace Database\Seeders;

use App\Models\BankGuaranteeRequest;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BankGuaranteeRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BankGuaranteeRequest::factory(200)->create();
    }
}
