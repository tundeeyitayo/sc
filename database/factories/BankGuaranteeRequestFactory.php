<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BankGuaranteeRequest>
 */
class BankGuaranteeRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $randomizer = collect([1,2,3,4,5])->random();
        return [
            'supplier_id' => 1,
            'buyer_id' => $this->faker->numberBetween(23, 32),
            'bank_id' => $randomizer === 1 ? null : $this->faker->numberBetween(1, 5),
            'request_type' => $this->faker->randomElement(['Preferred Bank', 'Open for Bidding (Auction)']),
            'guarantee_amount' => $this->faker->randomFloat(2, 600000, 20000000),
            'purchase_order_id' => $randomizer === 2 ? $this->faker->numberBetween(1, 20) : null,
            'purchase_order_url' => $this->faker->randomElement(['invoice_2023-08-08_02-25-14_16929.pdf', 'widgetworksNig.pdf', 'ret.pdf', 'ret_16933.pdf', 'k1.pdf', 'chain4.jpg', '1206.pdf', '1206_16933.pdf', '1206_16933.pdf', '1206_16933.pdf', 'k1_16933.pdf', '34d.pdf', 'chain4_16933.jpg', 'w1.pdf', 'k1_16933.pdf', '1206_16933.pdf']),
            'status' => $randomizer === 1 ? $this->faker->randomElement([0, 6, 8]) : $this->faker->randomElement([1, 2, 3, 5, 6, 7, 9]),
            'supporting_docs' => json_encode(collect([ [], ['widgetworksNig.pdf'], ['widgetworksNig.pdf', 'ret_16933.pdf'], ['chain4.jpg', '1206.pdf', '1206_16933.pdf'] ])->random()),
            'buyer_name' => $randomizer === 2 ? null : $this->faker->company(),
            'requesting_as' => 'seller',
            'bank_inputer' => $randomizer === 1 ? null : $this->faker->numberBetween(103, 202),
            'bank_authorizer' => $randomizer === 1 ? null : $this->faker->numberBetween(103, 202),
            'approved_rate' => $randomizer === 1 ? null : $this->faker->randomFloat(2, 14, 50),
            'expiration_date' => $randomizer === 1 ? null : $this->faker->dateTimeBetween( '+3 days', '+90 days'),
            'charges' => $randomizer === 1 ? null : $this->faker->randomFloat(2, 5870, 28000),
            'approved_tenure_days' => $randomizer === 1 ? null : $this->faker->randomElement([60, 90, 180]),
            'canceled_at' => null
        ];
    }
}
