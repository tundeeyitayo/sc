<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "company_name" => $this->faker->company() . ' ' . $this->faker->companySuffix(),
            "company_address" => $this->faker->streetAddress(),
            "company_city" => $this->faker->city(),
            "company_state" => $this->faker->numberBetween(1,36),
            "website" => $this->faker->url(),
            "contact_email" => $this->faker->companyEmail(),
            "contact_phone" => $this->faker->phoneNumber(),
            "business_type_id" => $this->faker->numberBetween(1, 10),
            "terms_agreed" => $this->faker->biasedNumberBetween(0,1, function () {return 1;}),
            "year_established" => $this->faker->year(),
            "employee_count_id" => $this->faker->numberBetween(1,5),
            "turnover_type_id" => $this->faker->numberBetween(1,5),
            "ownership_type_id" => $this->faker->numberBetween(1,5),
            "company_description" => $this->faker->paragraph(),
            "industry_type_id" => $this->faker->numberBetween(1, 8),
            "account_type" => $this->faker->randomElement(['supplier','buyer','buyer_supplier']),
            "company_tier" => $this->faker->biasedNumberBetween(1, 3, function () {return 1;}),
            "use_authorizer" => $this->faker->numberBetween(0, 1)
        ];
    }
}
