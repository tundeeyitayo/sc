<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inventory_finance_request_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id");
            $table->unsignedBigInteger("inventory_finance_request_id");
            $table->unsignedInteger("bank_id");
            $table->json("supporting_docs");
            $table->float("offer_rate");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->integer("auction_duration");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inventory_finance_request_bids');
    }
};
