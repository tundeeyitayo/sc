<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('enlisted_banks', function (Blueprint $table) {
            $table->id();
            $table->string("bank_name");
            $table->string("contact_person_name");
            $table->string("contact_person_email");
            $table->string("contact_person_phone");
            $table->boolean("active")->default(1);
            $table->date("subscription_expires_on")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('enlisted_banks');
    }
};
