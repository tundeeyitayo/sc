<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_order_finance_lines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("company_id");
            $table->unsignedBigInteger("bank_id");
            $table->string("line_unique_name");
            $table->double("amount");
            $table->text("request_description");
            $table->integer("proposed_tenure_months");
            $table->integer("approved_tenure_months");
            $table->tinyInteger("status");
            $table->boolean("closed_supplier_set");
            $table->boolean("single_po");
            $table->json("suppliers_list");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_order_finance_lines');
    }
};
