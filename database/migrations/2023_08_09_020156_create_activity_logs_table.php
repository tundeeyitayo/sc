<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_activity_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger("activity_type_id");
            $table->unsignedBigInteger("company_id");
            $table->unsignedBigInteger("pending_action_id")->nullable();
            $table->unsignedBigInteger("inputted_by");
            $table->unsignedBigInteger("authorized_by")->nullable();
            $table->boolean("authorization_required")->default(0);
            $table->json("payload");
            $table->timestamp("authorized_at");
            $table->timestamp("rejected_at");
            $table->enum("status", ["unauthorized", "authorized", "rejected"])->default("unauthorized");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('activity_logs');
    }
};
