<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('distributor_finance_request_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id");
            $table->unsignedBigInteger("distributor_finance_request_id");
            $table->unsignedSmallInteger("bank_id");
            $table->float("offer_rate");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->integer("proposed_tenure_days");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('distributor_finance_request_bids');
    }
};
