<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            $table->renameColumn('amount', 'requested_amount');
            $table->double('availed_amount')->after('amount')->nullable();
            $table->double('available_amount')->after('amount')->nullable();
            $table->float('approved_rate')->nullable()->after('status');
            $table->dropColumn('single_po');
            $table->enum('status', ['new', 'modification_requested', 'bidding_opened', 'canceled', 'processing', 'approved', 'rejected', 'suspended', 'modification_effected'])->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            //
        });
    }
};
