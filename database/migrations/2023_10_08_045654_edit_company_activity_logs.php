<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('company_activity_logs', function (Blueprint $table) {
            $table->enum('status', ['unauthorized','authorized','rejected','modification_requested'])->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('company_activity_logs', function (Blueprint $table) {
            //
        });
    }
};
