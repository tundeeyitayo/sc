<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_order_finance_line_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("purchase_order_finance_line_id");
            $table->unsignedBigInteger("invoice_id");
            $table->double("amount_released");
            $table->double("amount_available");
            $table->double("original_total_availed");
            $table->tinyInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_order_finance_line_transactions');
    }
};
