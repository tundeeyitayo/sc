<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_preshipment_invoice_advances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("purchase_order_id");
            $table->string("purchase_order_url");
            $table->tinyInteger("status");
            $table->unsignedBigInteger("bank_officer_id");
            $table->string("discount_agreement_url");
            $table->string("seller_account");
            $table->string("collection_account");
            $table->tinyInteger("repayment_count");
            $table->json("repayment_schedule");
            $table->boolean("invoice_required_per_payment");
            $table->json("additional_docs");
            $table->unsignedBigInteger("purchase_order_confirmation_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_preshipment_invoice_payments');
    }
};
