<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string("company_name");
            $table->string("company_address");
            $table->string("company_city");
            $table->tinyInteger("company_state");
            $table->string("website")->nullable();
            $table->string("contact_email")->nullable();
            $table->string("contact_phone")->nullable();
            $table->tinyInteger("business_type_id");
            $table->boolean("sc_terms_agreed")->default(0);
            $table->string("year_established", 4);
            $table->tinyInteger("employee_count_id");
            $table->tinyInteger("turnover_type_id");
            $table->tinyInteger("ownership_type_id");
            $table->string("logo_url")->nullable();
            $table->text("company_description");
            $table->json("certifications")->nullable();
            $table->json("licences_certificates")->nullable();
            $table->integer("industry_type_id");
            $table->enum("account_type", ["supplier", "buyer", "buyer_supplier"]);
            $table->tinyInteger("company_tier")->nullable();
            $table->string("purchase_order_terms_url")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
