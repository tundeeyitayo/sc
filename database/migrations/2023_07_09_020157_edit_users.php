<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum("role", ["user", "admin", "super_admin"]);
            $table->string("mobile");
            $table->boolean("active")->default(1);
            $table->enum("user_type", ["company", "bank", "supply_central"]);
            $table->integer("bank_id")->nullable();
            $table->integer("company_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
