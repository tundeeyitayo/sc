<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id");
            $table->unsignedBigInteger("purchase_order_id");
            $table->string("invoice_ref");
            $table->unsignedTinyInteger("bank_id")->nullable();
            $table->json("line_item_details");
            $table->double("total_amount");
            $table->json("additional_docs")->nullable();
            $table->tinyInteger("status")->default(0);
            $table->json("statutory_deduction_exception_doc")->nullable();
            $table->tinyInteger("modification_request_reason_id")->nullable();
            $table->json("other_line_item_details")->nullable();
            $table->boolean("modified")->default(0);
            $table->double("total_payments_to_date")->default(0);
            $table->date("next_payment_due");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invoices');
    }
};
