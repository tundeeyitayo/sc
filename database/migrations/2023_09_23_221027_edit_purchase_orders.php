<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->boolean('restricted_to_group')->default(0);
            $table->boolean('restricted_to_selected')->default(0);
            $table->boolean('restricted_to_own_suppliers')->default(0)->change();
            $table->json('selected_suppliers')->nullable();
            $table->unsignedBigInteger('selected_group_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            //
        });
    }
};
