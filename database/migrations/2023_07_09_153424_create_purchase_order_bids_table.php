<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_order_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("purchase_order_id");
            $table->unsignedBigInteger("supplier_id");
            $table->json("line_item_details");
            $table->text("additional_comments");
            $table->text("additional_docs");
            $table->integer("auction_duration");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_order_bids');
    }
};
