<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_guarantee_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id");
            $table->unsignedBigInteger("company_id")->nullable();
            $table->unsignedSmallInteger("bank_id");
            $table->unsignedBigInteger("purchase_order_id")->nullable();
            $table->string("purchase_order_url");
            $table->tinyInteger("status");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->json("supporting_docs");
            $table->float("approved_rate");
            $table->date("expiration_date");
            $table->integer("approved_tenure_days");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_guarantee_requests');
    }
};
