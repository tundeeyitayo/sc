<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_payment_obligations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('buyer_id');
            $table->unsignedBigInteger('supplier_id');
            $table->mediumInteger('bank_id')->nullable();
            $table->enum('request_type', ['Open for Bidding (Auction)','Preferred Bank']);
            $table->double('payment_obligation_amount');
            $table->unsignedBigInteger('purchase_order_id')->nullable();
            $table->string('purchase_order_url')->nullable();
            $table->smallInteger('status');
            $table->unsignedBigInteger('bank_inputer')->nullable();
            $table->unsignedBigInteger('bank_authorizer')->nullable();
            $table->json('supporting_docs')->nullable();
            $table->float('approved_rate')->nullable();
            $table->json('charges')->nullable();
            $table->date('expiration_date')->nullable();
            $table->integer('approved_tenure_days')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_payment_obligations');
    }
};
