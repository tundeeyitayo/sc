<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            $table->renameColumn("proposed_tenure_months", "proposed_tenure_days");
            $table->renameColumn("approved_tenure_months", "approved_tenure_days");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            //
        });
    }
};
