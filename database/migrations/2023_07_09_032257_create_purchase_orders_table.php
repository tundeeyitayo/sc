<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("company_id");
            $table->string("purchase_order_number");
            $table->json("line_item_details");
            $table->tinyInteger("status")->default(0);
            $table->text("buyer_comments");
            $table->string("bank_guarantee_url")->nullable();
            $table->date("delivery_date")->nullable();
            $table->integer("payment_terms_id");
            $table->unsignedBigInteger("inputer_id");
            $table->unsignedBigInteger("verifier_id");
            $table->boolean("name_restricted")->default(0);
            $table->boolean("open_bid")->default(0);
            $table->text("delivery_address");
            $table->text("shipping_terms")->nullable();
            $table->integer("expected_warranty_period")->nullable();
            $table->json("additional_docs");
            $table->string("termination_reason")->nullable();
            $table->float("net_price_discrepancy_tolerance")->default(0);
            $table->float("net_quantity_discrepancy_tolerance")->default(0);
            $table->integer("required_suppliers_count")->default(1);
            $table->unsignedBigInteger("po_finance_line_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_orders');
    }
};
