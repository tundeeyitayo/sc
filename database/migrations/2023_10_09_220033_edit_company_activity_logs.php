<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('company_activity_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('modification_requested_by')->nullable()->after('rejected_by');
            $table->timestamp('modification_requested_at')->nullable()->after('rejected_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('company_activity_logs', function (Blueprint $table) {
            //
        });
    }
};
