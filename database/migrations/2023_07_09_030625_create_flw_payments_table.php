<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flw_payments', function (Blueprint $table) {
            $table->id();
            $table->string("transaction_id");
            $table->string("flw_ref");
            $table->double("amount");
            $table->string("currency",4);
            $table->string("customer_ip",45);
            $table->string("customer_email");
            $table->string("card_country");
            $table->tinyInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flw_payments');
    }
};
