<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('distributor_finance_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id");
            $table->unsignedBigInteger("company_id");
            $table->unsignedSmallInteger("bank_id");
            $table->string("facility_letter_url");
            $table->string("master_distributor_finance_agreement_url")->nullable();
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->json("additional_docs");
            $table->float("loan_rate");
            $table->integer("loan_tenure_days");
            $table->tinyInteger("status");
            $table->double("loan_amount");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('distributor_finance_requests');
    }
};
