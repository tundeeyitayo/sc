<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('bank_guarantee_requests', function (Blueprint $table) {
            $table->unsignedBigInteger("bank_inputer")->nullable()->change();
            $table->unsignedBigInteger("bank_authorizer")->nullable()->change();
            $table->json("supporting_docs")->nullable()->change();
            $table->json("charges")->nullable()->change();
            $table->float("approved_rate")->nullable()->change();
            $table->date("expiration_date")->nullable()->change();
            $table->integer("approved_tenure_days")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('bank_guarantee_requests', function (Blueprint $table) {
            //
        });
    }
};
