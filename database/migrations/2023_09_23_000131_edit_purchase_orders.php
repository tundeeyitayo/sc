<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->boolean('restricted_to_own_suppliers')->after('purchase_order_number');
            $table->dropColumn('open_bid');
            $table->enum('request_type', ['Open for Bidding (Auction)','Preferred Supplier'])->after('status');
            $table->enum('status', ['new', 'active', 'cancelled', 'fulfilled', 'partly-fulfilled', 'expired', 'action required', 'bidding opened', 'authorization pending'])->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            //
        });
    }
};
