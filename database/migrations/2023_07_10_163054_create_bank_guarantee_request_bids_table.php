<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_guarantee_request_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("auction_id");
            $table->unsignedBigInteger("bank_guarantee_request_id");
            $table->unsignedSmallInteger("bank_id");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->float("offer_rate");
            $table->json("additional_docs");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_guarantee_request_bids');
    }
};
