<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('company_purchase_order', function (Blueprint $table) {
            $table->enum('po_acceptance', ['full', 'partial'])->nullable();
            $table->json('accepted_line_items')->nullable();
            $table->timestamp('selected_at')->nullable();
            $table->unsignedBigInteger('selected_by')->nullable();
            $table->unsignedBigInteger('selection_authorized_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('company_purchase_order', function (Blueprint $table) {
            //
        });
    }
};
