<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->text("buyer_comments")->nullable()->change();
            $table->unsignedBigInteger("payment_terms_id")->nullable()->change();
            $table->string("purchase_order_number")->nullable()->change();
            $table->text("delivery_address")->nullable()->change();
            $table->json("additional_docs")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            //
        });
    }
};
