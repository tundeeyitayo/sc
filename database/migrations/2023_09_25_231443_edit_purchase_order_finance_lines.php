<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            $table->unsignedBigInteger('bank_inputer')->nullable()->after('status');
            $table->unsignedBigInteger('bank_authorizer')->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('purchase_order_finance_lines', function (Blueprint $table) {
            //
        });
    }
};
