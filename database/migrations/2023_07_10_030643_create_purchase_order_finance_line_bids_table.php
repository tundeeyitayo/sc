<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase_order_finance_line_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("company_id");
            $table->unsignedBigInteger("purchase_order_finance_line_id");
            $table->tinyInteger("bank_id");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->float("offered_rate");
            $table->tinyInteger("offered_tenure_months");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase_order_finance_line_bids');
    }
};
