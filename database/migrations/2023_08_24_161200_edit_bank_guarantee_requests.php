<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('bank_guarantee_requests', function (Blueprint $table) {
            $table->enum("requesting_as", ["buyer", "seller"]);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('bank_guarantee_requests', function (Blueprint $table) {
            //
        });
    }
};
