<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_activity_logs', function (Blueprint $table) {
            $table->id();
            $table->smallInteger("activity_type_id");
            $table->unsignedBigInteger("activity_key");
            $table->unsignedBigInteger("company_id");
            $table->unsignedBigInteger("pending_action_id")->nullable();
            $table->unsignedBigInteger("inputted_by");
            $table->unsignedBigInteger("authorized_by")->nullable();
            $table->unsignedBigInteger("rejected_by")->nullable();
            $table->boolean("authorization_required");
            $table->json("payload");
            $table->timestamp("authorized_at")->nullable();
            $table->timestamp("rejected_at")->nullable();
            $table->text("rejection_comments")->nullable();
            $table->enum("status", ['unauthorized','authorized','rejected'])->default('unauthorized');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_activity_logs');
    }
};
