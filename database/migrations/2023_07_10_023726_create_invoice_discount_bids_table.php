<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invoice_discount_bids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("purchase_order_id");
            $table->unsignedBigInteger("seller_id");
            $table->unsignedBigInteger("bank_id");
            $table->unsignedBigInteger("company_id")->nullable();
            $table->unsignedBigInteger("invoice_id")->nullable();
            $table->string("purchase_order_url");
            $table->float("offer_rate");
            $table->json("other_charges")->nullable();
            $table->unsignedBigInteger("bank_inputer_id");
            $table->unsignedBigInteger("bank_authorizer_id");
            $table->text("additional_comments");
            $table->enum("invoice_type", ["pre-shipment", "post-shipment"]);
            $table->integer("auction_duration");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invoice_discount_bids');
    }
};
