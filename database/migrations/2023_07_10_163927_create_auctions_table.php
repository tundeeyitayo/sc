<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("supplier_id")->nullable();
            $table->unsignedBigInteger("buyer_id")->nullable();
            $table->unsignedSmallInteger("auction_type");
            $table->text("auction_description");
            $table->json("related_docs");
            $table->integer("auction_duration");
            $table->tinyInteger("status");
            $table->dateTime("ends_at");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('auctions');
    }
};
