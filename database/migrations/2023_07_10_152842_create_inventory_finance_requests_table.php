<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inventory_finance_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("seller_id");
            $table->unsignedTinyInteger("bank_id");
            $table->json("seller_supporting_docs");
            $table->json("bank_agreement_docs")->nullable();
            $table->tinyInteger("status")->default(0);
            $table->double("loan_amount");
            $table->smallInteger("tenure");
            $table->date("due_date");
            $table->float("approved_rate");
            $table->unsignedBigInteger("bank_inputer");
            $table->unsignedBigInteger("bank_authorizer");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inventory_finance_requests');
    }
};
